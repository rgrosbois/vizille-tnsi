---
hide:
  - navigation
---

# Gestion des processus sous Linux

<small>*(source : cours de J. DIRAISON)*</small>

??? tip "Programme exécutable Linux (hors programme NSI)"

    Le noyau Linux prend en charge le format de fichier ELF pour les programmes 
    exécutables.[^1]

    *[ELF]: Executable and Linkable Format
    [^1]: Windows utilise le format PE (Portable Executable) et Mac OS X le format 
    Mach-O (Mach-object).

    L'utilisation d'un format permet de segmenter le programme en plusieurs 
    sections selon leur usage :

    - Le code en langage machine (lecture et en exécution)
    - Les données telles que les constantes (lecture)
    - Les variables globales et les données non initialisées (lecture et écriture)
    - &hellip;

    Ce format fournit aussi des informations importantes au système :

    - Type d'exécutable.
    - Architecture cible.
    - Point d'entrée du programme (adresse début à exécuter).
    - &hellip;

    ??? example "Exemple"

        ```{.console hl_lines="9 10 12" .rgconsole title=Terminal}
        [nsi@localhost ~]$ readelf -a /bin/sleep
        En-tête ELF:
        Magique:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00 
        Classe:                            ELF64
        Données:                          complément à 2, système à octets de poids faible d'abord (little endian)
        Version:                           1 (actuelle)
        OS/ABI:                            UNIX - System V
        Version ABI:                       0
        Type:                              DYN (fichier objet partagé)
        Machine:                           Advanced Micro Devices X86-64
        Version:                           0x1
        Adresse du point d'entrée:         0x2b90
        Début des en-têtes de programme :  64 (octets dans le fichier)
        Début des en-têtes de section :    35056 (octets dans le fichier)
        Fanions:                           0x0
        Taille de cet en-tête:             64 (octets)
        Taille de l'en-tête du programme:  56 (octets)
        Nombre d'en-tête du programme:     13
        Taille des en-têtes de section:    64 (octets)
        Nombre d'en-têtes de section:      31
        Table d'index des chaînes d'en-tête de section: 30
        ...
        ```
        Il s'agit ici d'un programme exécutable de type `DYN` destiné à une architecture `X86-64` et le point d'entrée est fixé à l'adresse `0x2b90`.

        ```{.console hl_lines="25 26" .rgconsole title=Terminal}
        [nsi@localhost ~]$ readelf -a /bin/sleep
        ...
        En-têtes de section :
            [Nr] Nom               Type             Adresse           Décalage
                Taille            TaillEntrée      Fanion Lien  Info  Alignement
            ...
            [ 6] .dynsym           DYNSYM           00000000000003d0  000003d0
                0000000000000588  0000000000000018   A       7     1     8
            [ 7] .dynstr           STRTAB           0000000000000958  00000958
                00000000000002b3  0000000000000000   A       0     0     1
            [ 8] .gnu.version      VERSYM           0000000000000c0c  00000c0c
                0000000000000076  0000000000000002   A       6     0     2
            [ 9] .gnu.version_r    VERNEED          0000000000000c88  00000c88
                0000000000000050  0000000000000000   A       7     1     8
            [10] .rela.dyn         RELA             0000000000000cd8  00000cd8
                00000000000001b0  0000000000000018   A       6     0     8
            [11] .rela.plt         RELA             0000000000000e88  00000e88
                0000000000000450  0000000000000018  AI       6    24     8
            [12] .init             PROGBITS         0000000000002000  00002000
                000000000000001b  0000000000000000  AX       0     0     4
            [13] .plt              PROGBITS         0000000000002020  00002020
                00000000000002f0  0000000000000010  AX       0     0     16
            [14] .plt.sec          PROGBITS         0000000000002310  00002310
                00000000000002e0  0000000000000010  AX       0     0     16
            [15] .text             PROGBITS         00000000000025f0  000025f0
                0000000000002b62  0000000000000000  AX       0     0     16
            [16] .fini             PROGBITS         0000000000005154  00005154
                000000000000000d  0000000000000000  AX       0     0     4
            [17] .rodata           PROGBITS         0000000000006000  00006000
                0000000000000a60  0000000000000000   A       0     0     32
            ...
        ```

        On apprend ici que le code en langage machine (section `[15].text` située à 
        un décalage `0x25f0`) occupe une taille de `0x2b62` octets et qu'il doit 
        être positionné en mémoire à l'adresse `0x25f0` (réallouable).

    ??? tip "Astuce"

        On peut désassembler un programme avec la commande `objdump`:

        ```{.console .rgconsole}
        [nsi@localhost ~]$ objdump -d /bin/sleep
        ```

        (Rappel 1NSI : [simulation](https://rgrosbois.gitlab.io/vizille-1nsi/archi/02-architecture_ordinateur/) du fonctionnement d'un ordinateur) 
---

## 1. Création d'un processus

Elle se fait en 2 étapes :

1. **fork()** : le processus courant est *dupliqué*[^2] et devient **parent**

    - La copie devient le nouveau processus (**enfant**).
    - Le noyau lui attribue un nouveau numéro d'identification : `PID`=*Process Identification* (le numéro du processus parent est mémorisé dans `PPID`).

    [^2]: appel de la fonction `fork()` de l'OS.

    ??? example "Exemple avec Python"

        ??? info inline end "Documentation de la fonction os.fork"
            ```
            >>> import os
            >>> help(os.fork)
            Help on built-in function fork in module posix:

            fork()
                Fork a child process.
                
                Return 0 to child process and PID of child to parent process.
            ```

        Recopier ces instructions dans un fichier, ajouter lui
        le droit d'exécution et lancer le programme.
        (ne pas utiliser **spyder** mais seulement un simple éditeur de texte)

        ```python title="duplication.py" linenums="1"
        #!/usr/bin/env python3
        import os

        print("Processus initial :", os.getpid(), os.getppid())

        # Fork
        newpid = os.fork()

        if newpid == 0: # dans le processus enfant
            print("Enfant :", os.getpid(), os.getppid())
        else: # dans le processus parent
            print("Parent :", os.getpid(), newpid)
        ```

2. **exec**: Le processus enfant exécute[^3] le nouveau programme avec

    ??? tip inline end "Espace d'adressage (hors programme NSI)"

        C'est le nom donné à l'organisation de la mémoire mise à disposition du 
        processus.

        Exemple :
        ![Espace d'adressage](img/02-espace_adressage.svg){ width=60% }

        En raison de la virtualisation de la mémoire, chaque processus dispose d'un 
        espace d'adressage qui lui est propre et qui ne rentre pas en conflit avec 
        celui des autres processus.

    - Le chemin du programme.
    - Les arguments de la ligne de commande.
    - Les variables d'environnement.

    Les données recopiées du processus parent sont écrasées par celles du nouveau programme et un nouvel espace d'adressage en mémoire[^4] est utilisé.

    [^3]: appel d'une fonction `exec` de l'OS. 
    [^4]: par défaut, le processus enfant a accès aux ressources du parent. C'est ainsi qu'il peut écrire dans le terminal qui l'a lancé.

    ??? example "Exemple avec Python"

        Le programme d'un nouveau processus se défini dans une fonction (ici  `pgm`) :

        ```python title="creation.py" linenums="1"
        #!/usr/bin/env python3
        from multiprocessing import Process
        import os

        def pgm(nom):
            print("Nouveau processus : ", nom, os.getpid(), os.getppid())

        p1 = Process(target=pgm, args=('Proc1',))
        p1.start()
        p2 = Process(target=pgm, args=('Proc2',))
        p2.start()

        print("Parent : ", os.getpid())
        p1.join() # attendre fin de p1
        p2.join() # attendre fin de p2
        ```

## 2. Hiérarchie des processus

??? example "Exemple avec le shell"

    Sauvegarder ce script python dans un fichier (exécutable) :

    ```python title="pinfo.py" linenums="1"
    #!/usr/bin/env python3
    import os

    print("Processus: ", os.getpid(), os.getppid())
    ```

    Et lancer 2 fois le programme :

    ```{.console .rgconsole title="Terminal"}
    [nsi@localhost:~]$ python pinfo.py && python pinfo.py
    Processus:  388389 370500
    Processus:  388390 370500
    ```
    > Les 2 processus créés (par le shell) ont des `PID` différents mais le même `PPID` (qui est celui du shell).

Le processus de duplication entraine une hiérarchisation naturelle des processus, visible notamment 
avec la commande `pstree`.

```{.console .rgconsole title="Terminal"}
[nsi@localhost:~]$ pstree -p
systemd(1)─┬─...
           ...
           ├─systemd(213)─┬─(sd-pam)(224)
           ...            ...
           │              ├─gnome-terminal-(358)─┬─...
           ...            ...                    ...
           │              │                      ├─bash(382)─┬─pstree(405)
           │              │                      │           └─sleep(413)
```

??? tip "Initialisation du noyau et premiers processus (hors programme NSI)"

    1. Le BIOS (ou UEFI) lit et exécute le programme *bootloader* (sur disque dur).
    2. Le *bootloader* charge le noyau et mémoire et lance son exécution.
        - Appel de la fonction `start_kernel()` : détection et initialisation du matériel
        - Création du processus 0
        - Appel de la fonction `reset_init()` : création de 2 processus
            - Appel de la fonction `kernel_init()` : création du processus `/sbin/init` (`systemd`) de PID 1 (montage système de fichiers, démarrage OS sur disque en mode utilisateur).
            - Appel de la fonction `kthreadd()` : création du processus `[kthreadd]` de PID 2 en arrière-plan pour gérer les autres processus noyau.
        - Appel de la fonction `schedule()` : démarrage de l'ordonnanceur avec boucle d'attente sans fin (`cpu_idle()`) en parallèle du processus 1 (le processus 0 est interrompu).

## 3. Interactions avec le noyau

- Le processus peut avoir besoin du noyau pour réaliser des actions spécifiques
(ouverture, lecture/écriture d'un fichier, allocation mémoire, création et 
attente de processus enfant&hellip;)[^1]. Il utilise généralement une 
bibliothèque appropriée (exemple : *glibc*).

    [^1]: lorsqu'un processus sollicite une ressource (exemple : lecture d'un 
    fichier) et que cette dernière n'est pas immédiatement accessible 
    (exemple : disque dur déjà en cours d'utilisation), il est mis en **sommeil** 
    un certain temps pour être ensuite réveillé.

    ![Appels système](img/02-syscall.png)

- De son côté, le noyau maintient une liste (circulaire) de tous les processus 
avec leurs informations :

    | Nom         | Détail                                              |
    |:------------|:----------------------------------------------------|
    | `PID`       | numéro pour identifier le processus                 | 
    | `state`     | état du processus                                   |
    | `mm`        | mémoire allouée                                     |
    | `exit_code` | code de retour                                      |
    | `cred`      | contexte de sécurité (utilisateur, groupes&hellip;) |
    | &hellip;    |  &hellip;                                           |

*[PID]: Process Identification

## 4. États d'un processus

??? example "Exemple avec Python"

    ```python linenums="1" title="parallele.py"
    #!/usr/bin/env python3
    from multiprocessing import Process
    import os
    import time

    def pgm(nom):
        print(nom, os.getpid(), os.getppid())
        for i in range(10):
            print(f'{nom} en cours, itération {i}')
            time.sleep(0.02)

    p1 = Process(target=pgm, args=('Proc0',))
    p2 = Process(target=pgm, args=('Proc1',))
    
    # Démarrer chaque processus
    p1.start()
    p2.start()

    print("Parent : ", os.getpid())

    # Attendre la fin des processus enfants
    p1.join() 
    p2.join() 
    ```

    Lancer le script ci-dessus et observer le résultat.

    ??? tip "Résultat"
        ```{.console .rgconsole title="Terminal"}
        [nsi@localhost:~]$ python parallele.py
        Parent :  469938
        Proc0 469939 469938
        Proc0 en cours, itération 0
        Proc1 469940 469938
        Proc1 en cours, itération 0
        Proc0 en cours, itération 1
        Proc1 en cours, itération 1
        Proc0 en cours, itération 2
        Proc1 en cours, itération 2
        Proc0 en cours, itération 3
        Proc1 en cours, itération 3
        ...
        ```

        > On constate que les 2 processus s'exécutent *en même temps* (même
        si le système ne possède qu'un processeur *monoc&oelig;ur*). 

Le processus passe par différents états durant sa durée de vie et ne s'exécute que lorsqu'il est à l'état *Running* (élu) :

=== "Version simplifiée"

    ??? info inline end "Détails"

        - À sa création, il est placé dans l'état **prêt** (`Ready`) et inséré dans la 
        file d'attente de l'ordonnanceur.
        - Lorsque vient son tour, il passe à l'état **élu** (`Running`) : il s'exécute 
        pendant un *certain temps* puis passe à l'état 

            - *prêt*, si l'ordonnanceur reprend la main ou 
            - **bloqué**, s'il sollicite une ressource non disponible (il retournera
            à l'état prêt lorsque la ressource redeviendra disponible).

        - Lorsqu'il se termine (volontairement ou non), il passe à l'état **terminé**  (libération des ressources) et est détruit.

    ```mermaid
    stateDiagram-v2
    direction LR

    Ready: Ready<br/>(prêt)
    Running: Running<br/>(élu)
    Blocked: Blocked<br/>(bloqué)

    [*] --> Ready: création
    state actif {
        Ready --> Running : ordonnancement
        Running --> Ready : préemption
    }
    Running --> Blocked : attente<br/>évènement
    Blocked --> Ready : réception<br/>évènement

    Running --> [*] : terminaison
    ```

=== "Version plus complète"

    ??? info inline end "Détails"

        - À sa création, il est placé dans l'état **prêt** (`Ready`) et inséré dans la 
        file d'attente de l'ordonnanceur.
        - Lorsque vient son tour, il passe à l'état **élu** (`Running`) : il s'exécute 
        pendant un *certain temps* puis passe à l'état 

            - *prêt*, si l'ordonnanceur reprend la main ou 
            - **stoppé**, s'il reçoit le signal `SIGTSTP` ou
            - **bloqué**, s'il sollicite une ressource non disponible (il retournera
            à l'état prêt lorsque la ressource redeviendra disponible).

        - Lorsqu'il se termine (volontairement ou non), il passe à l'état **terminé** 
        (`zombie`) jusqu'à ce que son code de retour soit lu par le parent.
        - Une fois que le parent a lu le code de retour, il passe à l'état **mort** 
        (`Dead`) et est détruit (libération des ressources).

    ```mermaid
    stateDiagram-v2
    direction LR

    Ready: Ready<br/>(prêt)
    Running: Running<br/>(élu)
    Blocked: Blocked<br/>(bloqué)
    Stopped: Stopped<br/>(stoppé)
    Zombie: Zombie<br/>(terminé)
    Dead: Dead<br/>(mort)

    [*] --> Ready: fork()
    state actif {
        Ready --> Running : ordonnancement
        Running --> Ready : préemption
    }
    Running --> Blocked : attente<br/>évènement
    Blocked --> Ready : réception<br/>évènement
    Running --> Stopped : signal<br/>SIGTSTP
    Stopped --> Ready : signal<br/>SIGCONT

    Running --> Zombie : exit()<br/>(ou plantage)
    Zombie --> Dead : wait()
    Dead --> [*]
    ```


*[ordonnanceur]: gestionnaire de l'ordre d'exécution des processus (*scheduler*).

## 5. Pour aller plus loin

??? example "Quiz"

    Répondre en expérimentant et en faisant, si nécessaire, des recherches.

    1. Qu'est-ce qu'un `PID` ?
    2. Comment tuer un processus de `PID` 1234 alors que la commande `kill 1234` 
    semble sans effet ?
    3. Quelle commande permet d’obtenir spécialement le `PID` d’une commande déjà 
    lancée sur la base de son seul nom ?

        > On attend une réponse autre que `top` ou `ps` qui fournissent bien d’autres 
        informations que celle qui est attendue
    4. Que sont `UID` et `PPID` que l’on peut lire, par exemple, avec la commande 
    `ps -l` ?
    5. Commencer par taper dans la console `/bin/bash` (pour démarrer un second 
    interpréteur) puis lancer la commande `sleep 24h` en tâche de fond. 
        - Quel est le `PID` du processus parent de `sleep` ?
        > Quitter le second interpréteur de commande en tapant `exit`. 
        - Le processus `sleep` existe-t-il encore ? Si oui, quel est le `PID` 
        de son parent et quel est son nom ?
    6. À quoi correspond la valeur de la colonne `NI` affichée par la commande 
    `ps -l` ?

        - Comment peut-on modifier cette valeur à l’aide de la commande `top` ?
        - Comment peut-on affecter une autre valeur que *0* à `NI` au lancement d’une 
        commande ?
        - Comment modifier cette valeur pour une commande déjà lancée ?

??? tip "Processus légers (hors programme NSI)"

    Ces variants de *processus* (**thread**) partagent le même espace d'adressage (sauf la pile). Ils sont très courants pour :

    - pilotage d'interfaces graphiques
    - encodage vidéo
    - calculs scientifiques
    - utilisation réseau
    - &hellip;

