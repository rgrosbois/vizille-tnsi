---
hide:
  - navigation
---

Répondre en expérimentant et en faisant, si nécessaire, des recherches.

1. Qu'est-ce qu'un `PID` ?
2. Comment tuer un processus de `PID` 1234 alors que la commande `kill 1234` 
semble sans effet ?
3. Quelle commande permet d’obtenir spécialement le `PID` d’une commande déjà 
lancée sur la base de son seul nom ?

    > On attend une réponse autre que `top` ou `ps` qui fournissent bien d’autres 
    informations que celle qui est attendue
4. Que sont `UID` et `PPID` que l’on peut lire, par exemple, avec la commande 
`ps -l` ?
5. Commencer par taper dans la console `/bin/bash` (pour démarrer un second 
interpréteur) puis lancer la commande `sleep 24h` en tâche de fond. 
    - Quel est le `PID` du processus parent de `sleep` ?
    > Quitter le second interpréteur de commande en tapant `exit`. 
    - Le processus `sleep` existe-t-il encore ? Si oui, quel est le `PID` 
    de son parent et quel est son nom ?
6. À quoi correspond la valeur de la colonne `NI` affichée par la commande 
`ps -l` ?

    - Comment peut-on modifier cette valeur à l’aide de la commande `top` ?
    - Comment peut-on affecter une autre valeur que *0* à `NI` au lancement d’une 
    commande ?
    - Comment modifier cette valeur pour une commande déjà lancée ?

!!! tip "Réponses"

    1. *Process ID* : numéro de processus attribué par le noyau.
    1. `kill -9 1234`
    1. `pidof` ou `psgrep`
    1. `UID`: identifiant de l'utilisateur à qui appartient le processus. `PPID`
    : `PID` du processus parent.
    1. Celui du second shell. Le processus existe encore et est rattaché
    au processus *systemd* de l'utilisateur.
    1. *Nice* : priorité d'exécution du processus (maximum=-20, maximum 
    simple utilisateur=0, minimum=19).
        - Raccourci clavier ++r++
        - Commande `nice`
        - Commande `renice`
