---
hide:
  - navigation
---

# Notions de routage dynamique

!!! example "Exemple d'étude"

    Étudions une communication entre `PC1` et `PCA` via un réseau *intermédiaire* :

    ![Communication réseau intermédiaire](img/src/05-comm_reseau_intermediaire.svg)

    où `PC1` et `PCA` sont configurés avec des passerelles par défaut et le *réseau intermédiaire* est constitué de 3 routeurs:

    ![Réseau intermédiaire simple](img/src/05-intermediaire_simple.svg)

    Pour cette étude, nous considérerons que les liaisons *noires* et *rouges* ont des débits respectifs de 1 Gbps et 1.544 Mbps.

## 1. Métrique

Nous avons vu que chaque routeur ne connaît initialement que les routes vers ses propres réseaux *directement connectés*. L'activation d'un protocole de routage dynamique sur ces 3 routeurs va entraîner un partage de connaissances du type (réseau + information supplémentaire) :

![Partage de route](img/src/05-partage_route_intermediaire_simple.svg){ width=80% }

L'information supplémentaire, spécifique au protocole de routage, s'appelle une **métrique** (=distance/longueur/coût pour atteindre un réseau). Cette dernière va permettre de comparer les routes entre elles afin de ne conserver que les *meilleures.* 

Les tables de routages des routeurs auront donc généralement le format suivant :

| No | Destination | Métrique | Prochain routeur |
|:--:|-------------|:--------:|:-----------------|
| 1  | &hellip;    | &hellip; | &hellip;         |
| 2  | &hellip;    | &hellip; | &hellip;         |

## 2. Familles de protocoles

Il existe actuellement 2 familles de protocoles de routage dynamique :

??? tip inline end "Analogie routière"

    ![Panneau routier](img/src/04-route_metrique.svg){ align="left" }

    C'est le cas d'un automobiliste qui se rend à sa destination en n'utilisant que les informations des panneaux routiers à chaque intersection.

- Les protocoles à **vecteurs de distance** (ex : RIP) où le routeur ne connaît que la longueur (=métrique) et la direction (=prochain routeur) des meilleures routes menant aux réseaux.

??? tip inline end "Analogie routière"

    ![Carte routière](img/carte_routiere.png){ align="left" }

    C'est le cas d'un automobiliste qui se rend à sa destination en utilisant une carte routière (voire peut-être aussi les informations trafic en temps réel).

- Les protocoles à **états de liens** (ex : OSPF) où le routeur connaît la topologie complète (autour de lui) et notamment le coût de chaque lien. Il peut ainsi déterminer seul le meilleur chemin qui le sépare de chaque réseau.



## 3. Protocole RIP

(*Routing Information Protocol*)

!!! info "Idée" 

    Comme les signaux circulent à la *vitesse de la lumière* dans les liens, la *latence* est essentiellement due au traitement des routeurs intermédiaires. En effet, pour chaque paquet entrant, le routeur doit :

    1. accéder à l'en-tête pour identifier le réseau de destination,
    2. consulter sa table de routage pour identifier la route appropriée,
    3. réémettre le paquet sur la bonne sortie.

    Conclusion: le nombre de routeur rencontré par le paquet sur son trajet a une influence sur la rapidité d'acheminement.

La métrique utilisée est le *nombre de routeurs à traverser* (=**nombre de sauts**, *hop-count*) pour atteindre le réseau (`0` : réseau directement connecté, `16` : distance infinie/réseau non atteignable)

![RIP début](img/src/05-reseau_intermediaire_RIP-debut.svg){ width=80% }

Le processus RIP est itératif: à chaque itération chaque routeur diffuse à ses **voisins immédiats** les informations de routes (réseau + nombre de sauts) contenues dans sa table de routage.

!!! danger "Algorithme de Bellman-Ford"

    À chaque transfert de route, il faut:

    - Côté émission : incrémenter le nombre de saut de 1 et se signaler en tant que *prochain routeur*.

        ![Information Bellman-Ford](img/src/05-information_bellman_ford.svg){width=80%}

    - Côté réception : ajouter/modifier la route dans la table de routage si

        1. il s'agit d'un réseau encore inconnu ou,
        2. le nombre de saut est inférieur (ou égal) à celui de la route déjà connue.

        ![Information Bellman-Ford](img/src/05-information_bellman_ford2.svg){width=80%}

Après quelques itérations, les tables de routage des routages n'évoluent plus (on dit que le processus a *convergé*) : chaque routeur connaît alors les routes optimales vers chaque réseau (partagé par ce protocole).

![RIP convergé](img/src/05-reseau_intermediaire_RIP-fin.svg)

??? example "Exercice"

    À ce stade, `PC1` ne peut toujours pas communiquer avec `PCA` car le réseau intermédiaire n'a pas de routes pour `192.168.1.0/24` et `192.168.2.0/24`. Une solution consiste à activer aussi le protocole de routage RIP sur `R1` et `R2` (en plus de `RA`, `RB` et `RC`).

    1. Que devient la table de routage finale de `R1` (n'indiquer que le nom du routeur dans la dernière colonne) ?

        | Destination   | Métrique         | Prochain routeur     |
        |---------------|:----------------:|:---------------------|
        | 10.0.0.0/24   | 1                | RA                   |
        | 10.0.1.0/24   | {{rg_qnum(1,0)}} | {{rg_qsa(['RA'],3)}} |
        | 10.0.2.0/24   | {{rg_qnum(2,0)}} | {{rg_qsa(['RA'],3)}} |
        | 172.16.1.0/24 | {{rg_qnum(0,0)}} | -                    |
        | 172.16.2.0/24 | {{rg_qnum(2,0)}} | {{rg_qsa(['RA'],3)}} |
        | 192.168.1.0/24| {{rg_qnum(0,0)}} | {{rg_qsa(['-'],3)}}  |
        | 192.168.2.0/24| {{rg_qnum(3,0)}} | {{rg_qsa(['RA'],3)}} |



    1. Quel chemin suit un paquet pour aller `PC1` à `PCA` ?

        `PC1` &rarr; {{rg_qsa(['R1'],2)}} &rarr; {{rg_qsa(['RA'],2)}} &rarr; {{rg_qsa(['RC'],2)}} &rarr; {{rg_qsa(['R2'],2)}} &rarr; `PCA`

## 4. Protocole OSPF

(*Open Shortest Path First*)

!!! info "Idée" 

    Comme les liens peuvent avoir des débits très différents, il est judicieux d'emprunter en priorité ceux à débits maximaux.

La métrique utilisée pour définir l'état d'un lien est **inversement proportionnelle à son débit binaire** maximal *D* (souvent improprement qualifié de *bande-passante*).

!!! info "Exemple de métrique pour un lien"

    $$ metrique \simeq \frac{10^8}{D} \qquad\text{(avec $D$: débit binaire en bit/s)}$$

??? example "Exercice"

    Compléter le tableau suivant (arrondir en ne conservant que la partie entière, avec un minimum de 1):

    | **Débit** | 1 Gbps | 100 Mbps | 10 Mbps | 1.544 Mbps | 128 kbps | 64 kbps |
    |-----------|:------:|:--------:|:-------:|:----------:|:--------:|:-------:|
    | **Métrique** |{{rg_qnum(1,0)}}|{{rg_qnum(1,0)}}|{{rg_qnum(10,0)}}|{{rg_qnum(64,0)}}|{{rg_qnum(781,1)}}|{{rg_qnum(1562,1)}}|

Zone OSPF[^1] :

[^1]: Chaque routeur d'une zone OSPF possède un identifiant unique (32 bits en notation décimale pointée). Ex:
  1.1.1.1, 2.2.2.2&hellip;

![OSPF début](img/src/05-reseau_intermediaire_OSPF-debut.svg){width=80%}

Le processus OSPF comprend 3 phases successives où chaque routeur :

1. Découvre initialement tous les autres routeurs de sa zone &rarr; établissement de sa *table de voisinage*.

2. *Multidiffuse* ensuite ses informations OSPF à tous les autres routeurs de sa zone &rarr; mutualisation des connaissances sur les *états des liens*.

3. Exécute finalement l'**algorithme de Dijkstra** (cf plus tard) &rarr; remplissage de la table de routage avec les routes optimales.

![OSPF fin](img/src/05-reseau_intermediaire_OSPF-fin.svg){width=80%}

!!! warning "Attention"

    Les routes statiques vers les réseaux directement connectés sont prioritaires vis à vis de celles apprises par le protocole OSPF et ont donc une métrique nulle.

??? example "Exercice"

    À ce stade, `PC1` n'est pas encore capable de communiquer avec `PCA` car le réseau intermédiaire n'a pas de routes pour `192.168.1.0/24` et `192.168.2.0/24`. Une solution consiste à activer aussi le protocole de routage OSPF sur `R1` et `R2` (en plus de `RA`, `RB` et `RC`).

    1. Que devient la table de routage finale de `R1` (n'indiquer que le nom du routeur dans la dernière colonne) ?

        | Destination   | Métrique          | Prochain routeur     |
        |---------------|:-----------------:|:---------------------|
        | 10.0.0.0/24   | {{rg_qnum(65,2)}} | {{rg_qsa(['RA'],3)}} |
        | 10.0.1.0/24   | {{rg_qnum(2,0)}}  | {{rg_qsa(['RA'],3)}} |
        | 10.0.2.0/24   | {{rg_qnum(3,0)}}  | {{rg_qsa(['RA'],3)}} |
        | 172.16.1.0/24 | {{rg_qnum(0,0)}}  | -                    |
        | 172.16.2.0/24 | {{rg_qnum(4,0)}}  | {{rg_qsa(['RA'],3)}} |
        | 192.168.1.0/24| {{rg_qnum(1,0)}}  | {{rg_qsa(['-'],3)}}  |
        | 192.168.2.0/24| {{rg_qnum(5,0)}}  | {{rg_qsa(['RA'],3)}} |



    1. Quel chemin suit un paquet pour aller `PC1` à `PCA` ?

        `PC1` &rarr; {{rg_qsa(['R1'],2)}} &rarr; {{rg_qsa(['RA'],2)}} &rarr; {{rg_qsa(['RB'],2)}} &rarr; {{rg_qsa(['RC'],2)}} &rarr; {{rg_qsa(['R2'],2)}} &rarr; `PCA`
