# Chiffrement asymétrique avec RSA

(**R**ivest, **S**hamir, **A**dleman, 1977)

<img src="https://snlpdo.fr/tnsi/img/13-rsa.svg" width="400" alt="RSA">

Il s'agit de l'algorithme de chiffrement asymétrique le plus utilisé de nos jours.

## I. Principe de fonctionnement

Les opérations de chiffrement et déchiffrement utilisent les
opérateurs **puissance** (`**`) et **modulo** (`%`) et s'effectuent de
la manière suivante:

```python
# Chiffrement
message_chiffré = (message_clair ** exposant_chiffrement) % module

# Déchiffement
message_déchiffré = (message_chiffré ** exposant_dechiffrement) % module
```

avec:

- le module est identique pour le chiffrement et le
  déchiffrement. C'est un entier de très grande taille (de préférence
  $\ge$ 1024 bits)

- `exposant_chiffrement` et `exposant_dechiffrement` sont
  **différents** et on ne peut pas (*facilement*) déduire l'un depuis
  l'autre. Le premier très souvent rendu **publique**
  (`publicExponent`, valeur classique=65537) et le deuxième reste
  **privé**.

!!! warning "Remarques pratiques"

    - Pour que ces opérations soit réversibles, il faut que `message` soit 
	plus petit que `module` (ou décomposé en valeurs plus petites que 
	`module`). En pratique, on utilise surtout RSA pour chiffrer des 
	clés symétriques ou des empreintes numériques (~256 bits).
	- Pour accélérer les opérations de chiffrement et déchiffrement, on 
	utilise un algorithme d'*exponentiation rapide*  au lieu des opérateurs 
	*puissance* et *modulo* classiques (Cf. fonction `pow` à 3 arguments 
	de Python).

## II. Clés publique et privée

Le module (`modulus`) est une donnée publique. Pour assurer la **confidentialité** d'un message:

- l'exposant de chiffrement est publique (`publicExponent`, de valeur
  habituelle 65537). La **clé publique** correspond au couple
  (`modulus`, `publicExponent`)
- l'exposant de déchiffrement est privé (`privateExponent`). Cette
  valeur étant beaucoup plus grande que l'exposant publique, on
  utilise généralement l'algorithme CRT (*Chinese Remainder Theorem*,
  basé sur 2 autres exposants `exponent1`, `exponent2` et un
  `coefficient`) pour accélérer l'opération de déchiffrement. La **clé
  privée** contient au minimum le couple (`modulus`, `publicExponent`)
  mais bien souvent aussi (`exponent1`, `exponent2`, `coefficient`)
  voire aussi `publicExponent` (pour information), `prime1` et
  `prime2`.

!!! warning "Remarque"

	Pour **authentifier** l'expéditeur à l'aide de RSA, le chiffrement 
	s'effectuerait avec la clé privée et le déchiffrement avec la clé 
	publique.

### 1. Détails mathématiques

*(cette section est très largement hors programme pour la Terminale
NSI)*

La génération des clés s'effectue de la manière suivante:

1. On choisit 2 nombres **premiers** `prime1` et `prime2` très grand et on pose:
    ```python
    modulus = prime1 * prime2
    ```
2. On choisit l'entier `publicExponent` tel qu'il soit premier avec le
   nombre `(prime1-1)*(prime2-1)` &rarr; on prend très souvent le 4ème
   nombre premier de Fermat (=65537 ou `F4`).

3. On déduit `privateExponent` en résolvant (le théorème de Bezout indique qu'il existe un entier `k` tel que) :
    ```python
    privateExponent * publicExponent = 1 + k*(prime1-1)*(prime2-1)
    ```

!!! info "Remarque"
    
	Les mathématiciens utilisent généralement d'autre notations

	- `prime1` &rarr; `p`
	- `prime2` &rarr; `q`
	- `modulus` &rarr; `n` (`n=p*q`)
	- `publicExponent` &rarr; `e`
	- `privateExponent` &rarr; `d`

### 2. Génération avec OpenSSL

Si nécessaire, installer le paquet sur Fedora avec:
```console
$ sudo dnf install openssl
```

Pour générer une clé privée avec un module de 1024 bits:


```python
!openssl genrsa -out 13-macleprivee.pem 1024
```

!!! info "Remarque"
    
	La clé privée est codée en *base64* (selon le format ASN.1) dans le
	fichier `macleprivee.pem`, entre les délimiteurs 
	`--BEGIN PRIVATEKEY--` et `--END PRIVATE KEY--`.

Manipulation des informations contenues dans ce fichier:

- extraction de la clé publique (dans le fichier `maclepublique.pem`):


```python
!openssl rsa -in 13-macleprivee.pem -pubout -out 13-maclepublique.pem
```

- afficher les paramètres RSA contenus dans ces 2 fichiers:


```python
!openssl rsa -in 13-maclepublique.pem -pubin -text
```


```python
!openssl rsa -in 13-macleprivee.pem -text
```

Décodez vous-même la clé privée en adaptant les instructions ci-après et retrouver `modulus` dans le résultat:


```python
with open('13-macleprivee.pem', 'r') as f:
    contenu = f.read()
    print(contenu)
    code64 = contenu.split("-----")[2]
    code64 = code64[1:-1].encode('ascii')

code64
```


```python
import base64

decodage = base64.b64decode(code64)
decodage.hex(sep=':')
```

`modulus` commence à l'octet 10 et fait une taille de 128 octets


```python
decodage[10:10+128].hex(sep=':')
```

## III. Transmission d'une clé symétrique à l'aide de RSA

!!! info "Remarque"
    
	Dans la pratique, les actions se font dans l'ordre suivant:
    
	<img src="https://snlpdo.fr/tnsi/img/13-partage.svg" width="600" alt="partage de clé symétrique"> 
    
	1. Le destinataire envoie sa clé RSA publique à l'expéditeur
	2. L'expéditeur:
		1. génère une clé symétrique puis 
		2. la chiffre avec la clé RSA publique.
		3. Envoie la version chiffrée au destinataire
	3. Le destinataire déchiffre la clé symétrique avec sa clé RSA privée.

1. Génération de la clé symétrique (=valeur aléatoire sur 256 bits) en 
hexadécimal

	```python
	import random
	clesym = random.randint(0,2**256)
	clesym = clesym.to_bytes(32, 'big')
	# Affichage en hexadéimal
	print(clesym.hex())
	
	# Enregistrement en hexadécimal dans le fichier clesymetrique.hex
	with open('13-clesymetrique.bin', 'w') as f:
		f.write(clesym.hex())
	```

2. Chiffrement RSA avec la clé publique.


	```python
	!openssl rsautl -encrypt -inkey 13-maclepublique.pem -pubin -in 13-clesymetrique.bin -out 13-clesymetrique.enc
	
	# Lecture de la clé chiffré
	with open('13-clesymetrique.enc','rb') as fichier:
		clesymenc = fichier.read()
		print(clesymenc)
	```

3. Déchiffrement RSA avec la clé privée.


	```python
	!openssl rsautl -decrypt -inkey 13-macleprivee.pem -in 13-clesymetrique.enc
	```

## IV. Module Python

Si nécessaire, installer le module Python avec:
```console
$ pip install pycryptodome
```


```python
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
```

- Génération des clés publique et privée:


```python
cleRSA = RSA.generate(1024)
```


```python
# Clé publique
cleRSA.publickey().exportKey()
```


```python
# Clé privée
cleRSA.exportKey()
```

- Récupérer une clé depuis un fichier PEM:


```python
cleRSA = RSA.import_key(open("13-macleprivee.pem").read())
```

- Paramètres RSA


```python
cleRSA.n.to_bytes(128, 'big').hex(sep=':')
```


```python
# Le module
cleRSA.n
```


```python
# Le produit des deux nombres premiers
cleRSA.p * cleRSA.q
```

- Chiffrement RSA avec la clé publique:


```python
rsa = PKCS1_OAEP.new(cleRSA.publickey())
clesymenc = rsa.encrypt(clesym)
clesymenc
```

- Déchiffrement RSA avec la clé privée:


```python
rsa2 = PKCS1_OAEP.new(cleRSA)
res = rsa2.decrypt(clesymenc)
res.hex()
```
