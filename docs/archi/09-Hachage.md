---
hide:
  - navigation
---

# Introduction au hachage

??? danger "Hachage != Chiffrement"

    Il s'agit de deux types de codage qui s'utilisent dans le domaine de la cryptographie mais :

    - le chiffrement est une opération réversible.
    - le hachage est opération irréversible.

## 1. Fonction de hachage

C'est une fonction mathématique unidirectionnelle. Le résultat d'un hachage s'appelle condensat (ou *hash*[^1]).

[^1]: ou *empreinte numérique* ou *digest* ou *checksum* ou *somme de contrôle*

Propriétés d'une *bonne* fonction de hachage[^2]:

[^2]: ces propriétés ne sont pas toutes nécessaires selon l'utilisation qu'il en est fait.

- La fonction doit être facilement calculable, mais difficilement réversible :

    <figure markdown>
    ![Fonction de hachage](img/src/09-hachage_reversibilite.svg){ width="700" }
    </figure>

- Les *condensats* d'une même fonction font tous la même longueur : 

    <figure markdown>
    ![Longueur de condensat](img/src/09-hachage-longueur.svg){ width="400" }
    </figure>

- Une variation, même très faible, de la valeur d'entrée donne un condensat très différent :

    <figure markdown>
    ![Variabilité du l'empreinte](img/src/09-hachage-modif.svg){ width="400" }
    </figure>

- Il est *impossible* (d'un point de vue calculatoire) de trouver une autre entrée donnant le même condensat (=**collision**).

    <figure markdown>
    ![Collisions](img/src/09-hachage-collision.svg){ width="300" }
    </figure>

## 2. Utilisations

Les fonctions de hachage s'utilisent pour :

- Stocker des mots de passe,
- vérifier l'intégrité de données, 
- détecter des doublons, 
- signer des documents numériques, 
- valider une *blockchain*,
- accéder rapidement à la valeur d'une clé dans un dictionnaire (Python),
- ...

### a. Empreinte numérique

??? tip "Identification du type d'un condensat"

    L'utilitaire Python `hashid` permet d'indentifier le type de condensats (parmis 210 supportés).

=== "MD5"

    *(Message Digest 5, Ron Rivest - 1991)*

    - Les empreintes ont une taille de 128 bits (16 octets, 32 chiffres hexadécimaux).
    - Considéré obsolète aujourd'hui (utilisé seulement si aucune autre alternative)

    Utilisations :

    - empreinte de fichier :

        ```{ .console .rgconsole }
        $ md5sum <fichier>
        ....
        $ openssl md5 <fichier>
        ....
        ```

    - empreinte d'une chaîne de caractères (ex : mot de passe) :

        ```{ .console .rgconsole }
        $ echo -n "Ma chaine de caractères" | md5sum
        e7dee73197cd8e96a27e040633f597a5
        $ echo -n "Ma chaine de caractères" | openssl md5
        e7dee73197cd8e96a27e040633f597a5
        ```

=== "SHA"

    *(Secure Hash Algorithm)*

    - Suite d'algorithmes développés par la NSA

        - SHA-1: en 1995, plusieurs versions, 160 bits (20 octets, 40 chiffres hexa), obsolète aujourd'hui, plus lent que MD5
        - SHA-2: SHA-224 (224 bits), SHA-256 (256 bits), SHA-384 (384 bits), SHA-512 (512 bits).
        - SHA-3 (créé par NIST): alternative/remplacement? de SHA-2 avec SHA3-224 (224 bits), SHA3-256 (256 bits), SHA3-384 (384 bits), SHA3-512 (512 bits).

    *[NSA]: National Security Agency

    Utilisations :

    - empreinte de fichier :

        ```{ .console .rgconsole}
        $ sha1sum <fichier>

        $ sha224sum <fichier>
        $ sha256sum <fichier>
        $ sha384sum <fichier>
        $ sha512sum <fichier>

        $ openssl sha1 <fichier>
        $ openssl sha224 <fichier>
        $ openssl sha256 <fichier>
        $ openssl sha384 <fichier>
        $ openssl sha512 <fichier>

        $ openssl sha3-224 <fichier>
        $ openssl sha3-256 <fichier>
        $ openssl sha3-384 <fichier>
        $ openssl sha3-512 <fichier>
        ```

### b. Dictionnaire Python

Pour stocker ou accéder a un couple clé/valeur en mémoire :

1. La fonction de hachage `hash()` est appliquée la clé pour générer un condensat (entier) *unique*.
2. Le condensat est transformé en indice qui pointe vers un emplacement mémoire où stocker le couple clé/valeur.
3. En cas de collision, le couple clé/valeur est *chaîné* à la suite des couples existants.

La complexité d'accès est de $O(1)$ en moyenne.
