---
hide:
  - navigation
---

# Routage[^1] informatique

*(Rappels et compléments de 2nde SNT/1ère NSI)*

[^1]: Dans cette leçon, le routage informatique ne concerne que les 
    communications inter-réseaux.  Les communications intra-réseau local par 
    encapsulation du *paquet* dans une trame (ici Ethernet) sont hors programme 
    NSI.

## 1. Adresse IP et masque

Un réseau informatique est constitué d'hôtes, regroupés dans un ou plusieurs LAN[^2], eux-mêmes interconnectés par des routeurs.

*[LAN]: Local Area Network, réseau local (de petites dimensions)

![Sous-réseaux](img/src/04-sous_reseaux.svg)

[^2]: Les sous-réseaux Ethernet sont bâtis autours de commutateur(s) (ou switch(es)).


??? info inline end "Adresses IPv4/IPv6"

    Deux familles d'adresses *coexistent* actuellement:

    - IPv4 (en voie d'abandon): 32 bits (4 octets au format décimal, 
    séparés par `.`).

        Exemple: `172.16.10.14`

    - IPv6 (remplace progressivement IPv4): 128 bits (8 hextets au format 
    hexadécimale, séparés par `:`)

        Exemple: `2001:0db8:acad:0010:1001:14fd:ffb4:0001`

    La suite de ce cours est uniquement illustré avec des adresses IPv4.

Chaque hôte d'un réseau possède un identifiant unique : une  **adresse IP** qui se décompose en deux parties :

![Décomposition adresse IP](img/src/04-decomposition_adresse_reseau.svg)

1. Un préfixe de sous-réseau : identique pour tous les hôtes dans le même sous-réseau.

2. Une terminaison d'hôte : unique dans un même sous-réseau.

La longueur de ses deux parties étant variable, il faut nécessairement connaître le **masque**[^3] du sous-réseau pour les identifier.

[^3]: Le masque à la même longueur que l'adresse IP qu'il accompagne : `1` ou `0` 
dans le masque indique respectivement une appartenance au préfixe ou à 
la terminaison pour le bit correspondant dans l'adresse IP.

??? info "Représentation du masque de réseau"

    Le masque de sous-réseau peut être indiqué :

    - en décimal pointé (comme l'adresse IPv4 qu'il accompagne). 
    - sous forme de longueur de préfixe (notation *CIDR*): la longueur 
    correspond au nombre de bits de poids fort à `1`. 

    Compléter :

    | Décimal pointé   | Binaire pointé | Longueur de préfixe   |
    |------------------|---------|------------------------------|
    | 255.255.255.0 | 11111111.11111111.11111111.00000000 | /24 |
    | 255.255.0.0   | {{rg_qsa(['11111111.11111111.00000000.00000000'],24)}} | {{rg_qsa(['/16'],3)}} |
    | {{rg_qsa(['255.255.255.128'],12)}}| 11111111.11111111.11111111.10000000 | /25 |
    | {{rg_qsa(['255.255.240.0'],12)}}| {{rg_qsa(['11111111.11111111.11110000.00000000'],20)}} | /20 |
        
??? example "Exemple de décomposition d'adresse IPv4"

    <table border=1>
    <tr><td align=center colspan=4>192.168.1.10</td></tr>
    <tr><td>1100 0000</td><td>1010 1000</td><td>0000 0001</td><td>0000 1010</td></tr>
    <tr><td align=center colspan=4>255.255.255.0</td></tr>
    <tr><td>1111 1111</td><td>1111 1111</td><td>1111 1111</td><td>0000 0000</td></tr>
    </table>

    ??? tip "Astuce"
    
        La décomposition peut s'effectuer directement en décimal lorsque le masque a une longueur multiple de 8.

        | Adresse IP     | Masque        | Indicatif | Terminaison |
        |----------------|---------------|:---------:|:-----------:|
        | 192.168.1.10   | 255.255.255.0 | 192.168.1 | 10          |
        | 172.16.124.103 | 255.255.0.0   | {{rg_qsa(['172.16'])}} | {{rg_qsa(['124.103'])}} |
        | 10.10.10.10    | 255.0.0.0     | {{rg_qsa(['10'])}} | {{rg_qsa(['10.10.10'])}} | 


## 2. Adresses dans un sous-réseau

### a. Adresse de réseau

!!! danger "Définition"

    Un sous-réseau possède sa propre adresse. Elle s'obtient:
    
    -  à partir de l'IP et du masque de sous-réseau de n'importe lequel de ses hôtes (opération *ET logique*):

        $$ \text{adresse sous-réseau} = \text{adresse hôte} \quad AND\quad \text{masque sous-réseau} $$

    - à partir du préfixe en complétant avec des bits à 0 (jusqu'à avoir 32 bits). La terminaison de l'adresse de sous-réseau est donc nulle. 

    ??? warning inline end "Attention"
    
        L'adresse de sous-réseau ne peut être attribuée à aucun hôte.

    Elle correspond à la plus petite adresse (=la première) du sous-réseau.


??? example "Exemples et exercice"

    === "Exemple 1"

        ![Adresse réseau exemple 1](img/src/04-adresse_reseau_ex1.svg)

    === "Exemple 2"

        ![Adresse réseau exemple 1](img/src/04-adresse_reseau_ex2.svg)

        Résultat (décimal pointé): {{rg_qsa(['192.168.144.0'],12)}}

    ??? example "Calculer les adresses de réseaux"

        | Adresse hôte    | Longueur de préfixe | Adresse de sous-réseau             |
        |-----------------|---------------------|------------------------------------|
        | 192.168.200.139 | /24                 | {{rg_qsa(['192.168.200.0'],15)}}   |
        | 10.101.99.228   | {{rg_qsa(['/8'],3)}}| 10.0.0.0                           |
        | 128.107.0.55    | /16                 | {{rg_qsa(['128.107.0.0'],15)}}     |
        | 172.22.32.12    | /19                 | {{rg_qsa(['172.22.32.0'],15)}}     |
        | 192.168.1.245   | /30                 | {{rg_qsa(['192.168.1.244'],15)}}   |
        | 192.135.250.180 | /29                 | {{rg_qsa(['192.135.250.176'],15)}} | 

Une adresse de sous-réseau (avec son masque, ou sa longueur de préfixe) permet de *résumer* tous les hôtes d'un même sous-réseau (notamment pour les opérations de routage).

![Sous-réseaux résumés](img/src/04-sous_reseaux-resumes.svg)

### b. Adresse de diffusion

(ou *broadcast*)

!!! danger "Définition"

    Il s'agit d'une autre adresse spécifique du sous-réseau qui permet de communiquer avec tous les hôtes. 
    
    Elle s'obtient à partir du préfixe en complétant avec des bits à 1 (jusqu'à avoir 32 bits).

    ??? warning inline end "Attention"
    
        L'adresse de diffusion ne peut être attribuée à aucun hôte.

    Elle correspond à la plus grande adresse (=la dernière) du sous-réseau.

### c. Nombre maximal d'hôtes

Le nombre maximal d'adresses disponibles dans un sous-réseau est limitée par la taille de la partie terminaison. De plus, deux adresses ne peuvent pas être attribuées (adresses de réseau et de diffusion).

!!! danger "Résultat"

    Dans un réseau de longueur de préfixe /n[^6] on a, au maximum:

    $$ 2^{32-n} - 2 \qquad\text{adresses d'hôtes disponibles}$$

[^6]: Une adresse IPv4 étant codée sur 32 bits, si le préfixe a une longueur de *n* bits, la partie terminaison a une longueur de *32-n* bits.

??? example "Calculer le nombre maximal d'adresses d'hôtes disponibles"

    | Longueur de préfixe | Nombre d'hôtes      | 
    |---------------------|---------------------|
    | /24                 | {{rg_qnum(254, 0)}} |
    | /16                 | {{rg_qnum(65534, 0)}} |
    | /28                 | {{rg_qnum(14, 0)}} |
    | /30                 | {{rg_qnum(2, 0)}} |

Dans un sous-réseau:

- La première adresse d'hôte disponible est celle juste après l'adresse de réseau.
- La dernière adresse d'hôte disponible est celle juste avant l'adresse de diffusion.

## 3. Route et table de routage

!!! danger "Définition"

    ![Panneau routier](img/src/04-route.svg){ align="left" }

    Une **route** correspond à :

    1. Une destination : l'adresse et le masque d'une machine ou d'un réseau.
    2. Une direction :

        - si la destination est dans un autre sous-réseau: identification du prochain routeur pour y accéder (=**passerelle**)
        - sinon : accès direct (le réseau est dit *directement connecté*).

La **table de routage** d'un hôte donne la liste de toutes les routes qu'il connaît. Il l'utilise pour savoir comment envoyer un paquet à un destinataire donné.

??? question inline end 

    Quel est le numéro de la route qu'empruntera un paquet à destination de 192.168.2.110 ? {{rg_qnum(2,0)}}

| No | Destination      | Prochain routeur | *commentaires*                              | 
|:--:|------------------|:----------------:|---------------------------------------------|
| 1  | 192.168.1.0/24   |  -               | *directement connecté*                      |
| 2  | 192.168.2.0/24   |  -               | *directement connecté*                      |
| 3  | 172.16.0.0/16    | 192.168.1.1      | *IP de la passerelle dans le LAN de l'hôte* |
| 4  | &hellip;         | &hellip;         | &hellip;                                    |


## 4. Passerelle et route par défaut

L'hôte n'a souvent qu'une vision locale du réseau. Il ne connaît que :

- Les routes des réseaux auxquels il appartient et
- une **route par défaut** (destination : `0.0.0.0/0`) pour atteindre tous les autres 
réseaux[^4]. 

[^4]: La route par défaut n'existe que si une **passerelle par défaut** 
(ou *default gateway*) est configurée sur l'hôte.

Exemple :

![Un routeur](img/src/04-un_routeur.svg)

??? example "Tables de routage"

    === "PC1"

        ??? tip "Vision locale du réseau par PC1"
            
            ![Vision PC1](img/src/04-vision_PC1.svg)

        | No | Destination                       | Passerelle                     | *commentaire*          |
        |:--:|-----------------------------------|:------------------------------:|------------------------|
        | 1  | {{rg_qsa(['192.168.1.0/24'],12)}} | -                              | *directement connecté* |
        | 2  | {{rg_qsa(['0.0.0.0/0'],12)}}      | {{rg_qsa(['192.168.1.1'],12)}} | *route par défaut*     | 

        Quelle est le numéro de la route que va emprunter le paquet pour atteindre `192.168.2.10` ? {{rg_qnum('2',0)}}



    === "R1"

        Le routeur ne connaît que les 2 sous-réseaux auxquels il appartient. Sa table de routage ne fait donc apparaître que 2 routes:

        | No | Destination                       | Passerelle | *commentaires*         |
        |:--:|-----------------------------------|:----------:|------------------------|
        | 1  | 192.168.1.0/24                    | -          | *directement connecté* |
        | 2  | {{rg_qsa(['192.168.2.0/24'],15)}} | -          | *directement connecté* |

        Quelle est le numéro de la route que va emprunter le paquet pour quitter R1 ? {{rg_qnum('2',0)}}

    === "PCA"

        | No | Destination                       | Passerelle                     | *commentaire*          |
        |:--:|-----------------------------------|:------------------------------:|------------------------|
        | 1  | {{rg_qsa(['192.168.2.0/24'],12)}} | -                              | *directement connecté* |
        | 2  | {{rg_qsa(['0.0.0.0/0'],12)}}      | {{rg_qsa(['192.168.2.1'],12)}} | *route par défaut*     | 


??? warning "Problème des 2 routeurs successifs"

    ![Deux routeurs](img/src/04-deux_routeurs.svg)

    Les tables de routages de PC1 et PCA sont identiques à celles du premier exemple:

    - PC1:

        | No | Destination    | Passerelle  | *commentaire*          |
        |:--:|----------------|:-----------:|------------------------|
        | 1  | 192.168.1.0/24 | -           | *directement connecté* |
        | 2  | 0.0.0.0/0      | 192.168.1.1 | *route par défaut*     | 
        
    - PCA:

        | No | Destination    | Passerelle  | *commentaire*          |
        |:--:|----------------|:-----------:|------------------------|
        | 1  | 192.168.2.0/24 | -           | *directement connecté* |
        | 2  | 0.0.0.0/0      | 192.168.2.1 | *route par défaut*     | 

    La difficulté de cette configuration est que R1 n'appartient pas au sous-réseau `192.168.2.0` et ne connaît, a priori, pas son existence (idem pour R2 vis-à vis de `192.168.1.0`).

## 5. Routage 

### a. Statique

!!! danger "Définition"

    Le terme routage statique désigne l'action de configurer manuellement des nouvelles routes sur un routeur.

Une première technique pour résoudre la difficulté des deux routeurs successifs est d'ajouter une *route par défaut* au routeur :

??? example "Table de routage de R1"

    Configurer la route par défaut sur R1 :

    | No | Destination                  | Passerelle                    | *commentaire*             |
    |:--:|------------------------------|:-----------------------------:|---------------------------|
    | 1  | 192.168.1.0/24               | -                             | *directement connecté*    |
    | 2  | 172.16.1.0/24                | -                             | *directement connecté*    |
    | 3  | {{rg_qsa(['0.0.0.0/0'],10)}} | {{rg_qsa(['172.16.1.2'],10)}} | *route par défaut via R2* | 

Une technique équivalente consiste à configurer sur le routeur une route explicite vers le sous-réseau inconnu :

??? example "Table de routage de R2"

    Configurer la route explicite sur R2 :

    | No | Destination                       | Passerelle                    | *commentaire*            |
    |:--:|-----------------------------------|:-----------------------------:|--------------------------|
    | 1  | 192.168.2.0/24                    | -                             | *directement connecté*   |
    | 2  | 172.16.1.0/24                     | -                             | *directement connecté*   |
    | 3  | {{rg_qsa(['192.168.1.0/24'],12)}} | {{rg_qsa(['172.16.1.1'],12)}} | *route explicite via R1* | 

### b. dynamique

Une troisième alternative au problème des routeurs successifs consiste à ce que:

- chaque routeur *informe* automatiquement ses *voisins* des routes dont il a connaissance (par exemple en envoyant sa table de routage courante) et que
- ces derniers utilisent cette information pour mettre à jour leur propre table (uniquement s'ils apprennent l'existence d'un nouveau sous-réseau ou une nouvelle route plus *intéressante*).

on parle alors de **routage dynamique** (cf. prochain cours).

??? example "Exercice"

    Un réseau est constitué de 5 hôtes dont les adresses et tables de routage sont:

    === "PC1" 
    
        Adresse: `192.168.1.10`

        | Destination    | Passerelle       |
        |----------------|:-----------------|
        | 192.168.1.0/24 | -                |  
        | 0.0.0.0/0      | R1               |  

    === "R1" 
    
        Adresses: `10.0.0.1`, `10.0.1.1` et `192.168.1.1`

        | Destination    | Prochain routeur |
        |----------------|:-----------------|
        | 10.0.0.0/24    | -                |  
        | 10.0.1.0/24    | -                |  
        | 192.168.1.0/24 | -                |  
        | 0.0.0.0/0      | R3               |  

    === "R2" 
    
        Adresses: `10.0.1.2` et `10.0.2.1`

        | Destination    | Prochain routeur |
        |----------------|:-----------------|
        | 10.0.0.0/24    | R1               |  
        | 10.0.1.0/24    | -                |  
        | 10.0.2.0/24    | -                |  
        | 192.168.1.0/24 | R1               |  
        | 192.168.1.0/24 | R3               |  

    === "R3" 
    
        Adresses: `10.0.0.2`, `10.0.2.2` et `192.168.2.1`

        | Destination    | Prochain routeur |
        |----------------|:-----------------|
        | 10.0.0.0/24    | -                |  
        | 10.0.1.0/24    | R1               |  
        | 10.0.2.0/24    | -                |  
        | 192.168.1.0/24 | R1               |  
        | 192.168.2.0/24 | -                |  

    === "PCA" 
    
        Adresse: `192.168.2.10`

        | Destination    | Passerelle       |
        |----------------|:-----------------|
        | 192.168.2.0/24 | -                |  
        | 0.0.0.0/0      | R3               |  

    1. À combien de sous-réseaux appartient chaque hôte ?
    2. Quelles sont les passerelles respectives de PC1 et PCA ?
    3. Dessiner la topologie de ce réseau
    4. Identifier le chemin complet (i.e. les hôtes traversés) pour un paquet allant de PC1 à PCA.
    5. Quelle pourrait être l'utilité du routeur R2 ?

    ??? tip "Réponses"

        1. PC1 : 1, R1 : 3, R2 : 2, R3 : 3, PCA : 1.
        2. PC1 : R1, PCA : R3.
        3. Topologie:

            ![Réseau mystère](img/src/04-trois_routeurs.svg)

        4. PC1 → R1 → R3 → PCA.
        5. R2 permet d’offrir une route de secours.

{{rg_corriger_page()}}
