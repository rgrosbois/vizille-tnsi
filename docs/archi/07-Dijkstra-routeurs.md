---
hide:
  - navigation
---
# Annexe : calcul d'itinéraire optimal par l'algorithme de Dijkstra

On étudie le réseau dont la topologie est la suivante :

![réseau simplifié](img/src/07-topologie_routeurs.svg){width=50%}

## I. Mémorisation de la topologie

2 représentations sont habituellement utilisées pour stocker les données d'un graphe :

- une matrice d'adjacence

- des listes de voisinages

=== "Matrice d'adjacence"

    On mémorise les distances[^1] entre chaque routeur dans une matrice (*liste 2D*) :

    |          | L        | A        | B        | C        | &hellip; |
    |----------|:--------:|:--------:|:--------:|:--------:|:--------:|
    | L        | $\infty$ | $\infty$ | 50       | $\infty$ | &hellip; |
    | A        | $\infty$ | $\infty$ | $\infty$ | 53       | &hellip; |
    | B        | 50       | $\infty$ | $\infty$ | 62       | &hellip; |
    | C        | $\infty$ | 53       | 62       | $\infty$ | &hellip; |
    | &hellip; | &hellip; | &hellip; | &hellip; | &hellip; | &hellip; |

    [^1]: une distance $\infty$ indique qu'il n'existe pas de liaison directe

    !!! info "Implémentation Python"
    
        1. On fixe un ordre, arbitraire, des routeurs à l'aide d'une liste :

            ```py
            routeurs = ['L', 'A', 'B', 'C', 'V', 'G', 'M', 'I', 'E', 'N']
            ```

            !!! example "Numéro d'un routeur"
            
                Compléter le code de la fonction `numero_routeur()`:

                ```py
                def numero_routeur(nom: str, liste_routeurs: list[str]) -> int:
                    """
                    Parcourir la liste jusqu'à retrouver la position d'un routeur spécifique 
                    (ou générer une erreur s'il n'existe pas).

                    >>> numero_routeur('L', routeurs)
                    0
                    >>> numero_routeur('V', routeurs)
                    4
                    >>> numero_routeur('F', routeurs)
                    Traceback (most recent call last):
                    ValueError: F n'est pas présent dans la liste
                    """
                    ...
                    
                    # Routeur non trouvé
                    raise ValueError(nom + " n'est pas présent dans la liste")
                ```

                ??? tip "Remarque"

                    La méthode {{rg_qsa(['index'], 10)}} de la classe {{rg_qsa(['list'], 10)}} réalise la même opération que cette fonction.

        2. On remplit la matrice (selon l'ordre des routeurs défini précédemment)
        
            a. Créér la matrice `topologie` (liste 2D) avec les bonnes dimensions et en initialisant tous les éléments avec `#!python float('inf')`:

            ```py
            topologie = ...
            ```

            b. Compléter le code de la fonction `ajouter_liaison` et ajouter les 14 liaisons ci-après :

            ```py
            def ajouter_liaison(graphe: list[list[int]], liste_routeurs:list[str],\
                depart:str, arrivee:str, distance:int):    
                # 1. Récupérer les indices des routeurs départ et arrivée
                i_dep = ...
                i_arr = ...

                # 2. Remplir les éléments correspondants dans la matrice
                ...

            # Ajouter les 14 liaisons de la topologie
            ajouter_liaison(topologie, routeurs, 'L', 'B', 50)
            ajouter_liaison(topologie, routeurs, 'L', 'E', 102)
            ajouter_liaison(topologie, routeurs, 'E', 'V', 87)
            ajouter_liaison(topologie, routeurs, 'E', 'I', 69)
            ajouter_liaison(topologie, routeurs, 'I', 'G', 39)
            ajouter_liaison(topologie, routeurs, 'B', 'V', 46)
            ajouter_liaison(topologie, routeurs, 'B', 'C', 62)
            ajouter_liaison(topologie, routeurs, 'V', 'G', 26)
            ajouter_liaison(topologie, routeurs, 'G', 'C', 60)
            ajouter_liaison(topologie, routeurs, 'G', 'N', 115)
            ajouter_liaison(topologie, routeurs, 'C', 'A', 53)
            ajouter_liaison(topologie, routeurs, 'C', 'M', 103)
            ajouter_liaison(topologie, routeurs, 'A', 'M', 90)
            ajouter_liaison(topologie, routeurs, 'M', 'N', 60)
            ```

            ??? example "Vérification"

                Créer une fonction pour afficher le contenu de `topologie`, ligne par ligne, chaque élément sur 3 caractères, séparé par un espace (`inf` pour une valeur infinie):

                ??? check "Résultat attendu"

                    ```
                    inf inf  50 inf inf inf inf inf 102 inf 
                    inf inf inf  53 inf inf  90 inf inf inf 
                     50 inf inf  62  46 inf inf inf inf inf 
                    inf  53  62 inf inf  60 103 inf inf inf 
                    inf inf  46 inf inf  26 inf inf  87 inf 
                    inf inf inf  60  26 inf inf  39 inf 115 
                    inf  90 inf 103 inf inf inf inf inf  60 
                    inf inf inf inf inf  39 inf inf  69 inf 
                    102 inf inf inf  87 inf inf  69 inf inf 
                    inf inf inf inf inf 115  60 inf inf inf 
                    ```
            
=== "Listes de voisinages"

    Pour chaque routeur, on mémorise sa liste de voisinage avec les distances à parcourir:

    | Routeur  | Voisinage                       |
    |----------|---------------------------------|
    | L        | B (50), E (102)                 |
    | A        | C (53), M (90)                  |
    | B        | L (50), V (46), C (62)          |
    | C        | B (62), G (60), A (53), M (103) |
    | &hellip; | &hellip;                        |

    !!! info "Implémentation Python"

        On utilise un dictionnaire de dictionnaires:

        - Le dictionnaire principal associe à chaque routeur de départ (=*clé*) sa liste de voisinage (=*valeur*).
        
        - Liste de voisinage: un dictionnaire associe à chaque routeur d'arrivée (=*clé*) la distance à parcourir (=*valeur*) depuis le routeur de départ.

        Compléter le code de la fonction `ajouter_liaison` afin de remplir le dictionnaire `topologie`:

        ```py
        def ajouter_liaison(dico: dict, depart: str, arrivee: str, distance: int):
            # à compléter
            ...

        topologie = {}

        # Ajouter les 14 routes
        ajouter_liaison(topologie, 'L', 'B', 50)
        ajouter_liaison(topologie, 'L', 'E', 102)
        ajouter_liaison(topologie, 'E', 'V', 87)
        ajouter_liaison(topologie, 'E', 'I', 69)
        ajouter_liaison(topologie, 'I', 'G', 39)
        ajouter_liaison(topologie, 'B', 'V', 46)
        ajouter_liaison(topologie, 'B', 'C', 62)
        ajouter_liaison(topologie, 'V', 'G', 26)
        ajouter_liaison(topologie, 'G', 'C', 60)
        ajouter_liaison(topologie, 'G', 'N', 115)
        ajouter_liaison(topologie, 'C', 'A', 53)
        ajouter_liaison(topologie, 'C', 'M', 103)
        ajouter_liaison(topologie, 'A', 'M', 90)
        ajouter_liaison(topologie, 'M', 'N', 60)
        ```

## II. Algorithme de Moore-Dijkstra

Durant l'exécution de l'algorithme il faut se souvenir, pour chaque routeur, des 3 éléments suivants :

- Sa distance au routeur de départ ;
- S'il a déjà été *visité* ;
- Le routeur précédent dans l'itinéraire (optimal).

=== "Matrice d'adjacence"

    Cette mémorisation se fait à l'aide de 3 listes, toujours indexées selon le même ordre des routeurs.

    1. Compléter la fonction de création de ces 3 listes:

        ```py
        def init_algo(liste_routeurs: list[str]) -> (list[float], list[bool], list[int]):
            """
            Créer et renvoyer 3 nouvelles listes (de même taille que liste_routeurs) permettant de mémoriser pour chaque routeur:
            - sa distance au routeur de départ (infinie par défaut)
            - s'il a déjà été visités (faux par défaut)
            - l'index du routeur précédent dans le parcours (-1 si pas de parent)
            """
            dist = [float('inf') for v in liste_routeurs]
            vis = # à compléter
            prec = # à compléter

            return dist, vis, prec
        ```

    2. Compléter la fonction d'exécution de l'algorithme:

        ```py
        def dijkstra(depart: str, liste_routeurs: list[str],\
            graphe: list[list[float]]) -> (list[float], list[int]):
            """
            Créer l'arbre de Dijkstra connaissant:
            - depart: le routeur de départ 
            - liste_routeurs: la liste ordonnées des routeurs
            - graphe: la matrice d'adjacence de la topologie.

            Renvoie la liste des distances et des parents de chaque routeur.
            """
            # Initialiser l'algorithme
            distance, visite, parent = init_algo(liste_routeurs)
            
            # Indiquer une distance nulle pour le routeur de départ
            id_depart = numero_routeur(depart, liste_routeurs)
            ## à compléter
            
            while                            # Tant qu'il reste des routeurs non visités
                
                # 1) Identifier le (premier) routeur de distance minimale (non visité)
                dist_min = float('inf')
                i_min = -1
                ## à compléter
                
                
                ##
                visite[i_min] = True

                # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
                # a. Mettre à jour la distance
                # b. Mémoriser le routeur de distance minimale en tant que parent
                for i in range(len(graphe[i_min])):
                    # à compléter
                    
                    ##
                        
            return distance, parent
        ```

=== "Listes de voisinages"

    Cette mémorisation se fait à l'aide de 3 dictionnaires dont les clés sont les routeurs de la topologie.

    1. Compléter la fonction de création des ces 3 dictionnaires:

        ```py
        def init_algo(graphe: dict[dict]) -> (dict[str,float], dict[str,bool], dict[str,str]):
            """
            Créer et renvoyer 3 nouveaux dictionnaires pour mémoriser:
            - la distance au routeur de départ (infinie par défaut)
            - les routeurs visités (vrai ou faux)
            - Le nom du routeur précédent dans le parcours (None si pas de parent)
            """
            dist = { routeur:float('inf') for routeur in graphe }
            vis = # à compléter
            par = # à compléter

            return dist, vis, par  
        ```

    2. Compléter la fonction d'exécution de l'algorithme:

        ```py
        def dijkstra(depart: str, graphe: dict[dict]) -> (dict[str,float], dict[str,str]):
            """
            Créer l'arbre de Dijkstra connaissant:
            - depart: le routeur de départ 
            - graphe: les dictionnaires de voisinages.

            Renvoie les dictionnaires des distances et des parents de chaque routeur.
            """
            # Initialiser l'algorithme
            distance, visite, parent = init_algo(graphe)
            
            # Indiquer une distance nulle pour le routeur de départ
            ## à compléter
            
            while                            # Tant qu'il reste des routeurs non visités
                
                # 1) Sélectionner le (premier) routeur non visité de distance minimale
                dist_min = float('inf')
                routeur_min = None
                for routeur in graphe:
                    ## à compléter
                    
                    
                    ##
                visite[routeur_min] = True

                # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
                # a. Mettre à jour la distance
                # b. Mémoriser le routeur de distance minimale en tant que parent
                for routeur_dst in graphe[routeur_min]:
                    # à compléter
                    
                    ##
                        
            return distance, parent
        ```

Utiliser cette fonction pour identifier le routeur de cette topologie le plus éloigné de `G`:

| Routeur | Distance |
|-------|:---------|
| {{rg_qsa(['M'], 10)}} | {{rg_qnum(163,0)}} |

## III. Itinéraires optimaux

!!! info "Représentation d'un itinéraire" 

    Nous utiliserons une liste de tuples où chaque tuple est de la forme (`routeur`, `distance`).

    Par exemple, pour l'itinéraire `L` &rarr; `B` &rarr; `V` &rarr; `G`:

    ```python
    it = [('L', 0), ('B', 50), ('V', 96), ('G', 122)]
    ```

Compléter la fonction qui retourne l'itinéraire optimal entre 2 routeurs quelconques :

=== "Matrice d'adjacence"

    ```py
    def itineraire(depart: str, arrivee: str, liste_routeurs: list[str],\
        graphe: list[list[float]]) -> list[tuple]:
        """
        Calcule et renvoie l'itinéaire optimal entre 2 routeurs.
        """

        # Générer l'arbre de dijkstra du routeur de départ 
        # (rem: non nécessaire si déjà calculé pour ce départ)
        distance, parent = # à compléter
        
        # Index du routeur d'arrivée
        id_arrivee = # à compléter
        
        # Reconstruction de l'itinéraire (en partant de l'arrivée)
        parcours = []
        i = id_arrivee
        id_depart = numero_routeur(depart, liste_routeurs)
        while             # Revenir jusqu'au routeur de départ
            # à compléter
            
            ##
            
        # Ajouter le routeur de départ
        parcours.insert(0,(depart, 0))
        
        return parcours
    ```

=== "Listes de voisinages"

    ```py
    def itineraire(depart: str, arrivee: str, graphe: dict[dict]) -> list[tuple]:
        """
        Calcule et renvoie l'itinéraire optimal entre 2 routeurs.
        """

        # Générer l'arbre de dijkstra du routeur de départ 
        # (rem: non nécessaire si déjà calculé pour ce départ)
        distance, parent = # à compléter
        
        # Reconstruction de l'itinéraire (en partant de l'arrivée)
        parcours = []
        routeur = arrivee
        while routeur != depart:
            ## à compléter
            
            ##
            
        # Ajouter le routeur de départ
        parcours.insert(0,(depart, 0))
        
        return parcours
    ```

Utiliser cette fonction pour compléter l'itinéraire optimal entre `L` et `M`:

| Étape                | Distance           |
|----------------------|:------------------:|
| L                    | 0                  |
| {{rg_qsa(['B'],10)}} | {{rg_qnum(50,0)}}  | 
| {{rg_qsa(['C'],10)}} | {{rg_qnum(112,0)}} | 
| {{rg_qsa(['M'],10)}} | {{rg_qnum(215,0)}} | 
