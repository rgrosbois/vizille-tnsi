---
hide:
  - navigation
---
# Annexe : calcul d'itinéraire optimal par l'algorithme de Dijkstra

On donne la carte routière simplifiée suivante :

![carte routière simplifiée](img/src/07-itineraire_routier-dark.svg){width=80%}

## I. Mémorisation de la carte routière

2 représentations sont habituellement utilisées pour stocker les données d'un graphe :

- une matrice d'adjacence

- des listes de voisinages

=== "Matrice d'adjacence"

    On mémorise les distances[^1] entre chaque ville dans une matrice (*liste 2D*) :

    |             | Lyon     | Albertville | Bourgoin | Chambéry | &hellip; |
    |-------------|:--------:|:-----------:|:--------:|:--------:|:--------:|
    | Lyon        | $\infty$ | $\infty$    | 50       | $\infty$ | &hellip; |
    | Albertville | $\infty$ | $\infty$    | $\infty$ | 53       | &hellip; |
    | Bourgoin    | 50       | $\infty$    | $\infty$ | 62       | &hellip; |
    | Chambéry    | $\infty$ | 53          | 62       | $\infty$ | &hellip; |
    | &hellip;    | &hellip; | &hellip;    | &hellip; | &hellip; | &hellip; |

    [^1]: une distance $\infty$ indique qu'il n'existe pas de route directe

    !!! info "Implémentation Python"
    
        1. On ordonne, une fois pour toute, les villes de la carte dans une liste :

            ```py
            villes = ['Lyon', 'Albertville', 'Bourgoin-Jallieu', 'Chambéry', 'Voiron', 'Grenoble','Modane', 'Villard de Lans', 'Valence', 'Briançon']
            ```

            ??? example "Index d'une ville"
            
                Compléter le code de la fonction `index()`:

                ```py
                def index(nom: str, liste_villes: list[str]) -> int:
                    """
                    Parcourir une liste jusqu'à retrouver un nom spécifique et retourner son index
                    (-1 s'il n'existe pas)
                    """
                    ...
                    
                    # Ville non trouvée
                    return -1
                ```

        2. On remplit la matrice (en fonction de l'ordre des villes précédent)
        
            a. Initialiser tous les éléments de `carte` (liste 2D) avec la valeur `#!python float('inf')`:

            ```py
            carte = ...
            ```

            b. Ajouter les 14 routes en complétant le code de la fonction `ajouter_route` :

            ```py
            def ajouter_route(graphe: list[list[str]], liste_villes:list[str],\
                depart:str, arrivee:str, distance:int):    
                # 1. Identifier les indices des villes de départ et arrivée
                ...

                # 2. Remplir les éléments correspondants
                ...

            # Ajouter les 14 routes de la carte
            ajouter_route(carte, villes, 'Lyon', 'Bourgoin-Jallieu', 50)
            ajouter_route(carte, villes, 'Lyon', 'Valence', 102)
            ajouter_route(carte, villes, 'Valence', 'Voiron', 87)
            ajouter_route(carte, villes, 'Valence', 'Villard de Lans', 69)
            ajouter_route(carte, villes, 'Villard de Lans', 'Grenoble', 39)
            ajouter_route(carte, villes, 'Bourgoin-Jallieu', 'Voiron', 46)
            ajouter_route(carte, villes, 'Bourgoin-Jallieu', 'Chambéry', 62)
            ajouter_route(carte, villes, 'Voiron', 'Grenoble', 26)
            ajouter_route(carte, villes, 'Grenoble', 'Chambéry', 60)
            ajouter_route(carte, villes, 'Grenoble', 'Briançon', 115)
            ajouter_route(carte, villes, 'Chambéry', 'Albertville', 53)
            ajouter_route(carte, villes, 'Chambéry', 'Modane', 103)
            ajouter_route(carte, villes, 'Albertville', 'Modane', 90)
            ajouter_route(carte, villes, 'Modane', 'Briançon', 60)
            ```

            ??? example "Vérification"

                Afficher le contenu de `carte`, ligne par ligne, chaque élément sur 3 caractères séparé par un espace (`inf` pour une valeur infinie):

                ??? check "Résultat attendu"

                    ```
                    inf inf  50 inf inf inf inf inf 102 inf 
                    inf inf inf  53 inf inf  90 inf inf inf 
                     50 inf inf  62  46 inf inf inf inf inf 
                    inf  53  62 inf inf  60 103 inf inf inf 
                    inf inf  46 inf inf  26 inf inf  87 inf 
                    inf inf inf  60  26 inf inf  39 inf 115 
                    inf  90 inf 103 inf inf inf inf inf  60 
                    inf inf inf inf inf  39 inf inf  69 inf 
                    102 inf inf inf  87 inf inf  69 inf inf 
                    inf inf inf inf inf 115  60 inf inf inf 
                    ```
            
=== "Listes de voisinages"

    Pour chaque ville, on mémorise sa liste de voisinage avec les distances à parcourir:

    | Ville       | Voisinage                                                    |
    |-------------|--------------------------------------------------------------|
    | Lyon        | Bourgoin (50), Valence (102)                                 |
    | Albertville | Chambéry (53), Modane (90)                                   |
    | Bourgoin    | Lyon (50), Voiron (46), Chambéry (62)                        |
    | Chambéry    | Bourgoin (62), Grenoble (60), Albertville (53), Modane (103) |
    | &hellip;    | &hellip;                                                     |

    !!! info "Implémentation Python"

        On utilise un dictionnaire de dictionnaires:

        - Le dictionnaire principal associe à chaque ville de départ (=*clé*) sa liste de voisinage (=*valeur*).
        
        - Liste de voisinage: un dictionnaire associe à chaque ville d'arrivée (=*clé*) la distance à parcourir (=*valeur*) depuis la ville de départ.

        Compléter le code de la fonction `ajouter_route` afin de remplir le dictionnaire `carte`:

        ```py
        carte = {}

        def ajouter_route(dico: dict, depart: str, arrivee: str, distance: int):
            # à compléter
            ...

        # Ajouter les 14 routes
        ajouter_route(carte, 'Lyon', 'Bourgoin-Jallieu', 50)
        ajouter_route(carte, 'Lyon', 'Valence', 102)
        ajouter_route(carte, 'Valence', 'Voiron', 87)
        ajouter_route(carte, 'Valence', 'Villard de Lans', 69)
        ajouter_route(carte, 'Villard de Lans', 'Grenoble', 39)
        ajouter_route(carte, 'Bourgoin-Jallieu', 'Voiron', 46)
        ajouter_route(carte, 'Bourgoin-Jallieu', 'Chambéry', 62)
        ajouter_route(carte, 'Voiron', 'Grenoble', 26)
        ajouter_route(carte, 'Grenoble', 'Chambéry', 60)
        ajouter_route(carte, 'Grenoble', 'Briançon', 115)
        ajouter_route(carte, 'Chambéry', 'Albertville', 53)
        ajouter_route(carte, 'Chambéry', 'Modane', 103)
        ajouter_route(carte, 'Albertville', 'Modane', 90)
        ajouter_route(carte, 'Modane', 'Briançon', 60)
        ```

## II. Algorithme de Moore Dijkstra

Durant l'exécution de l'algorithme il faut se souvenir, pour chaque ville, des 3 éléments suivants:

- sa distance à la ville de départ,
- si elle a déjà été *visitée*,
- la ville précédente dans l'itinéraire (optimal).

=== "Matrice d'adjacence"

    Cette mémorisation se fait à l'aide de 3 listes, toujours indexées avec le même ordre des villes.

    1. Compléter la fonction de création de ces 3 listes:

        ```py
        def init_algo(liste_villes: list[str]) -> (list[float], list[bool], list[int]):
            """
            Créer et renvoyer 3 nouvelles listes (de même taille que liste_villes) pour mémoriser:
            - la distance à la ville de départ (infinie par défaut)
            - les villes visitées (faux par défaut)
            - l'index de la ville précédente dans le parcours (-1 si pas de parent)
            """
            dist = [float('inf') for v in liste_villes]
            vis = # à compléter
            prec = # à compléter

            return dist, vis, prec
        ```

    2. Compléter la fonction d'exécution de l'algorithme:

        ```py
        def dijkstra(depart: str, liste_villes: list[str],\
            graphe: list[list[float]]) -> (list[float], list[int]):
            """
            Créer l'arbre de Dijkstra connaissant:
            - depart: la ville de départ 
            - liste_villes: la liste ordonnées des villes
            - graphe: la matrice d'adjacence de la carte.

            Renvoie la liste des distances et des parents de chaque ville.
            """
            # Initialiser l'algorithme
            distance, visite, parent = init_algo(liste_villes)
            
            # Indiquer une distance nulle pour la ville de départ
            id_depart = index(depart, liste_villes)
            ## à compléter
            
            while                            # Tant qu'il reste des villes non visités
                
                # 1) Sélectionner la (première) ville non visitée de distance minimale
                dist_min = float('inf')
                i_min = -1
                ## à compléter
                
                
                ##
                visite[i_min] = True

                # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
                # a. Mettre à jour la distance
                # b. Mémoriser la ville de distance minimale en tant que parent
                for i in range(len(graphe[i_min])):
                    # à compléter
                    
                    ##
                        
            return distance, parent
        ```

=== "Listes de voisinages"

    Cette mémorisation se fait à l'aide de 3 dictionnaires dont les clés sont les villes de la carte.

    1. Compléter la fonction de création des ces 3 dictionnaires:

        ```py
        def init_algo(graphe: dict[dict]) -> (dict[str,float], dict[str,bool], dict[str,str]):
            """
            Créer et renvoyer 3 nouveaux dictionnaires pour mémoriser:
            - la distance à la ville de départ (infinie par défaut)
            - les villes visitées (vrai ou faux)
            - Le nom de la ville précédente dans le parcours (None si pas de parent)
            """
            dist = { ville:float('inf') for ville in graphe }
            vis = # à compléter
            par = # à compléter

            return dist, vis, par  
        ```

    2. Compléter la fonction d'exécution de l'algorithme:

        ```py
        def dijkstra(depart: str, graphe: dict[dict]) -> (dict[str,float], dict[str,str]):
            """
            Créer l'arbre de Dijkstra connaissant:
            - depart: la ville de départ 
            - graphe: les dictionnaires de voisinages.

            Renvoie les dictionnaires des distances et des parents de chaque ville.
            """
            # Initialiser l'algorithme
            distance, visite, parent = init_algo(graphe)
            
            # Indiquer une distance nulle pour la ville de départ
            ## à compléter
            
            while                            # Tant qu'il reste des villes non visités
                
                # 1) Sélectionner la (première) ville non visitée de distance minimale
                dist_min = float('inf')
                ville_min = None
                for ville in graphe:
                    ## à compléter
                    
                    
                    ##
                visite[ville_min] = True

                # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
                # a. Mettre à jour la distance
                # b. Mémoriser la ville de distance minimale en tant que parent
                for ville_dst in graphe[ville_min]:
                    # à compléter
                    
                    ##
                        
            return distance, parent
        ```

Utiliser cette fonction pour identifier la ville de cette carte la plus éloignée de Grenoble:

| Ville | Distance |
|-------|:---------|
| {{rg_qsa(['Modane'], 10)}} | {{rg_qnum(163,0)}} |

## III. Itinéraires optimaux

!!! info "Représentation d'un itinéraire" 

    Nous utiliserons une liste de tuples où chaque tuple est de la forme (`ville`, `distance`).

    Par exemple, pour l'itinéraire `Lyon` &rarr; `Bourgoin-Jallieu` &rarr; `Voiron` &rarr; `Grenoble`:

    ```python
    it = [('Lyon', 0), ('Bourgoin Jallieu', 50), ('Voiron', 96), ('Grenoble', 122)]
    ```

Compléter la fonction qui retourne l'itinéraire optimal entre 2 villes quelconques:

=== "Matrice d'adjacence"

    ```py
    def itineraire(depart: str, arrivee: str, liste_villes: list[str],\
        graphe: list[list[float]]) -> list[tuple]:
        """
        Calcule et renvoie l'itinéaire optimal entre 2 villes.
        """

        # Générer l'arbre de dijkstra de la ville de départ 
        # (rem: non nécessaire si déjà calculé pour ce départ)
        distance, parent = # à compléter
        
        # Index de la ville d'arrivée
        id_arrivee = # à compléter
        
        # Reconstruction de l'itinéraire (en partant de l'arrivée)
        parcours = []
        i = id_arrivee
        id_depart = index(depart, liste_villes)
        while             # Revenir jusqu'à la ville de départ
            # à compléter
            
            ##
            
        # Ajouter la ville de départ
        parcours.insert(0,(depart, 0))
        
        return parcours
    ```

=== "Listes de voisinages"

    ```py
    def itineraire(depart: str, arrivee: str, graphe: dict[dict]) -> list[tuple]:
        """
        Calcule et renvoie l'itinéraire optimal entre 2 villes.
        """

        # Générer l'arbre de dijkstra de la ville de départ 
        # (rem: non nécessaire si déjà calculé pour ce départ)
        distance, parent = # à compléter
        
        # Reconstruction de l'itinéraire (en partant de l'arrivée)
        parcours = []
        ville = arrivee
        while ville!=depart:
            ## à compléter
            
            ##
            
        # Ajouter la ville de départ
        parcours.insert(0,(depart, 0))
        
        return parcours
    ```

Utiliser cette fonction pour compléter l'itinéraire optimal entre Lyon et Modane:

| Étape                               | Distance           |
|-------------------------------------|:------------------:|
| Lyon                                | 0                  |
| {{rg_qsa(['Bourgoin-Jallieu'],10)}} | {{rg_qnum(50,0)}}  | 
| {{rg_qsa(['Chambéry'],10)}}         | {{rg_qnum(112,0)}} | 
| {{rg_qsa(['Modane'],10)}}           | {{rg_qnum(215,0)}} | 
