---
hide:
  - navigation
---

<small>*(source : cours de J. DIRAISON)*</small>

---

# Introduction aux processus via la CLI (Linux)

!!! danger "Définition"
    Un processus est un programme qui s'exécute en disposant de ses propres 
    ressources[^1], allouées par le système d'exploitation.

[^1]: cycle de processeur, mémoire vive, fichiers (entrées-sorties, sockets 
réseau&hellip;)

---

??? tip "Rappels sur la CLI"

    L'invite de commande (ou *CLI*) est accessible depuis un terminal (ex: 
    *lxterminal*, *gnome-terminal*, *xterm*&hellip;) et débute par un *prompt* :
    
    ??? info inline end "Détails"

        - nom de l'utilisateur : `nsi`
        - nom de l'ordinateur : `localhost`
        - répertoire actuel : `~/` (= `/home/nsi`, zone personne de l'utilisateur)
        - `$` : l'utilisateur n'a pas de privilège particulier (`#` pour 
        un administrateur)

    ```{.console .rgconsole}
    [nsi@localhost:~]$
    ```

    Elle permet de lancer des programmes ou d'exécuter des primitives du shell (*commandes*).

La CLI permet d'effectuer de nombreuses opérations impliquant des processus :

- Créer un nouveau processus (lancer un programme),
- Suspendre un processus temporairement,
- Interrompre définitivement un processus,
- Lister les différents processus en action,
- Visualiser les informations propres à chaque processus.

*[CLI]: Interface en ligne de commande

## 1. Création

Pour lancer un programme, il faut saisir son nom dans le *shell* [^2] :

??? info inline end "Détails"

    - 1ère ligne: lancement du programme `sleep` (chemin non spécifié) avec 
    l'argument `10` &rarr; attente d'un certain nombre de secondes puis arrêt.

    - 2nde ligne: lancement du programme `echo` en spécifiant le chemin absolu.

    - 3ème ligne: lancement du programme `echo` sans spécifier son emplacement (ne fonctionne que si ce dernier est présent dans la variable `PATH`).

    - 4ème ligne: affichage du contenu de la variable d'environnement `PATH`. 
    À noter l'ajout du caractère `$` devant le nom de la variable (note: ici le 
    programme `echo` utilisé est celui du shell)


```{.console .rgconsole title=Terminal}
[nsi@localhost:~]$ sleep 10
[nsi@localhost:~]$ /usr/bin/echo "NSI"
NSI
[nsi@localhost:~]$ echo "NSI"
NSI
[nsi@localhost:~]$ echo $PATH
/home/nsi/bin:/usr/local/bin:/usr/bin:/bin
```

[^2]: préciser le chemin *relatif* ou *absolu* si le répertoire contenant le 
programme n'est pas renseigné dans la variable d'environnement `PATH`.

## 2. Tâche de fond

Pour éviter que l'exécution du programme ne monopolise le terminal, on peut 
choisir de le lancer en *arrière-plan* (ou *tâche de fond*) en 
ajoutant le caractère `&` à la fin de la ligne de commande.

??? info inline end "Détails"

    - 1ère ligne: le lancement en arrière-plan affiche:
        - l'index du processus (entre crochets) dans le terminal (ici `1`)
        - l'identifiant du processus (`PID`) attribué par le système 
        d'exploitation (ici `4129`)

    - 2ème ligne: la commande `fg` suivi de l'index du processus (précédé de 
    `%`) permet de refaire passer le processus en *avant-plan* dans le terminal.

```{.console .rgconsole title="Terminal"}
[nsi@localhost:~]$ sleep 120 &
[1] 4129
[nsi@localhost:~]$ fg %1
```

## 3. Code de terminaison

Lorsqu'un processus se termine, il renvoie au processus parent un code de retour (*0* si tout s'est bien passé). 
 
> Dans le shell, le code de retour de la dernière commande est contenu dans la variable `$?`.

=== "Fin normale"

    ??? info inline end "Détails"

        - 1ère ligne: lister tous les sous-répertoires du répertoire racine 
        contenant le caractère `r`.

        - 2ème ligne: dans le *shell*, la variable `?` contient le code de retour.

    ```{.console .rgconsole title="Terminal"}
    [nsi@localhost:~]$ ls -d /*r*
    /proc  /root  /run  /srv  /usr  /var
    [nsi@localhost:~]$ echo $?
    0
    ```

=== "Fin anormale"

    ??? info inline end  "Détails"

        La commande `ls` tente de lister le fichier `reponses.txt` qui n'existe pas. 
        Elle renvoie au shell la valeur `2` (erreur d'exécution).

        !!! warning "Attention"

            Les valeurs de codes de retour peuvent différer selon les programmes.

    ```{.console .rgconsole title="Terminal"}
    [nsi@localhost:~]$ ls responses.txt
    ls: impossible d'accéder à 'reponses.txt': Aucun fichier ou dossier de ce type
    [nsi@localhost:~]$ echo $?
    2
    ```


## 4. Suspension et arrêt

Il est parfois utile de mettre fin à un processus [^3] :

[^3]: lorsque le processus ne répond plus, semble tourner en rond, prend trop 
de temps, consomme trop de ressources (CPU, RAM, disque), semble faire des 
choses inattendues&hellip;

- À l'aide d'une combinaison de touches (pour un processus en avant-plan) : 

    - Terminaison prématurée : ++ctrl+c++ (signal `SIGINT`)
    
        ```{.console .rgconsole title="Terminal"}
        [nsi@localhost:~]$ sleep 120
        ^C
        ```

    - Suspension et récupération de la main dans le terminal : ++ctrl+z++ 
    (signal `SIGTSTP`)

        ```{.console .rgconsole title="Terminal"}
        [nsi@localhost:~]$ sleep 120
        ^Z
        [1]+ Stoppé             sleep 120
        [nsi@localhost:~]$ bg %1
        ```

- En envoyant un *signal* à l'aide de la commande `kill` (processus en 
*arrière-plan* ou dans un autre terminal) :

    ```{.console .rgconsole title="Terminal"}
    [nsi@localhost:~]$ sleep 120 &
    [1] 4129
    ```

    === "Terminer prématurément"

        ```{.console .rgconsole}
        [nsi@localhost:~]$ kill -SIGINT 4129
        ```

    === "Suspendre"

        ```{.console .rgconsole}
        [nsi@localhost:~]$ kill -SIGTSTP 4129
        ```

        pour reprendre 


        ```{.console .rgconsole}
        [nsi@localhost:~]$ kill -SIGCONT 4129
        ```

    === "Terminer brutalement"

        *(à éviter sauf cas extrême)*

        ```{.console .rgconsole}
        [nsi@localhost:~]$ kill -9 4129
        ```

    ??? tip "Signaux possibles"

        La liste des signaux disponibles est donnée par la commande `kill -l`

            1) SIGHUP	    2) SIGINT	    3) SIGQUIT	    4) SIGILL	    5) SIGTRAP
            6) SIGABRT	    7) SIGBUS	    8) SIGFPE	    9) SIGKILL	    10) SIGUSR1
            11) SIGSEGV	    12) SIGUSR2	    13) SIGPIPE	    14) SIGALRM	    15) SIGTERM
            16) SIGSTKFLT	17) SIGCHLD	    18) SIGCONT	    19) SIGSTOP	    20) SIGTSTP
            21) SIGTTIN	    22) SIGTTOU	    23) SIGURG	    24) SIGXCPU	    25) SIGXFSZ
            26) SIGVTALRM	27) SIGPROF	    28) SIGWINCH	29) SIGIO	    30) SIGPWR
            31) SIGSYS	    34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
            38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
            43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
            48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
            53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
            58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
            63) SIGRTMAX-1	64) SIGRTMAX	

    ??? example "Utilisation d'un signal dans un script Python"
        Il faut définir une fonction de gestion du signal et l'activer à
        l'aide de la fonction `signal` du module `signal`.

        Exemple :
        ```python linenums="1" title="gestion_signal.py"
        import signal
        import time

        compteur = 0

        def gestionnaire(signum, frame):
            print("compteur=", compteur)

        # utilisation du signal SIGUSR1
        signal.signal(signal.SIGUSR1, gestionnaire)
            
        for _ in range(100):
            compteur += 1
            time.sleep(1)
        ```

        Lancer ce programme en arrière-plan et attendre quelques secondes 
        (ici 9s) et envoyer le bon signal au processus :

        ```{.console .rgconsole}
        [nsi@localhost:~]$ python gestion_signal.py &
        [1] 501530
        [nsi@localhost:~]$ kill -SIGUSR1 501530
        compteur= 9
        ```

## 5. Inspection

Plusieurs commandes permettent de visualiser la liste des processus *tournant* 
sur le système : `ps`, `pstree`, `top`, `htop`&hellip;

```{.console hl_lines="6" .rgconsole title="Terminal"}
[nsi@localhost:~]$ ps ux
USER    PID %CPU %MEM    VSZ   RSS   TTY  STAT     START   TIME COMMAND
nsi    2317  0.0  0.0  24436 13788 ?        Ss   sept.03   0:04 /usr/lib/systemd/systemd --user
nsi    2322  0.0  0.0 200068  4176 ?        S    sept.03   0:00 (sd-pam)
nsi  129919  0.0  0.0 233068  5676 pts/5    Ss     12:41   0:00 bash
nsi  129924  0.0  0.0 220708   760 pts/5    S+     12:41   0:00 sleep 120
nsi  129935  0.0  0.0 235436  3984 pts/4    R+     12:41   0:00 ps ux
```

??? tip "Informations sur un processus"

    Le noyau Linux maintient pour chaque processus un répertoire virtuel 
    `/proc/PID` (où `PID` est l'identifiant du processus) qui permet d'accéder 
    à de nombreuses informations concernant le processus lui-même.

    ```{.console .rgconsole} 
    [nsi@localhost:~]$ ls -l /proc/129924
    ```

    En lisant le contenu du fichier `/proc/129924/cmdline` on apprend que le 
    programme `/usr/bin/sleep` a été appelé avec les arguments `sleep` (nom du 
    programme) et `120` (le temps d'attente).

    À tester:
    ```{.console .rgconsole} 
    [nsi@localhost:~]$ cat /proc/129924/environ | tr "\0" "\n"
    ...
    ```
