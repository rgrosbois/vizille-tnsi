# Chiffrement de César

## 1. Généralités <a name="generalites"></a>

Il s'agit d'une des plus ancienne technique de chiffrage décrite à ce jour. Elle utilise la **substitution mono-alphabétique**: chaque lettre de l'alphabet est remplacée (*substituée*) par une seule autre lettre (*mono-alphabétique*) du même alphabet obtenu par **décalage** (=*clé*).

César aurait eu pour habitude d'utiliser un décalage de 3 lettres vers la gauche (*d* donne *a*, *e* donne *b*, *f* donne *c*...), mais il est possible d'utiliser n'importe quelle autre valeur de décalage.

![Roue de César](img/roue_cesar.svg)

## 2. Alphabet de travail <a name="alphabet"></a>

Les chaînes de caractères Python correspondent à la classe `str` où
chaque caractère est codé selon le standard *unicode*.

- Pour obtenir le code d'une lettre, on peut utiliser la fonction `#!py3 ord()`:
```python
ord('a')
```

- Pour obtenir la lettre correspondant à une code, on peut utiliser la
  fonction `#!py3 chr()`:
```python
chr(97)
```

??? note "Travail à faire" 
    Afficher le caractère correspondant à
    chaque code entre *32* et *126* (inclus) en utilisant une boucle
    `#!py3 for` et l'une des fonctions ci-dessus.

	Exemple:

    | Code     | Lettre   |
	|----------|----------|
	| &hellip; | &hellip; |
	| 97       | a        |
	| &hellip; | &hellip; |


    ```python
	# Lignes de code à compléter
	for ...
	```

Ces caractères constituent l'**alphabet** pour la suite de ce travail.

??? info "Question"
    Quelle est sa taille ?

	```
	Nombre de caractères = 
	```

!!! tip "Fonction fournie"

	La fonction `#!py3 normalisation()` ci-après permet de restreindre la 
	plupart des chaînes de caractères à cet alphabet .


	```python
	import unicodedata
	
	def normalisation(s):
      '''Enlever les caractères accentuées'''
      return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
    ```
	
	Utilisation:
	
	```python
	normalisation("Il s'agit d'une des plus ancienne technique de chiffrage décrite à ce jour")
	```

## 3. Chiffrement <a name="chiffrement"></a>
    
Corriger et compléter la fonction ci-après afin qu'elle opère correctement sur les exemples fournis dans la docstring.


```python
def chiffrement(chaine, decalage):
    """
    Décale chaque caractère de la chaîne selon la valeur spécifiée dans `decalage`.
    
    >>> chiffrement('def', 3)
    'abc'
    >>> chiffrement('def', 4)
    '`ab'
    >>> chiffrement('Décodage', 3)
    'Ab`la^db'
    >>> chiffrement('!', 3)
    '}'
    """
    codage = ''
    for c in chaine2:
        code = ord(c)-3
        codage += chr(code)
    return codage
```

## 4. Déchiffrement <a name="dechiffrement"></a>
    
Il s'agit de l'opération inverse de `chiffrement`.
    
Compléter la fonction ci-après. Vous avez le droit de vous aider de la fonction `chiffrement`.


```python
def dechiffrement(chaine, decalage):
    # À compléter 
    
```

## 5. Décryptage <a name="decryptage"></a>

La chaîne suivante a été chiffrée selon l'algorithme de César (mais le décalage est inconnu). 
```python
'|@SMS5S5I>CIF8Z<I=SHFC=GS79BHSEI5F5BH9`<I=HS5BGSG=LSAC=GS9HS8=L`B9I:S>CIFGSEI9S@9GSD5F=G=9BGSGZ9J9=@@9F9BHS5IS6FI=HS89SHCIH9GS@9GS7@C7<9GSGCBB5BHS5S;F5B89SJC@99S85BGS@5SHF=D@9S9B79=BH9S89S@5Sv=H9_S89S@Z)B=J9FG=H9S9HS89S@5S*=@@9aSv9SBZ9GHS79D9B85BHSD5GSIBS>CIFS8CBHS@Z<=GHC=F9S5=HS;5F89SGCIJ9B=FSEI9S@9SiS>5BJ=9FSdgkeaS&=9BS89SBCH56@9S85BGS@Z9J9B9A9BHSEI=SA9HH5=HS5=BG=S9BS6F5B@9_S89GS@9SA5H=B_S@9GS7@C7<9GS9HS@9GS6CIF;9C=GS89S$5F=GaSv9SBZ9H5=HSB=SIBS5GG5IHS89SD=75F8GSCIS89S6CIF;I=;BCBG_SB=SIB9S7<5GG9SA9B99S9BSDFC79GG=CB_SB=SIB9SF9JC@H9S8Z97C@=9FGS85BGS@5SJ=;B9S89S 55G_SB=SIB9S9BHF99S89SBCHF98=HSHF9GSF98CIH9SG9=;B9IFSACBG=9IFS@9SFC=_SB=SA9A9SIB9S69@@9SD9B85=GCBS89S@5FFCBGS9HS89S@5FFCBB9GG9GS5S@5S}IGH=79S89S$5F=GaSv9SBZ9H5=HSD5GSBCBSD@IGS@5SGIFJ9BI9_SG=S:F9EI9BH9S5ISEI=BN=9A9SG=97@9_S89SEI9@EI9S5A65GG589S7<5A5FF99S9HS9AD5B57<99aS|@SMS5J5=HS5SD9=B9S89ILS>CIFGSEI9S@5S89FB=9F9S75J5@7589S89S79S;9BF9_S79@@9S89GS5A65GG589IFGS:@5A5B8GS7<5F;9GS89S7CB7@IF9S@9SA5F=5;9S9BHF9S@9S85ID<=BS9HS!5F;I9F=H9S89Sy@5B8F9_S5J5=HS:5=HSGCBS9BHF99S5S$5F=G_S5IS;F5B8S9BBI=S89S!CBG=9IFS@9S75F8=B5@S89SuCIF6CB_SEI=_SDCIFSD@5=F9S5ISFC=_S5J5=HS8IS:5=F9S6CBB9SA=B9S5SHCIH9S79HH9SFIGH=EI9S7C<I9S89S6CIF;A9GHF9GS:@5A5B8G_S9HS@9GSF9;5@9F_S9BSGCBS<CH9@S89SuCIF6CB_S8ZIB9SACI@HS69@@9SACF5@=H9_SGCH=9S9HS:5F79_SH5B8=GSEIZIB9SD@I=9S65HH5BH9S=BCB85=HS5SG5SDCFH9SG9GSA5;B=:=EI9GSH5D=GG9F=9GaS 9SiS>5BJ=9F_S79SEI=SA9HH5=HS9BS9ACH=CBSHCIHS@9SDCDI@5=F9S89S$5F=G_S7CAA9S8=HS}9<5BS89S(FCM9G_S7Z9H5=HS@5S8CI6@9SGC@9BB=H9_SF9IB=9S89DI=GSIBSH9ADGS=AA9ACF=5@_S8IS>CIFS89GS&C=GS9HS89S@5Sy9H9S89GSyCIGaSv9S>CIF`@5_S=@S89J5=HSMS5JC=FS:9IS89S>C=9S5S@5SzF9J9_SD@5BH5H=CBS89SA5=S5S@5S7<5D9@@9S89SuF5EI9S9HSAMGH9F9S5IS$5@5=GS89S}IGH=79aS 9S7F=S9BS5J5=HS9H9S:5=HS@5SJ9=@@9S5SGCBS89SHFCAD9S85BGS@9GS75FF9:CIFG_SD5FS@9GS;9BGS89S!CBG=9IFS@9SDF9JCH_S9BS695ILS<CEI9HCBGS89S75A9@CHSJ=C@9H_S5J97S89S;F5B89GS7FC=LS6@5B7<9GSGIFS@5SDC=HF=B9a'
```
Identifier le **décalage** utilisé et retrouver le **message en clair**.


```python

```
