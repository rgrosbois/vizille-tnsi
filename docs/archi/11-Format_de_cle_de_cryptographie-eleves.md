# Format de clé de cryptographie

Une clé d'algorithme de (dé-)chiffrement est un entier de très grande taille

- chiffrement symétrique: $\leq$ 256 bits (32 octets)
- chiffrement asymétrique: $\ge$ 1024 bits (128 octets)

## I. Séquence d'octets (`bytes`)

Prenons l'exemple d'un entier sur 256 bits (exécuter plusieurs fois si 
nécessaire):

```python
import random
i256 = random.randint(0, 2**256)
print(i256)
```

- La *séquence* de 32 octets (type `bytes`) est obtenue avec:

    ```python
	octets = i256.to_bytes(32, 'big')
	print(octets)
	```

	!!! info "Précisisions"

        L'affichage de la séquence fait apparaitre soit des caractères ASCII 
		(lorsqu'ils sont affichables), soit les codes hexadécimaux (2 chiffres 
		précédés par `\x`).

- Pour récupérer la chaîne (type `str`) de codes hexadécimaux:

	```python
	octets.hex()
	```

	!!! tip ""
	    et pour utiliser un séparateur entre octets:

		```python
		octets.hex(sep=':')
		```

!!! alert "Travail à effectuer"
    
    Compléter les instructions pour afficher, en hexadécimal, les 256
	octets d'un entier aléatoire sur 2048 bits.


	```python
	# Instructions à compléter
	i2048 = 

	```

**Problématique:** quel format utiliser pour stocker ou transférer
cette information *de nature binaire* sous une forme textuelle
(c.à.d. avec uniquement des codes ASCII affichables) ?


```python
chaine = str(i256)
len(chaine)
```

Nous allons nous intéresser aux deux représentations suivantes:

- [II. Chaîne de caractères hexadécimaux](#hexa)
- [III. Codage en base64](#base64)

## II. Chaîne de caractères hexadécimaux <a name="hexa"></a>

Dans cette représentation, la valeur hexadécimale de chaque octet est
directement écrite en ASCII (uniquement avec les caractères: `0`,
`1`&hellip;`9`, `A`, `B`&hellip;`F`).

Quelques considérations (il faut 2 caractères par octet):

- un entier de 256 bits (32 octets) est représenté par: 64 caractères.
- un entier de 1024 bits (128 octets) est représenté par: 256 caractères.

Quelques fonctions Python utiles:

- La fonction `hex(x)` permet d'obtenir diretement la chaîne
  hexadécimale (préfixée par `0x`) correspondant à l'entier `x`.


    ```python
	h = hex(i256)
	print(h)
	```

	!!! tip "Note"

	    Pour enlever les 2 premiers caractères (`0x`), on peut utiliser 
		un *slice*:

		```python
		h[2:]
		```

!!! alert "Travail à effectuer"
    
    Utiliser la fonction `hex()` pour afficher la chaîne hexadécimale, 
	sans le préfixe `0x`, correspondant à l'entier `i2048`

	```python
	# à compléter
	hex()
	```

- la fonction `int(x, 16)` permet d'obtenir l'entier correspondant à
	une chaîne de caractères hexadécimaux `x` (préfixée ou non par
	`0x`)

	```python
	int('0x2912', base=16)
	```

## III. Codage en base64 <a name="base64"></a>

Il faut 2 caractères pour représenter un octet avec une chaîne hexadécimale. Peut-on faire mieux ?

- 1 caractère pour 1 octet ? &rarr; il n'y a pas 256 caractères affichables dans la table ASCII

- 1 caractère pour 6 bits &rarr; codage *base64*

Le principe consiste à regrouper les bits par 6 et de remplacer chaque groupe par un caractère ASCII affichable (caractère *base64*):

- Il faut (au minimum) 64 caractères ASCII affichables. En fait, un 65ème (`=`) sert de caractère de *complétion*.
- Les caractères ASCII utilisés sont (dans l'ordre): `A`, `B`&hellip;`Z`, `a`, `b`&hellip;`z`, `0`, `1`&hellip;`9`, `+`, `/` (et `=`)

Les instructions suivant permette de remplir une liste `b64` avec les caractères *base64* (dans l'ordre):


```python
# Initialisation
b64 = []
# Caractères de A à Z
for i in range(ord('A'), ord('Z')+1):
    b64.append(chr(i))
# Caractères de a à z
for i in range(ord('a'), ord('z')+1):
    b64.append(chr(i))
# Chiffres de 0 à 9
for i in range(10):
    b64.append(str(i))
# 3 derniers caractères
b64.append('+')
b64.append('/')
b64.append('=')
```

On vérifie que la liste fait la bonne taille


```python
assert len(b64)==65, "La liste ne fait pas la bonne taille"
```

!!! alert "Travail à effectuer"

    Compléter les instructions pour peupler le dictionnaire `codes` qui 
	associe à chaque caractère *base64* (=clé) son indice (=valeur) dans la 
	liste `b64` précédente:


	```python
	codes = {}
	# à compléter
    
	```

Vérifier certaines entrées:


```python
assert codes['A']==0,  "A n'est pas associé au bon indice"
assert codes['a']==26, "a n'est pas associé au bon indice"
assert codes['=']==64, "= n'est pas associé au bon indice"
```

Pour effectuer ce codage:

1. on fusionne 3 octets consécutifs pour former un entier de 24 bits
2. l'entier est ensuite divisé en 4 blocs de 6 bits.

![Codage base64](img/11-base64.svg)

Le contenu de chaque bloc s'obtient à l'aide d'opérations de décalage et de masque binaire:

```python
bloc4 = entier24       & 0b00111111
bloc3 = (entier24>>6)  & 0b00111111
bloc2 = (entier24>>12) & 0b00111111
bloc1 = (entier24>>18) & 0b00111111
```

La fonction `bytes3_vers_base64()` ci-après permet de convertir 3 octets (=*séquence d'octets* de longueur 3) en 4 caractères *base64*, selon les 3 étapes suivantes:

1. Récupération de la valeur de l'entier représenté par ces 3 octets.
2. Découpage de cette valeur en 4 blocs de 6 bits à l'aide des opérateurs de décalage et de masques binaires appropriés.
3. Renvoi des 4 caractères *base64* en utilisant la liste `b64`.


```python
def bytes3_vers_base64(octets):
    assert len(octets) == 3, "Mauvais nombre d'octets"
    
    # Étape 1
    val_entier = int(octets.hex(), base=16)
    
    # Étape 2
    b1 = (val_entier>>18)&0x3F
    b2 = (val_entier>>12)&0x3F
    b3 = (val_entier>>6)&0x3F
    b4 = val_entier&0x3F
    
    # Étape 3
    return b64[b1]+b64[b2]+b64[b3]+b64[b4]
```

On teste la fonction:


```python
a = b'\xc1\x01\x45' # correspond à 0xc10145
bytes3_vers_base64(a)
```

On valide ce résultat avec celui de la fonction `b64encode` du module `base64` de Python:


```python
import base64
base64.b64encode(a)
```

La fonction `int256_vers_base64` ci-après permet de convertir un entier de 256 bits (32 octets) en caractères *base64*, selon les 4 étapes suivantes:

1. Conversion de l'entier en une séquence de 32 octets.
2. Pour arriver à une longueur de 33 octets (c.à.d. multiple de 3), complétion de cette séquence en suffixant avec **un** octet nul (`b'\x00'`).
3. Traitement de la séquence par blocs de 3 octets et concaténation des résultats des appels à la fonction `bytes3_vers_base64`.
4. Remplacement du dernier octet par **un** caractère `=` (pour indiquer que la séquence avait été *complétée* avec **un** octet nul).


```python
def int256_vers_base64(val):
    # Étape 1: conversion de val en une séquence de 32 octets
    octets = val.to_bytes(32, 'big')
    
    # Étape 2: complétion avec un octet nul
    octets += b'\x00'

    # Étape 3: concaténation des résultats de conversions 3 par 3
    conversion = ''
    for i in range(0,len(octets),3):
        conversion = conversion + bytes3_vers_base64(octets[i:i+3])
        
    # Étape 4: caractère de complétion
    conversion = conversion[:-1]+'='
    
    return conversion
```


```python
# Test sur l'entier de 256 bits généré au début du TP
int256_vers_base64(i256)
```

on compare le résultat avec celui de la fonction `b64encode` du module `base64` de Python.


```python
# import base64
res256 = base64.b64encode(i256.to_bytes(32, 'big'))
print(res256)
```

!!! alert "Travail à effectuer"
    
	Terminer la fonction `int2048_vers_base64` (en vous aidant de 
	`int256_vers_base64`) pour obtenir le codage *base64* d'une clé de 
	2048 bits (**attention:** il faut compléter la séquence avec 
	**2 octets nuls** pour obtenir une longueur divisible par 3). 

	```python
	def int2048_vers_base64(val):
		# Étape 1: conversion de val en une séquence de 256 octets
		octets = val.to_bytes(256, 'big')
    
		# Étape 2: complétion avec deux octets nuls
		octets += 
		
		# Étape 3: concaténation des résultats de conversions 3 par 3
		conversion = ''
			
			
			
		# Étape 4: placer les 2 caractères de complétion
		conversion = conversion[:-2]+'=='
		
		return conversion
	```


	```python
	# Test sur l'entier de 2048 bits généré dans la première partie
	int2048_vers_base64(i2048)
	```

	Comparaison avec la fonction `b64encode` du module `base64` de Python:

	```python
	# import base64
	res2048 = base64.b64encode(i2048.to_bytes(256, 'big'))
	print(res2048)
	```

!!! tip "Note"
	La fonction `b64decode` du module `base64` de Python permet d'effectuer 
	la conversion inverse:

	```python
	base64.b64decode(res256).hex()
	```
