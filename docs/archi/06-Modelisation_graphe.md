---
hide:
  - navigation
---

# Modélisation d'un réseau par un graphe

## 1. Hypothèse simplificatrice

On va simplifier l'étude en considérant les routeurs plutôt que les sous-réseaux en tant que destinations.

=== "Avant simplification"

    ![Topologie complète](img/src/06-topologie_complete.svg)

    Table de routage de `R1`:

    | Destination    | Métrique | Prochain saut    |
    |:---------------|:--------:|:----------------:|
    | 10.0.0.0/24    |    1     | 172.16.1.2       |
    | 10.0.1.0/24    |    1     | 172.16.1.2       |
    | 10.0.2.0/24    |    2     | 172.16.1.2       |
    | 172.16.1.0/24  |    0     | -                |
    | 172.16.2.0/24  |    2     | 172.16.1.2       |
    | 192.168.1.0/24 |    0     | -                |
    | 192.168.2.0/24 |    3     | 172.16.1.2       |


=== "Après simplification"

    ![Topologie complète](img/src/06-topologie_complete-simplifiee.svg)

    Table de routage de `R1`:

    | Destination | Métrique | Prochain routeur |
    |:-----------:|:--------:|:----------------:|
    | RA          | 1        | RA               |
    | RB          | 2        | RA               |
    | RC          | 2        | RA               |
    | R2          | 3        | RA               |

    ??? warning "Conséquences"

        - Certaines routes disparaissent de la table de routage (mais il sera possible de les retrouver ultérieurement à partir du routeur directement connecté le plus proche).
        - Les topologies ne font apparaître que des routeurs (plus de commutateur, ni de PC&hellip;).
        - Les adresses IP ne sont plus utiles (les mêmes raisonnements sont valables pour IPv4 et IPv6).
        - Les métriques s'appliquent aux liens entre routeurs (comme pour OSPF): on écrit généralement les valeurs à côté des liens correspondants.
        - La seule différence entre un réseau fonctionnant avec RIP ou avec OSPF est la pondération des liens.


??? example "Exercices"

    === "RIP"

        On modélise un réseau utilisant le protocole RIP. Compléter les métriques et les *prochains routeurs* de chaque table de routage.

        ![Graphe RIP](img/src/06-graphe_RIP.svg)

        === "RA"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          |  1             | R1                   |
            | RB          |{{rg_qnum(1,0)}}| {{rg_qsa(['RB'],3)}} |
            | RC          |{{rg_qnum(1,0)}}| {{rg_qsa(['RC'],3)}} |
            | R2          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |

        === "RB"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 2              | RA                   |
            | RA          |{{rg_qnum(1,0)}}| {{rg_qsa(['RA'],3)}} |
            | RC          |{{rg_qnum(1,0)}}| {{rg_qsa(['RC'],3)}} |
            | R2          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |

        === "RC"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 2              | RA                   |
            | RA          |{{rg_qnum(1,0)}}| {{rg_qsa(['RA'],3)}} |
            | RB          |{{rg_qnum(1,0)}}| {{rg_qsa(['RB'],3)}} |
            | R2          |{{rg_qnum(1,0)}}| {{rg_qsa(['R2'],3)}} |

        === "R2"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 3              | RC                   |
            | RA          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |
            | RB          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |
            | RC          |{{rg_qnum(1,0)}}| {{rg_qsa(['RC'],3)}} |

    === "OSPF"

        On modélise un réseau utilisant le protocole OSPF. Compléter les métriques et les *prochains routeurs* de chaque table de routage.

        ![Graphe OSPF](img/src/06-graphe_OSPF.svg)

        === "R1"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | RA          | 1              | RA                   |
            | RB          |{{rg_qnum(2,0)}}| {{rg_qsa(['RA'],3)}} |
            | RC          |{{rg_qnum(3,0)}}| {{rg_qsa(['RA'],3)}} |
            | R2          |{{rg_qnum(4,0)}}| {{rg_qsa(['RA'],3)}} |

        === "RA"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          |  1             | R1                   |
            | RB          |{{rg_qnum(1,0)}}| {{rg_qsa(['RB'],3)}} |
            | RC          |{{rg_qnum(2,0)}}| {{rg_qsa(['RB'],3)}} |
            | R2          |{{rg_qnum(3,0)}}| {{rg_qsa(['RB'],3)}} |

        === "RB"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 2              | RA                   |
            | RA          |{{rg_qnum(1,0)}}| {{rg_qsa(['RA'],3)}} |
            | RC          |{{rg_qnum(1,0)}}| {{rg_qsa(['RC'],3)}} |
            | R2          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |

        === "RC"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 3              | RB                   |
            | RA          |{{rg_qnum(2,0)}}| {{rg_qsa(['RB'],3)}} |
            | RB          |{{rg_qnum(1,0)}}| {{rg_qsa(['RB'],3)}} |
            | R2          |{{rg_qnum(1,0)}}| {{rg_qsa(['R2'],3)}} |

        === "R2"

            | Destination | Métrique       | Prochain routeur     |
            |:-----------:|:--------------:|:--------------------:|
            | R1          | 4              | RC                   |
            | RA          |{{rg_qnum(3,0)}}| {{rg_qsa(['RC'],3)}} |
            | RB          |{{rg_qnum(2,0)}}| {{rg_qsa(['RC'],3)}} |
            | RC          |{{rg_qnum(1,0)}}| {{rg_qsa(['RC'],3)}} |

On reconnaît des schémas de type **graphes pondérés** non orientés[^1]: la problématique du routage dans un réseau s'apparente donc à la recherche de chemin optimal dans un graphe pondéré.

[^1]: certains liens pourraient être orientés, par exemple pour une liaison ADSL.

## 2. Algorithme de Bellman-Ford (RIP)

??? definition "Wikipedia" 

    Routing Information Protocol (RIP, protocole d’information de routage) est un protocole de routage IP de
    type Vector Distance (à vecteur de distances) s’appuyant sur l’algorithme de détermination des routes décentralisé Bellman-Ford. Il permet à chaque routeur de communiquer aux routeurs voisins la métrique, c’est-à-dire la distance qui les sépare d’un réseau IP déterminé quant au nombre de sauts ou « hops » en anglais.

!!! danger "Algorithme"

    La connaissance de la destination se propage d'**un** routeur à chaque itération.

    1. on initialise un arbre avec, pour racine, le **routeur de destination**.

    2. on construit les niveaux successifs de l'arbre en intégrant progressivement les routeurs voisins (=parcours en **largeur**).

        Règles à respecter:

        1. on ne revient jamais en arrière

        2. on arrête d'explorer une branche si l'on retombe sur un routeur déjà rencontré précédemment (car on a alors nécessairement un coût supérieur).

    ??? tip "Astuce"
    
        on peut éventuellement symboliser (par exemple avec des pointillés) les chemins alternatifs (i.e. de même coût) menant vers un même routeur, mais **attention**, on perd alors la structure d'arbre (création d'un cycle).

On étudie par exemple la topologie suivante:

![Topologie Bellman-Ford](img/src/06-topologie_Bellman_Ford.svg)

??? example "Questions"

    1. Identifier tous les chemins optimaux et les distances depuis chaque routeur pour atteindre le réseau de `PC1` 
    
        (on pourra, dans un premier temps, simplifier en considérant que la destination est `RK`).

        ??? tip "Réponse"

            On obtient l'arbre: ![Arbre Bellman-Ford](img/src/06-arbre_Bellman_Ford.svg)

    2. Quel chemin optimal suit un paquet pour aller de `PC0` à `PC1` ? Quelle est sa longueur en nombre de sauts ?

        ??? tip "Réponse"

            `PC0` &rarr; `RA` &rarr; `RF` &rarr; `RE` &rarr; `RK` &rarr; `PC1` (longueur=4)

    3. Quel routeur est le plus éloigné de `PC1` ? Quelle est sa distance en nombre de sauts ? À quelle distance de `PC1` se trouve un PC directement connecté à ce routeur (en nombre de sauts)?

        ??? tip "Réponse"

            `RB` est le routeur le plus éloigné avec une distance de 4. Un PC directement connecté à `RB`se trouve à une distance de 5 de `PC1`.

## 3. Algorithme de Moore-Dijkstra (OSPF)

!!! danger "Algorithme"

    On créé un arbre avec le **routeur de départ** comme racine et on décore chaque n&oelig;ud avec la valeur du coût cumulatif.

    Étapes de construction:

    1. On sélectionne la racine (coût 0).
    2. Depuis le n&oelig;ud sélectionné, on dessine toutes les branches possibles (=1 arête + 1 n&oelig;ud et sa décoration) sans revenir en arrière.
    3. Si un routeur apparaît dans plusieurs n&oelig;uds, on ne conserve que le n&oelig;ud de coût le plus faible.
    4. On **sélectionne la feuille avec le coût le plus faible** et on recommence à l'étape 2 (on ne s'arrête que lorsqu'il n'existe plus de branche possible depuis les feuilles restantes).

On étudie le réseau suivant (exercice 5 du sujet 0 - modifié):

![Topologie Sujet 0 Exercice 5](img/src/06-topologie_sujet0-ex5.svg)

??? example "Questions"

    1. Identifier les chemins optimaux vers tous les autres routeurs depuis `A`.

        ??? tip "Réponse"

            Arbre: 
            
            ![Arbre A](img/src/06-arbre_dijkstraA.svg)

            Vision sous forme de tableau:


            | B        | C           | D        | E          | F           | G           | Choix | 
            |:--------:|:-----------:|:--------:|:----------:|:-----------:|:-----------:|:-----:|
            | **10/A** | 10 000/A    | 10/A     | $\infty$/- | $\infty$/-  | $\infty$/-  | **B** |
            |          | 10 000/A    | **10/A** | $\infty$/- | $\infty$/-  | $\infty$/-  | **D** |
            |          | 10 000/A    |          | **11/D**   | $\infty$/-  | $\infty$/-  | **E** |
            |          |  2 011/A    |          |            | $\infty$/-  | **1 011/e** | **G** |
            |          | **2 011/A** |          |            | 2 011/G     |             | **C** |
            |          |             |          |            | **2 011/G** |             | **F** |

    2. Quel chemin optimal suit un paquet issu d'un PC connecté à `A` pour atteindre un PC connecté à `F` ? Quel est son coût ?

        ??? tip "Réponse"

            `A` &rarr; `D` &rarr; `E` &rarr; `G` &rarr; `F` (coût=2 011)

    3. Identifier les plus courts chemin depuis `B`

??? example "Exercice"

    Identifier tous les chemins optimaux dans le réseau intermédiaire du cours.

    (la table de voisinage et le graphe pondéré non-orienté sont 2 représentations équivalentes)

    === "Table de voisinage"

        |    | RA | RB | RC |
        |:--:|:--:|:--:|:--:|
        | RA | 0  | 1  | 64 |
        | RB | 1  | 0  | 1  |
        | RC | 64 | 1  | 0  |

    === "Graphe pondéré"

        ![Graphe pondéré](img/src/06-graphe_pondere_reseau_intermediaire.svg)

        ??? tip "Réponse"

            ![Arbres du réseau intermédiaire](img/src/06-arbres_reseau_intermediaire.svg)