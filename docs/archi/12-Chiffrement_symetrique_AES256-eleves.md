# Chiffrement symétrique avec AES-256-CBC

*Advanced Encryption Standard*: basé sur l'algorithme Rijndael (Joan Daemen, Vincent Rijmen), sélectionné en 1997 par la U.S. NIST.

<img src="https://snlpdo.fr/tnsi/img/12-aes.svg" alt="AES" width="600">

Caractéristiques :

- Clé de 256 bits (autres possibilités: 128 bits et 192 bits)
- Blocs de 128 bits (*block cipher*).
- CBC (*Cipher Block Chaining*) est un des modes de fonctionnement d'AES avec :
    - un vecteur d'initialisation (aléatoire) de 128 bits et 
    - des messages de longueurs multiples de 128 bits.

!!! info "Quelques précisions"

    La clé **et** le vecteur d'initialisation doivent être transmis au 
	récepteur. Souvent:
    
	- la clé est transmise/générée au début de la communication (en utilisant 
	un algorithme asymétrique).
	- le vecteur d'initialisation est transmis en clair après chaque 
	réinitialisation.

Exemple de message:
```python
octets_message = """Le message à transmettre
sur plusieurs lignes avec des informations
confidentielles, telles que mon mot de passe, 
ou les coordonnées de ma carte bancaire, mot numéro
de compte...""".encode('utf-8')

len(octets_message)
```

## I. Rappels/compléments sur les fichiers binaires

- Créer un nouveau fichier `message.txt` contenant des octets &rarr; ouvrir en écrire un fichier binaire (mode `wb`)


```python
with open('12-message.txt', 'wb') as fichier:
    fichier.write(octets_message)
```

- Ouvrir un fichier contenant des octets &rarr; ouvrir en lecture un fichier binaire (mode `rb`):


```python
with open('12-message.txt', 'rb') as fichier:
    contenu = fichier.read()
    
print(contenu)
```

## II. Mise en &oelig;uvre avec OpenSSL

Si nécessaire, installer le paquet sur Fedora avec:
```console
$ sudo dnf install openssl
```

### 1. Génération des paramètres

- Clé aléatoire de 256 bits (32 octets) au format hexadécimal:


```python
!openssl rand -hex -out 12-clesymetrique.hex 32

# Affichage de la clé stockée dans le fichier
with open('12-clesymetrique.hex', 'rb') as fichier:
    print(fichier.read())
```

- Vecteur d'initialisation aléatoire de 128 bits (16 octets) au format
  hexadécimal:


```python
!openssl rand -hex -out 12-iv.hex 16

# Affichage du vecteur stocké dans le fichier
with open('12-iv.hex', 'rb') as fichier:
    print(fichier.read())
```

### 2. Chiffrement du message


```python
!openssl enc -aes-256-cbc -in 12-message.txt -out 12-message.txt.enc -K $(cat 12-clesymetrique.hex) -iv $(cat 12-iv.hex)

# Affichage du message chiffré dans le fichier
with open('12-message.txt.enc', 'rb') as fichier:
    print(fichier.read())
```

### 3. Déchiffrement du message


```python
!openssl enc -d -aes-256-cbc -in 12-message.txt.enc -out 12-recup_message.txt -K $(cat 12-clesymetrique.hex) -iv $(cat 12-iv.hex)

# Affichage du message récupéré
with open('12-recup_message.txt', 'rb') as fichier:
    print(fichier.read())
```

## III. Mise en &oelig;uvre avec Python

Si nécessaire, installer le module `pycryptodome`:
```console
$ pip install pycryptodome
```


```python
from Crypto.Cipher import AES
```

- Générer une clé aléatoire de 32 octets (256 bits) au format hexadécimal:


```python
import random
cle32 = random.randint(0,2**256)
cle32 = cle32.to_bytes(32, 'big')
cle32
```

- Générer un vecteur d'initialisation de 16 octets (128 bits) au format hexadécimal:


```python
iv16 = random.randint(0,2**128)
iv16 = iv16.to_bytes(16, 'big')
iv16
```

- Compléter le message pour qu'il ait une longueur multiple de 128 bits (16 octets). Les octets supplémentaires ont tous la même valeur correspondant au nombre d'octets ajoutés.


```python
longueur = 16 - (len(octets_message)%16)
message_complete = octets_message + bytes([longueur])*longueur
message_complete, len(message_complete)
```

- Chiffrement


```python
aes = AES.new(cle32, AES.MODE_CBC, iv=iv16)
message_chiffre = aes.encrypt(message_complete)
message_chiffre
```

- Déchiffrement:


```python
aes2 = AES.new(cle32, AES.MODE_CBC, iv=iv16)
aes2.decrypt(message_chiffre)
```

- Mise en &oelig;uvre avec la clé et le vecteur générés avec OpenSSL


```python
# Lecture de la clé hexadécimale
with open('12-clesymetrique.hex', 'r') as fichier:
    cle32 = fichier.read()
    cle32 = int(cle32,base=16).to_bytes(32, 'big')
    
# Lecture du vecteur d'initialisation hexadécimal
with open('12-iv.hex', 'r') as fichier:
    iv16 = fichier.read()
    iv16 = int(iv16,base=16).to_bytes(16, 'big')

# Lecture du fichier binaire chiffré
with open('12-message.txt.enc', 'rb') as fichier:
    message_chiffre = fichier.read()
    
aes2 = AES.new(cle32, AES.MODE_CBC, iv=iv16)
aes2.decrypt(message_chiffre)
```


```python
aes = AES.new(cle32, AES.MODE_CBC, iv=iv16)
message_chiffre = aes.encrypt(message_complete)
message_chiffre
```
