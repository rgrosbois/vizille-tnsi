---
hide:
  - navigation
---

# Ordonnancement

<small>*(source : cours de J. DIRAISON)*</small>

---

## 1. Multitâche

Un système d'exploitation est dit multitâche lorsqu'il est capable de simuler l'exécution simultanée de plusieurs processus (indépendamment du nombre de processeurs et de c&oelig;urs).

Il y a 2 principales approches :

- Multitâche **coopératif** : les processus décident eux-mêmes du moment où ils *rendent la main* (à la fin de leur exécution ou en cas de blocage).

    Exemples : [Windows 3.11](https://copy.sh/v86/?profile=windows31) (1993), 
    [Mac OS 9](http://www.virtualdesktop.org/complete/os9/index.html) (1999)

- Multitâche **préemptif** : octroi de temps d'exécution entrelacés pour chaque 
processus. 

    !!! danger "Définition"

        Dans un système multitâche préemptif, chaque processus s'exécute pendant un temps
        multiple d'une durée minimale appelée quantum (*q*). La main est :

        - reprise de *force* par le système (interruption et sauvegarde de l'état) si le nombre de quanta alloués est échu ou
        - rendue prématurément par le processus s'il se termine ou se bloque.

    Exemples : [AmigaOS](http://www.chiptune.com/) (1985), [Windows 95](https://copy.sh/v86/?profile=windows95) (1995), Mac OS X (2001).

Autres types : OS temps réel&hellip;    

## 2. Ordonnanceur

Au c&oelig;ur du système d'exploitation se trouve l'**ordonnanceur**
(*scheduler* en anglais) qui, comme un chef d'orchestre, gère
l'utilisation du processeur par les différents programmes exécutés par
les utilisateurs.

Il y a souvent plus de processus à exécuter que de processeurs. En
allouant successivement à chaque processus, des **quanta de temps** 
(exemple : 10 ms sous Linux), l'utilisateur a l'illusion que plusieurs programmes
fonctionnent simultanément (*pseudo-parallélisme*).

![Schéma ordonnanceur](img/src/04-schme_ordonnanceur.svg){ width=50% }

### 2.1. Stratégies

!!! example "Exemple 1"

    Considérons 4 processus `A`, `B`, `C` et `D` (de même priorité) devant s'exécuter sur un système d'exploitation dont le quantum vaut $q=10 ms$.

    - `A` est prêt à l'instant 0 et nécessite 70 ms de temps CPU.
    - `B` est prêt à 20 ms et nécessite 30 ms de temps CPU.
    - `C` est prêt à 30 ms et nécessite 10 ms de temps CPU.
    - `D` est prêt à 60 ms et nécessite 30 ms de temps CPU.

    ??? tip "Représentation graphique de l'exécution sur 4 processeurs"

        ```mermaid
        gantt
            dateFormat H
            axisFormat %_H

            section Proc1
            A   : 0, 7h

            section Proc2
            B   : 2 ,3h

            section Proc3
            C   : 3, 1h

            section Proc4
            D   : 6, 3h
        ```

        > l'échelle de temps est graduée en quanta

Pour la suite, nous considérons que le système ne possède qu'un seul processeur monoc&oelig;ur.

??? success "(plus courte) durée d'exécution de tous les processus"

    Il existe toujours un processus qui s'exécute à chaque instant, c'est donc la somme de tous les temps CPU : 140 ms

Plusieurs stratégies peuvent être utilisées :

- Ordre d'arrivée : les processus s'exécutent jusqu'à leur terme (ou un blocage) selon leur ordre d'arrivée.

    ??? tip "Réponse"
        ```mermaid
        gantt
            dateFormat H
            axisFormat %_H

            section Proc1
            A   :a, 0, 7h
            B   :b, after a ,3h
            C   :c, after b, 1h
            D   :d, after c, 3h
        ```

    > C'est la stratégie utilisée par les systèmes non-préemptifs.

- Le plus rapide à finir en premier (`SRTF = Shortest Remaining Time First`) : élection (à la fin de chaque quantum) du processus dont le temps restant est le plus petit. 

    ??? tip "Réponse"
        `AABCBBDDDAAAAA`
        <!-- ![SRTF](img/03-SRTF.png) -->

    > Nécessite la connaissance préalable des durées effectives de chaque 
    processus.

- `Round Robin` (le tourniquet) : élection basée sur 

    - une liste circulaire (remplie selon l'ordre d'arrivée des processus).

        ??? tip "Réponse"
            `AABCABDABDADAA`

    - une *file*/FIFO (chaque processus non terminé est remis à la fin de la FIFO 
    à la fin du quantum d'exécution). L'élection se fait *en tout début* de quantum.

        ??? tip "Réponse"
            `AABACBADBADADA`

- Avec des priorités

    ??? example "Exemple 1 (modifié)"

        Les processus `A`, `B`, `C` et `D` ont pour priorités respectives `3`, `1`, `4` et `2` et, à chaque nouveau quantum, l'ordonnanceur élit le processus dont la valeur de priorité est la plus faible.

        ??? tip "Réponse"
            `AABBBADDDAAAAC`

- &hellip;

### 2.2. Temps d'exécution

On peut distinguer 4 durées caractéristiques :

- **Latence** : délai entre l'instant de création et celui du début d'exécution du programme.

    ??? tip "Réponse"

        | Stratégie | Arrivée | FIFO    | Liste circulaire | Priorité |
        |-----------|---------|---------|------------------|----------|
        | A         |    0 ms |    0 ms |           0 ms   |     0 ms |
        | B         |   50 ms |    0 ms |           0 ms   |     0 ms |
        | C         |   70 ms |   10 ms |           0 ms   |   100 ms |
        | D         |   50 ms |   10 ms |           0 ms   |    0 ms | 

        > Avec l'exemple, on voit que la stratégie tourniquet est globalement la plus *réactive* (la stratégie prioritaire ne l'est que pour des processus de grande priorité)

- **Processeur** (ou effective) : temps durant lequel le CPU exécute le programme. 

    ??? tip "Réponse"

        Quelle que soit la stratégie :

        | Processus |   CPU   |
        |-----------|---------|
        | A         |   70 ms |
        | B         |   30 ms |
        | C         |   10 ms |
        | D         |   30 ms |

- **Réelle** (ou apparente, ou de séjour) : délai entre l'instant où le processus est prêt et celui où il se termine.

    ??? tip "Réponse"

        | Processus | Arrivée |   FIFO  | Circulaire | Priorité |
        |-----------|---------|---------|------------|----------|
        | A         |   70 ms |  140 ms |     140 ms |   130 ms |
        | B         |   80 ms |   70 ms |      70 ms |    30 ms |
        | C         |   80 ms |   20 ms |      10 ms |   110 ms |
        | D         |   80 ms |   70 ms |      70 ms |    30 ms |

- **Attente** : différence entre les durées réelle et processeur.

    ??? tip "Réponse"

        | Processus | Arrivée |   FIFO  | Circulaire | Priorité |
        |-----------|---------|---------|------------|----------|
        | A         |    0 ms |   70 ms |      70 ms |    60 ms |
        | B         |   50 ms |   40 ms |      40 ms |     0 ms |
        | C         |   70 ms |   10 ms |       0 ms |   100 ms |
        | D         |   50 ms |   40 ms |      40 ms |     0 ms |

??? info "Commande shell : time"

    À placer avant toute commande pour obtenir les durées réelle et effective 
    (distinction entre durées d'exécution par utilisateur `user` et par le 
    noyau `sys`).

    ```{.console .rgconsole}
    [nsi@localhost:~]$ time sleep 5
    real	0m5,002s
    user	0m0,000s
    sys	    0m0,002s
    ```

### 2.3. Coût

De nombreuses opérations de commutation doivent être répétées entre chaque 
changement de processus par d'un ordonnanceur préemptif :

1. sauvegarde du *contexte*[^1] du processus précédent, 
2. élection du nouveau processus, 
3. restauration du *contexte* du nouveau processus

[^1]: Contenu des registres, espace d'adressage&hellip;

Ces opérations de *commutation* prennent un temps 
$\Delta t_s$ dont l'impact est souvent non négligeable sur les performances 
du système.

!!! danger "Coût d'ordonnancement"

    C'est le rapport entre le temps d'exécution des opérations d'ordonnancement 
    et celui d'exécution des processus.

    \[ Coût = \frac{\Delta t_s}{q} \]

??? example "Exemple 2"

    3 processus `A`, `B` et `C`, de durées CPU respectives 300 ms, 400 ms 
    et 200 ms, prêts en même temps (dans cet ordre), sont exécutés par un 
    ordonnanceur *tourniquet* de temps de commutation $\Delta t_s=1 ms$.

    1. Calculer le coût d'ordonnancement (en %), la latence moyenne, la durée 
    apparente et l'attente de `C` pour :

        - q = 10 ms
        - q = 5 ms
        - q = 20 ms

        ??? tip "Réponse"

            | q         |   5 ms |  10 ms |  20 ms |
            |-----------|--------|--------|--------|
            | Coût      |  20 %  |  10 %  |   5 %  |
            | Latence   |   7 ms |  12 ms |  33 ms |
            | Durée `C` | 720 ms | 660 ms | 630 ms |
            | Attente   | 520 ms | 440 ms | 510 ms |

    2.  Conclure sur l'influence de la valeur du quantum.

        ??? tip "Réponse"

            Plus q diminue et plus le système est réactif mais plus les temps 
            apparents s'allongent (dû au surcoût des commutations)


## 3. Synchronisation 

Chaque processus possède son propre espace mémoire, mais il peut être amené à
utiliser des *ressources* communes (via le noyau) qui ne peuvent parfois être
utilisées que par :

- Un unique processus (ressource en exclusion mutuelle ou *mutex*) : gestion à 
l'aide d'un cadenas/verrou (`lock`) que le processus acquiert (`acquire`) puis 
rend (`release`).

    ??? example "Exemple Python"

        ```python linenums="1" title="verrou.py"
        #!/usr/bin/env python3
        import os
        from multiprocessing import Process, Lock

        def programme(v, index):
            print(f'P{index} : {os.getpid()}')

            # acquérir le verrou (et ne pas le rendre)
            v.acquire()   
            # v.release()

            while True:
                pass

        verrou = Lock()

        p1 = Process(target=programme, args=(verrou, 1))
        p1.start()
        p2 = Process(target=programme, args=(verrou, 2))
        p2.start()
            
        # Attendre la fin de tous les processus
        p1.join()
        p2.join()
        ```

        ??? tip "Résultat"

            ```{.console hl_lines="9 10" .rgconsole}
            [nsi@localhost:~]$ ./verrou.sh
            [1] 475796
            P1 : 475797
            P2 : 475798
            [nsi@localhost:~]$ ps -l
            F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
            0 S  1000  467483  419938  0  80   0 - 56416 do_wai pts/5    00:00:00 bash
            0 S  1000  475796  467483  0  80   0 - 58119 do_wai pts/5    00:00:00 python3
            1 R  1000  475797  475796 87  80   0 - 58119 -      pts/5    00:00:03 python3
            1 S  1000  475798  475796  0  80   0 - 58119 futex_ pts/5    00:00:00 python3
            4 R  1000  475824  467483  0  80   0 - 56470 -      pts/5    00:00:00 ps
            ```

            P1 est actif (state : `R`) alors que P2 est bloqué (state : `S`) car 
            le verrou a été acquis par P1 mais jamais rendu.

- Un nombre fini de processus : gestion à l'aide de *sémaphore* qui 
distribue et récupère des *jetons*.

??? tip "Section critique (hors programme)"

    Les verrous et sémaphores sont aussi être utilisés pour synchroniser des
    programmes ou éviter que certaines parties ne s'exécutent en même temps.

??? example "Exemple 3"

    Un ordonnancement préemptif utilise une stratégie *tourniquet* 
    avec élection par FIFO pour une durée *q=20 ms*. Il gère 3 processus 
    `A`, `B` et `C` avec une ressource `R` en exclusion mutuelle.

    - `A` est prêt à 0 ms, dure 90 ms[^2], utilise `R` au bout de 30 ms[^2] 
    pour la rendre 50 ms[^2] plus tard.
    - `B` est prêt à 30 ms, dure 60 ms[^2], utilise `R` au bout de 10 ms[^2]
    et la rend (40 ms) plus tard.
    - `C` est prêt à 40 ms et dure 40 ms[^2].

    [^2]: Durée effective

    Indiquer le processus utilisé à chaque instant.

    ??? tip "Réponse"

        `AA AA BC CA AC CA AB BA BB B`

## 4. Notion d'interblocage (deadlock)

Un interblocage peut survenir entre 2 processus si, au même moment, chacun attend 
la ressource mobilisée par l'autre :

```mermaid
flowchart LR
    p1((Processus 1))
    p2((Processus 2))
    p1 ---> A["Ressource<br>A"] -. "Réveillez-moi<br>quand la ressource<br> devient disponible" .-> p2 ---> B["Ressource<br>B"] -. "Réveillez-moi<br>quand la ressource<br> devient disponible" .-> p1
```

Le blocage mutuel (ou *deadlock*) met en sommeil définitivement les deux 
processus.

??? example "Exemple 4"

    On considère le cas de 4 processus et 2 ressources ordonnancés par
    *tourniquet* à liste circulaire de quantum *q=20 ms* :

    | Processus | création | durée | verrou | acquisition | durée |
    |-----------|----------|-------|--------|-------------|-------|
    | `A`       |  0 ms    | 60 ms | R1     | 10 ms       | 40 ms |
    |           |          |       | R2     | 40 ms       | 10 ms |
    | `B`       | 20 ms    | 60 ms | -      | -           | -     |
    | `C`       | 30 ms    | 50 ms | R1     | 10 ms       | 40 ms |
    | `D`       | 40 ms    | 70 ms | R2     | 10 ms       | 60 ms |

    1. Indiquer le processus utilisé à chaque instant.

        ??? tip "Réponse"

            `AA BB CD DA AB BD DB BD DD AA CC CC`

    2. Que devient l'exécution si `D` demande aussi `R1` à 30 ms pour 10 ms ?

        ??? tip "Réponse"

            À partir de 140 ms :

            - `B` est terminé
            - Interblocage : 

                - `A` possède `R1` et attend `R2` &rarr; bloqué 
                - `D` possède `R2` et attend `R1` &rarr; bloqué 

            - `C` attend `R1` (détenu par `A` qui est bloqué) &rarr; bloqué 

!!! tip "Graphe d'allocation des ressources"

    C'est une méthode de détection d'interblocage qui utilise un *graphe orienté* (cf. un prochain cours) composé de 2 types de n&oelig;uds :

    - les processus (symbolisés par des cercles)
    - les ressources (symbolisés par des rectangles)

    et 2 types d'arcs :

    - du processus vers la ressource mobilisée (en trait plein).
    - de la ressource vers le(s) processus en attente (en pointillé).

    La découverte d'un cycle dans ce graphe signifie la présence d'un interblocage :

    ```mermaid
    flowchart LR
        tt((Traitement\nde texte))
        tab((Tableur))
        sgbd((SGBD))
        cao((CAO))

        tt ---> D1 -.-> tab ---> D5 -.-> cao ---> D4 -.-> sgbd ---> D2 -.-> tt
        D3 -.-> sgbd 
        D3 -.-> cao
    ```


!!! tip "Deadlock Empire (jeu)"

    [The Deadlock Empire](https://deadlockempire.github.io){ target=_blank } (en langage C) pour jouer le rôle d'un ordonnanceur.
