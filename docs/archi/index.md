# Architectures matérielles, systèmes d'exploitation et réseaux

## Rappels 1NSI
- [Escape Game](00-Escape_game.md) (TP)
- [L'histoire d'UNIX](https://www.youtube.com/watch?v=Za6vGTLp-wg){ target=_blank } (Vidéo)

## Les processus
- [Introduction via la CLI Linux](01-Processus_CLI.md).
- [Gestion sous Linux](02-Processus_Linux.md).
- [L'ordonnancement](03-Ordonnancement.md).

## Réseau

- [Contexte](04-Contexte.md)
- [Notions de routage dynamique](05-Routage_dynamique.md)
- [Modélisation par des graphes](06-Modelisation_graphe.md)

    - [Annexe : itinéraire optimal par l'algorithme de Dijkstra](07-Dijkstra-routeurs.md) ([version carte routière](07-Dijkstra.md))

- [Sécurisation des communications](08-Securite.md)
- [Fonctions de hachage(bonus)](09-Hachage.md)