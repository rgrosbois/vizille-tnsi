---
hide:
  - navigation
---

# Sécurisation des communications

Les premiers réseaux informatiques n'incorporaient aucune sécurité
de transmission. La seule protection (illusoire) des données ne
tenait qu'à celle des liens physiques.

![Hacker](img/hacker.svg){ width=60% style="margin: auto; display: block;" }

Les problèmes de sécurité apparus avec la croissance exponentielle
des réseaux et notamment le développement des transmissions sans fil
ont amené à intégrer des techniques cryptographiques :

![Cryptographie](img/cryptographie.svg){ width=60% style="margin: auto; display: block;" }

## I. Vocabulaire

Un algorithme de **chiffrement** effectue un codage :

- Réversible.
- Basé sur une technologie publique.
- Qui s'appuie sur un paramètre *secret* : la **clé**.

!!! info "Note"

    Même si ce n'a pas toujours été le cas (Cf. [Chiffrement de César](10-Chiffrement_de_Cesar-eleves.md)), il est dorénavant admis que le 
	niveau de sécurité d'un chiffrement dépend de sa clé et non de la 
	connaissance ou non de son algorithme.

Au niveau des actions :

- L'expéditeur **chiffre** le message en clair (avec une clé).
- Le destinataire **déchiffre** le message chiffré (avec une clé).
- Un hacker essaye de **décrypter** le message chiffré (sans clé).


## II. Familles d'algorithmes 

Il existe 2 familles d'algorithmes cryptographiques :

- Les algorithmes **symétriques** (les 2 correspondants utilisent la
  même clé)

    La clé de chiffrement est identique à celle de chiffrement (ou se
    déduit facilement de cette dernière).
  
- Les algorithmes **asymétriques** (les clés à chaque extrémité sont
  distinctes) :

	Les clés de chiffrement et de déchiffrement sont complétement
	différentes et il est *quasiment impossible* de trouver la
	deuxième, même en connaissant la première.

!!! info "Comparatif (rapide) des performances"

    |                     | Symétrique | Asymétrique |
	|---------------------|:----------:|:-----------:|
	| Vitesse d'exécution | +          | -           |
	| Taille des messages | +          | -           |
	| Taille de clé       | +          | -           |
	| Partage de clé      | -          | +           |

### 1. Algorithme asymétrique

appelé aussi algorithme à **clé publique**

Exemples: DH, DSA, **RSA**, ElGamal&hellip;

Une clé peut être rendue **publique** (l'autre clé reste
**privée**) 

![Clé publique](img/cle_publique.svg){ width=60% style="margin: auto; display: block;" }

- Le destinataire conserve précieusement sa clé *privée* ![icone cle
  privée](img/icone_clepriv.svg)

- La même clé *publique* ![icone cle publique](img/icone_clepub.svg)
  peut être partagée par tous les expéditeurs.

Seul le possesseur de la clé privée peut déchiffrer les messages qui lui sont destinés.

!!! warning "Note"

    La confidentialité est unidirectionnelle car seules les communications 
	aboutissant vers le propriétaire de la clé privée peuvent être chiffrée 
	par cette méthode.
	
	Pour une confidentialité bidirectionnelle, l'expéditeur doit 
	fournir une autre clé publique.
	
### 2. Algorithme symétrique

appelé aussi algorithme à **clé partagée**.

Exemples: DES, 3DES, **AES**, SEAL, RC&hellip;

La même clé est utilisée par les 2 correspondants. Elle doit donc
être *pré-partagée* mais rester *secrète*.

![Clé partagée](img/cle_partagee.svg){ width=60% style="margin: auto; display: block;" }

Il existe 2 types de techniques:

- *stream cipher*: le message est chiffré octet après octet (voire bit
  après bit) &rarr; extrêmement rapide.
  
- *block cipher*: le message est chiffré bloc après bloc (ex taille :
  128 bits) &rarr; un peu moins rapide.

### 3. Partage d'une clé symétrique

L'utilisation d'une clé publique permet de corriger l'*unique*
problème du chiffrement symétrique: le partage de la clé secrète dans
un canal de communication non (encore) sécurisé.

![Partage d'une clé](img/partage.svg){ width=60% style="margin: auto; display: block;" }

1. L'expéditeur génère une clé symétrique (généralement pour une seule
   discussion).
   
2. Le destinataire envoie sa clé publique (possibilité d'utiliser les
   certificats *PKI*)
   
3. L'expéditeur chiffre sa clé symétrique avec la clé publique et
   l'envoie au destinataire.
   
4. Le destinataire déchiffre la clé symétrique à l'aide de sa clé privée.

Ensuite peut commencer une communication confidentielle
*bi-directionnelle*.

## III. Principaux axes de sécurité

Dans la pratique, la sécurisation complète d'une communication doit
répondre à plusieurs exigences :

- La **Confidentialité** (ce que nous venons de voir).

- L'**Authentification** du(des) correspondant(s).

	Par exemple :
    ![Authentification](img/authentification.svg){ width=50% style="margin: auto; display: block;" }


- La vérification de l'**Intégrité** (voire la **non-répudiation**) des
  données.

	Par exemple :
    ![Intégrité](img/integrite.svg){ width=40% style="margin: auto; display: block;" }
