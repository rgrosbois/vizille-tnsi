// Compteur de bonnes réponses
var pageNbOK = 0;
var pageNbTot = 0;

/**
 * Cocher ou décocher un listitem (li) d'une question à choix multiples.
 * 
 * @param {Event} ev Évènement déclenché par le clic sur le listitem
 */
function cocher(ev) {
    ev.target.classList.toggle('coche');
}

/**
 * Corriger une question à choix multiple.
 * 
 * @param {Node} questRoot élément racine de la question (un fieldset)
 * @param {Array[boolean]} evaluation Tableau indiquant les choix corrects.
 */
function correction_qcm(questRoot, evaluation) {
    let ul = questRoot.querySelector('ul');
    let children = ul.children;
    let nbOK = 0;
    let nbTot = 0;
    for (let i=0; i<children.length; i++) {
        if (evaluation[i]) {
            nbTot += 1;
        }
        
        if (children[i].classList.contains("coche")) { // Case cochée
            if(evaluation[i]) { // Réponse attendue
                // entourer en vert
                children[i].style.border = '3px solid green';
                // Colorier en vert
                children[i].style.background = '#b6e4ce';
                children[i].style.color = '#255c41';
                nbOK += 1;
            } else { 
                // Colorier en rouge
                children[i].style.background = '#fbd7d8';
                children[i].style.color = '#b71c1c';
                // décocher la case
                children[i].classList.toggle('coche'); 
            }
        } else { // Case non cochée
            if(evaluation[i]) { // Réponse attendue
                // entourer en rouge
                children[i].style.border = '3px solid red';
                // cocher la case
                children[i].classList.toggle('coche'); 		
            }
        }
    }
    ul.removeEventListener('click', cocher)

    let bouton = questRoot.querySelector(".validate");
    bouton.innerHTML = nbOK+'/'+nbTot;
    bouton.disabled = true;

    pageNbOK += nbOK;
    pageNbTot += nbTot;
}

/**
 * Réinitialiser une question à choix multiple: supprimer l'éventuelle correction 
 * et mélanger les propositions.
 * 
 * @param {Node} racine Noeud parent de la question dans le code HTML
 * @param {Array[boolean]} reponses Tableau indiquant les bonnes réponses actuelles
 */
 function reinitialiser_qcm(racine, reponses) {
    // Énoncé de la question: conserver tel quel

    // Récupérer les propositions existantes et les supprimer du DOM
    let ul = racine.querySelector("ul");
    propositions = new Array(ul.childNodes.length)
    let i=0;
    horizontal = false;
    while (ul.firstChild) {
    	child = ul.firstChild;
        if (child.style.display==='inline') {
            horizontal = true;
        }

        if(reponses[i]) { // bonne réponse
            propositions[i] = '+ ' + child.innerText;
        } else { // mauvaise réponse
            propositions[i] = '- ' + child.innerText;
        }
        child.remove();
        i += 1;
    }
    // Mélanger les propositions et réidentifier les bonnes réponses
    propositions.sort(() => Math.random()-0.5 );
    for (let i=0; i<propositions.length; i++) {
	    // Identifier si la réponse est bonne (i.e. débute par '+')
	    reponses[i] = propositions[i].charAt(0)==="+";
    }
    // Replacer les propositions
    for (let i=0; i<propositions.length; i++) {
	    li = ul.appendChild(document.createElement("li"));
	    li.innerText = propositions[i].slice(2);
        li.onclick = cocher;
        if (horizontal) {
            li.style.display = 'inline';
        }
    }
    
    // Réinitialiser le bouton de validation
    reset_score(racine)
    let b1 = racine.querySelector(".validate")
    b1.onclick = () => correction_qcm(racine, reponses);

    // Réinitialiser le bouton de reset
    let b2 = racine.querySelector(".reset")
    b2.onclick = () => reinitialiser_qcm(racine, reponses);
}

/**
 * Réinitialiser le bouton de validation en effaçant un éventuel score.
 * 
 * @param {Node} racine 
 */
function reset_score(racine) {
    let b = racine.querySelector(".validate")
    if (b.innerText.includes("/")) { // Si question déjà corrigée
        tmp = b.innerText.split('/')
        pageNbOK -= parseInt(tmp[0])
        pageNbTot -= parseInt(tmp[1])
    }
    b.innerHTML = "&#10003;";
    b.disabled = false;    
}

/**
 * Corriger une question à réponse numérique.
 * 
 * @param {Node} racine Noeud parent de la question dans le code HTML
 * @param {Number} reponse Valeur numérique correcte
 * @param {Number} precision précision attendue
 */
function correction_qnum(racine, reponse, precision=0) {
    let source = racine.querySelector("input");
    let saisie = Number(source.value.replace(",","."));
    let bouton = racine.querySelector(".validate");
    
    if (Math.abs(saisie-reponse)<=precision) { // Bonne réponse
        source.classList.toggle('reponse-vide', false)
        source.classList.toggle('reponse-correcte', true)
        bouton.innerText = '1/1';
        pageNbOK += 1;
    } else { // Mauvaise réponse
        source.classList.toggle('reponse-vide', false)
        source.classList.toggle('reponse-fausse', true)
        source.title = reponse;
        bouton.innerText = '0/1';
    }
    bouton.disabled = true;
    pageNbTot += 1;
}

/**
 * Correction d'une question à réponse courte sous forme de texte.
 * 
 * @param {Node} racine Parent de la question dans le code HTML
 * @param {Array[string]} reponse Tableau des réponses textuelles correctes 
 * (fournir les éléments en minuscules ??)
 */
function correction_qsa(racine, reponse) {
    let source = racine.querySelector("input");
    let saisie = source.value;//.toLowerCase();
    let bouton = racine.querySelector(".validate");
    
    if (reponse.includes(saisie)) { // Bonne réponse
        source.classList.toggle('reponse-vide', false)
        source.classList.toggle('reponse-correcte', true)
        bouton.innerText = '1/1';
        pageNbOK += 1;
    } else { // Mauvaise réponse
        source.classList.toggle('reponse-vide', false)
        source.classList.toggle('reponse-fausse', true)
        source.title = reponse;
        bouton.innerText = '0/1';
    }
    bouton.disabled = true;
    pageNbTot += 1;
}

/**
 * Réinitialiser une question à réponse numérique ou à réponse courte.
 * 
 * @param {Node} racine Parent de la question dans le code HTML
 */
function reset_question(racine) {
    let source = racine.querySelector("input");
    source.classList.toggle('reponse-correcte', false)
    source.classList.toggle('reponse-fausse', false)
    source.value = "";
    source.classList.toggle('reponse-vide', true)
    source.title = "";

    // Réinitialiser le bouton de validation
    reset_score(racine);
}

/**
 * Lancer la correction de tous les exercices de la page (provoquer le clic sur
 * les boutons d'évaluation) et afficher le score comme texte du bouton.
 * 
 * @param {Button} bouton 
 */
function corriger_page(bouton) {
    let btns = document.querySelectorAll('button.validate');
    for(let i=0; i<btns.length; i++) {
	    btns[i].click();
    }

    bouton.innerHTML = 'Score final : '+Math.ceil(pageNbOK/pageNbTot*100)+" %";
    bouton.disabled = true;
    bouton.classList.toggle('md-button')
}

/**
 * Gestion du début de déplacement d'une proposition dans une question DnD:
 * mémoriser l'élément HTML de cette proposition.
 * 
 * @param {MouseEvent} ev 
 */
function rg_dragstart(ev) {
	ev.dataTransfer.setData('text/plain', ev.target.id);
}

/**
 * Gestion du glissement d'une proposition au dessus d'un emplacement pour
 * une question DnD: modifier la forme du curseur.
 * 
 * @param {Event} ev 
 */
function rg_dragover(ev) {
	ev.preventDefault();
	ev.dataTransfer.dropEffect = "move";
}

/**
 * Gestion de dépôt de la proposition sur un emplacement.
 * 
 * @param {Event} ev 
 * @param {string} reponse Texte de l'emplacement attendu pour cette proposition.
 */
function rg_drop(ev, reponse) {
    // Récupérer l'élément HTML source
    ev.preventDefault();
    let source = document.querySelector("#"+ev.dataTransfer.getData('text/plain'));
    source.toggleAttribute('draggable', false);

    // Placer la proposition au dessus de l'emplacement
    source.classList.toggle('proposition-depose');
    ev.target.appendChild(source);

    // Désactiver la zone de dépôt
    ev.target.toggleAttribute('ondrop')
    ev.target.toggleAttribute('ondragover')
}

function corriger_dnd(racine, reponses) {
    let nbOK = 0;
    let nbTot = 0;

    let emplacements = racine.querySelectorAll(".emplacement");
    for(let i=0; i<emplacements.length; i++) {
        if(emplacements[i].childElementCount==1 && 
            reponses[emplacements[i].firstChild.data]===emplacements[i].childNodes[1].innerText) {
            emplacements[i].classList.toggle('reponse-correcte', true)
            nbOK += 1
            nbTot += 1;
        } else if (reponses[emplacements[i].firstChild.data].length>0) {
            emplacements[i].classList.toggle('reponse-fausse', true)
            emplacements[i].title = reponses[emplacements[i].firstChild.data]
            nbTot += 1;
        }
    }

    // Afficher le score dans le bouton de validation
    let bouton = racine.querySelector(".validate");
    bouton.innerHTML = nbOK+'/'+nbTot;
    bouton.disabled = true;
    // Mettre à jour le score global
    pageNbOK += nbOK;
    pageNbTot += nbTot;
}

function reset_dnd(racine, reponses) {
    let pilePropositions = racine.querySelector(".pile-propositions");

    // Remettre les propositions déplacées dans la pîle de propositions
    let propDeplacees = racine.querySelectorAll(".proposition-depose")
    for (let i=0; i<propDeplacees.length; i++) {
        propDeplacees[i].classList.toggle('proposition-depose', false);
        propDeplacees[i].setAttribute('draggable', true);
        pilePropositions.appendChild(propDeplacees[i]);
    }

    // Réactiver les emplacements
    let emplacements = racine.querySelectorAll(".emplacement");
    for (let i=0; i<emplacements.length; i++) {
        emplacements[i].setAttribute('ondrop', 'rg_drop(event, \''+reponses[emplacements[i].firstChild.data]+'\')');
        emplacements[i].setAttribute('ondragover', 'rg_dragover(event)');
        emplacements[i].classList.toggle('reponse-fausse', false);
        emplacements[i].classList.toggle('reponse-correcte', false);
    }

    reset_score(racine);
}

