# Le chiffre de Vernam

Le **masque jetable**, également appelé **chiffre de Vernam**, est un algorithme 
de cryptographie inventé par Gilbert Vernam en 1917 et perfectionné par Joseph 
Mauborgne, qui rajouta la notion de clé aléatoire.

Le chiffrement par la méthode du masque jetable consiste à combiner le message 
en clair avec une clé présentant les caractéristiques très particulières suivantes :

   * La clé doit être une suite de caractères au moins aussi longue que le 
   message à chiffrer.
   * Les caractères composant la clé doivent être choisis de façon totalement 
   aléatoire.
   * Chaque clé, ou « masque », ne doit être utilisée qu'une seule fois (d'où le 
   nom de masque jetable).

Ce chiffre peut aisément se faire à la fin, mais nous verrons ici la version 
informatisée qui repose sur l'opérateur OU-exclusif.

## Exercice 1

**Écrire** une fonction conv_bin qui prend en paramètre un entier positif `n` 
(inférieur à 256) et renvoie une liste de 8 entiers correspondant à la 
représentation binaire de `n`.

Aide :

* l'opérateur `//` donne le quotient de la division euclidienne : `5 // 2` donne 
`2` ;
* l'opérateur `%` donne le reste de la division euclidienne : `5 % 2` donne `1` ;
* `append` est une méthode qui ajoute un élément à une liste existante : 
Soit `T=[5,2,4]`, alors `T.append(10)` ajoute 10 à la liste `T`. Ainsi, `T` 
devient `[5,2,4,10]`.
* `reverse` est une méthode qui renverse les éléments d'une liste. Soit 
`T=[5,2,4,10]`. Après `T.reverse()`, la liste devient `[10,4,2,5]`.

On remarquera qu’on récupère la représentation binaire d’un entier `n` en partant
 de la gauche en appliquant successivement les instructions :

``b = n % 2``

``n = n // 2``

répétées autant que nécessaire.

## Exercice 2

Pour crypter un message avec le chiffre de Vernam, on procède ainsi :
* on génère une clé aléatoire de même taille que le message
* on transforme le message en une suite de bits
* on réalise un OU-exclusif bit à bit entre le message et la clé

Pour déchiffrer le message on refait la même opération avec cette même clé.

La fonction ``xor()`` permet de réaliser l'opération OU-exclusif entre deux bits. 
Cette opération est décrite par la table de vérité suivante :

A | B | A xor B
 --- | --- | --- 
0 | 0 | 0 
0 | 1 | 1 
1 | 0 | 1 
1 | 1 | 0 

La fonction ``generatiion_cle()`` prend en paramètre le message et renvoie une 
clé aléatoire sous forme d'une liste de bits. La clé doit avoir la même taille 
que le message initial.

**Compléter** les différentes fonctions pour qu'elles répondent aux spécifications.
