---

author: François Meyer

title: Mme Tortue et M. Lièvre font la course

---


# Listes chaînées : Mme Tortue et M. Lièvre font la course
-------------------------------------------------

Dans une liste chaînée, la dernière cellule n'a pas de `suivante`. Si c'était le cas, sa `suivante` serait une des cellules précédentes, ce qui formerait une **boucle**. Le parcours d'une telle liste ne se terminerait jamais. 

Cet exercice consiste à réaliser une fonction `contient_boucle(tête: Cellule)` qui renvoie : 

  * `True` si la liste chaînée dont la première cellule est  `tête` contient une boucle, 

  *  `False` dans le cas contraire. 

Dans cet exercice, la classe `Cellule`  réalise une cellule de liste chaînée.

```python
class Cellule():
    def __init__(self):
        self.info = self.suivante = None
```

Voici un exemple de liste chaînée comportant une boucle. 

```mermaid
graph LR;
	0[0]-->1[1];
	1[1]-->2[2];
	2[2]-- ... de 3 à 10 ... -->11[11];
	11[11]-->12[12];
	12[12]-->13[13];
	13[13]-->14[14];
	14[14]-->15[15];
	15[15]-->16[16];
	16[16]-->17[17];
	17[17]-->13[13]

```

(version texte alternative)

	0 → 1 → ... 12 → 13→ 14→ 15 
                      ↑       ↓  
                      17  ←  16

(les n° correspondent aux valeurs de l'attribut `info` de chaque cellule)

Et un programme python permettant de la construire : 

```python linenums="1"
tête = prec = Cellule()
tête.info = 0

for i in range(1, 18):
    c = Cellule()
    c.info = i
    if i == 13:      # entrée de la boucle
        début_boucle = c
    prec.suivante = c
    prec = c

c.suivante = début_boucle # la suivante de la n°17 devient la n°13
```

Étudiez bien ce programme pour comprendre la suite, et testez vos modifications pour bien comprendre la structure de la liste. 

Il existe un moyen très simple de détecter une boucle. 

Supposons deux variables `lièvre` et `tortue` qui prennent comme valeurs des cellules successives d'une liste chaînée, en commençant par la `tête`, mais de sorte que `lièvre` passe d'une cellule directement à *la suivante de sa suivante*, alors que `tortue` passe seulement d'une cellule à sa suivante. Autrement dit, `lièvre` fait deux pas quand `tortue` n'en fait qu'un. 

Dans notre exemple, on représente sur ce tableau les positions respectives de  `lièvre` et `tortue` en écrivant leur attribut `info`. Le tableau s'arrête lorsque `tortue == lievre`.


<!-- je ne sais pas contrôler les tableaux en markdown -->
<table>
	<tr>
	<td><code>tortue.info</code></td>
	<td>0</td>
	<td>1</td>
<td>2</td>
<td>3</td>
<td>4</td>
<td>5</td>
<td>6</td>
<td>7</td>
<td>8</td>
<td>9</td>
<td>10</td>
<td>11</td>
<td>12</td>
<td>13</td>
<td>14</td>
<td>15</td>
</tr>
<tbody>
<tr>
<td><code>lievre.info</code></td>
<td>0</td>
<td>2</td>
<td>4</td>
<td>6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
<td>13</td>
<td>15</td>
<td>17</td>
<td>14</td>
<td>16</td>
<td>13</td>
<td>15</td>
</tr>
<tr>
<td>nb de pas du <code>lievre</code></td>
<td>0</td>
<td>2</td>
<td>4</td>
<td>6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
<td>18</td>
<td>20</td>
<td>22</td>
<td>24</td>
<td>26</td>
<td>28</td>
<td>30</td>
</tr>
</tbody>
</table>

**Lorsque `lièvre` entre dans la boucle, il se met à tourner en rond. Au bout d'un moment, `tortue` le rattrape et se trouve sur la même cellule que lui. Quand la condition `lièvre == tortue` devient vraie, nous savons qu'il y a une boucle.**

Cet algorithme est dû à Robert Floyd.

**Écrivez la fonction** `contient_boucle(tête: Cellule)`.

Il est garanti que la liste chaînée `liste` compte au moins une cellule, et qu'elle comporte une boucle ou se termine par une cellule dont la `suivante = None`.

!!! tip "Indices"

    1. Penser à ajouter une docstring et d'autres tests unitaires.
    2. Attention au point de départ : si lièvre et tortue partent de la même cellule, `lièvre == tortue`
	3. Attention à la fin de course : si la tortue rejoint le lièvre, ou si le lièvre arrive en fin de liste (sans boucle))


??? success "Réponse"

```python linenums="1"
def detect_boucle(tete):
	"""
	Détecte une boucle dans la liste chaînée dont la première
	cellule est tête, grâce à l'algorithme de Floyd
	"""
	lievre = tortue = tete
    while lievre is not None and lievre.suivante is not None:
        tortue = tortue.suivante
        lievre = lievre.suivante.suivante
        if lievre == tortue:
            return True
    return False

assert detect_boucle(test1) # avec boucle
assert not detect_boucle(test2) # sans 
assert not detect_boucle(test3) # sans
assert detect_boucle(test4) # avec boucle
assert detect_boucle(test5) # avec boucle
assert not detect_boucle(test6) # sans
```




## Pourquoi ça marche ? 

De manière générale, nous savons que lorsque `tortue` a effectué $x$ pas, `lièvre` en a effectué $2x$. De plus, à partir d'une des cellules comprises dans la boucle, si `lièvre` fait un tour complet, il se retrouve sur la même cellule. 

Appelons $\lambda$ le nombre de cellules dans la boucle. Dans notre exemple, $\lambda = 5$.

Lorsque `tortue` arrive sur la première cellule de la boucle  dont le n° est un multiple de $\lambda$ (`tortue.info` est donc de la forme $k \lambda$ où $k$ est  entier non nul),  `lièvre` a effectué $2 k \lambda = k  \lambda + k \lambda$  pas. Autrement dit, il a effectué $k$ tours depuis la  cellule n°$k \lambda$ où se trouve actuellement  la `tortue`, ce qui fait que `tortue` == `lièvre`.

Dans notre exemple, $k = 3$. 
