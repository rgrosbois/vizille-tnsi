---
hide:
  - navigation
---
# Programmation dynamique

## I. Contexte

**Exemple :** problème du sac à dos

![sac à dos](img/src/sac_a_dos.svg)

Question : quelle combinaison d'objets de valeur maximale peut-on emporter sans dépasser 40 kg ? 

Les 8 objets sont mémorisés dans des tuples (nom, poids, valeur) :

```python    
objets = [ ('A', 15, 500), ('B', 24, 400), ('C', 9, 350),  ('D', 25, 750), ('E', 5, 400),  ('F', 12, 800), ('G', 2, 1400), ('H', 18, 550) ]
```

=== "Force brute"

    Pour chaque combinaison, un objet est soit présent soit absent (1 bits d'information par objet). Il y a donc $2^8=256$ combinaisons possibles. 

    On teste toutes les combinaisons et on ne conserve que la (ou une des) solution(s) optimale(s).

    ??? info "Implémentation"

        ```py title="Problème du sac à dos par force brute"
        meilleure_combinaison = 0
        valeur_max = 0
        poids_valeur_max = 0

        for combinaison in range(256):
            poids = 0
            valeur = 0
            # La décomposition binaire de combinaison
            # indique la présence ou non de chaque objet
            for i in range(8):
                if (combinaison>>i) & 1 == 1:
                    poids += objets[i][1]
                    valeur += objets[i][2]

            if poids <= 40 and valeur > valeur_max: # considérer cette combinaison
                meilleure_combinaison = combinaison
                valeur_max = valeur
                poids_valeur_max = poids                
        ```

    ??? success "Conclusion"
    
        La complexité de cette méthode est en $o(2^n)$ (donc NP=Non Polynomiale).

=== "Algorithme glouton"

    À chaque itération, on sélectionne le **meilleur objet** restant d'un point de vue rapport valeur/poids jusqu'à atteindre 40 kg (sans dépasser).

    ??? info "Implémentation"
    
        ```py title="Problème du sac à dos avec un algorithme glouton"
        selection = []
        poids = 0
        valeur = 0

        objets_tries = sorted(objets, key=lambda o:o[2]/o[1], reverse=True)
        for o in objets_tries:
            if poids + o[1] <= 40: # ajouter l'objet
                selection.append(o[0])
                poids  += o[1]
                valeur += o[2]
        ```

    ??? success "Conclusion"
    
        La complexité est en $o(n)$ mais on n'obtient pas (nécessairement) une solution optimale.

**Question :** existe-t-il une méthode de résolution de ce problème de complexité polynomiale ?

## II. Principe

??? warning "Programmation dynamique = appellation *marketing*"

    Le concept de programmation dynamique a été introduit en 1953 par Richard Bellman (RAND Corporation). À l’époque, le terme programmation signifiait planification et ordonnancement. Ainsi, la programmation dynamique n’est pas un nouveau paradigme de programmation (telles que la programmation objet, la programmation fonctionnelle…) mais un paradigme algorithmique.

    2 origines possibles de cette dénomination :

    - selon R. Bellman : permet de masquer l'aspect théorique afin d'obtenir plus facilement des financements.
    - autre hypothèse : surenchère sur le concept de *programmation linéaire*.


!!! danger "Programmation dynamique"

    C'est une approche de résolution de gros problèmes d'**optimisation** (sous contrainte) où :

    - le problème se **divise** (souvent *récursivement*) en sous-problèmes facilement solubles (note: même principe que *diviser pour régner*).

        - les sous-problèmes sont **non indépendants** (i.e. se chevauchent).

        - l'ensemble des sous-problèmes est fini (i.e. sous-problèmes *indexables*)

    - les solutions des sous-problèmes sont **mémorisées**, pour ne les résoudre qu'une seule fois, et permettent de reconstruire *efficacement* la solution optimale du problème initial.


**Exemple:** application au problème du sac à dos

Notons $S^{nb}_{poids}$ une valeur (solution) optimale utilisant les *nb* premiers objets (ordre arbitraire: A, B, ...) et respectant la contrainte $\leq poids$.

Réfléxion sur $S^8_{40}$ (valeur optimale pour A, B, ..., H ne dépassant pas 40 kg):

=== "1ère division"

    $S^8_{40}$ correspond à la valeur maximale entre:

    - $S^7_{40}$ (valeur optimale pour A, B, ... G ne dépassant pas 40 kg) ou
    - $valeur(H) + S^7_{40-poids(H)} = 550 + S^7_{22}$ (car *H* fait 18 kg et vaut *550*) 

    $S^8_{40} = max(S^7_{40}, 550 + S^7_{22})$  
    
    ![Arbre sac à dos - étape 1](img/src/arbre_sac_a_dos-etape1.svg)

=== "2ème division"

    Avec le même raisonnement on peut dire que:

    - $S^7_{40} = max(S^6_{40}, val(G) + S^6_{40-poids(G)}) = max(S^6_{40}, 1400 + S^6_{38})$
    - $S^7_{22} = max(S^6_{22}, val(G) + S^6_{22-poids(G)}) = max(S^6_{22}, 1400 + S^6_{20})$

    ![Arbre sac à dos - étape 2](img/src/arbre_sac_a_dos-etape2.svg)

=== "3ème division"

    - $S^6_{40} = max(S^5_{40}, val(F) + S^5_{40-poids(F)}) = max(S^5_{40}, 800 + S^5_{28})$
    - $S^6_{38} = max(S^5_{38}, val(F) + S^5_{38-poids(F)}) = max(S^5_{38}, 800 + S^5_{26})$
    - $S^6_{22} = max(S^5_{22}, val(F) + S^5_{22-poids(F)}) = max(S^5_{22}, 800 + S^5_{10})$
    - $S^6_{20} = max(S^5_{20}, val(F) + S^5_{20-poids(F)}) = max(S^5_{20}, 800 + S^5_{8})$

    ![Arbre sac à dos - étape 2](img/src/arbre_sac_a_dos-etape3.svg)

=== "4ème division"

    - $S^5_{40} = max(S^4_{40}, val(E) + S^4_{40-poids(E)}) = max(S^4_{40}, 400 + S^4_{35})$
    - $S^5_{38} = max(S^4_{38}, val(E) + S^4_{38-poids(E)}) = max(S^4_{38}, 400 + S^4_{33})$
    - $S^5_{28} = max(S^4_{22}, val(E) + S^4_{28-poids(E)}) = max(S^4_{28}, 400 + S^4_{23})$
    - $S^5_{26} = max(S^4_{26}, val(E) + S^4_{26-poids(E)}) = max(S^4_{26}, 400 + S^4_{21})$
    - $S^5_{22} = max(S^4_{22}, val(E) + S^4_{22-poids(E)}) = max(S^4_{22}, 400 + S^4_{17})$
    - $S^5_{20} = max(S^4_{20}, val(E) + S^4_{20-poids(E)}) = max(S^4_{20}, 400 + S^4_{15})$
    - $S^5_{10} = max(S^4_{10}, val(E) + S^4_{10-poids(E)}) = max(S^4_{10}, 400 + S^4_{5})$
    - $S^5_{8} = max(S^4_{8}, val(E) + S^4_{8-poids(E)}) = max(S^4_{8}, 400 + S^4_{3})$

    ![Arbre sac à dos - étape 2](img/src/arbre_sac_a_dos-etape4.svg)

=== "Ensuite"

    Au fur et à mesure des divisions vont commencer à apparaître:

    - des problèmes insolubles: tous objets restants ont des poids strictement supérieurs à la contrainte $\rightarrow$ inutile de continuer à diviser.
    - des problèmes qui se chevauchent (nombre d'objet et contrainte de poids identiques).

    Pour continuer de manière efficace, il est important d'éviter de devoir résoudre plusieurs fois les mêmes sous-problèmes ($\rightarrow$ **mémorisation** des solutions aux sous-problèmes déjà rencontrés).

## III. Considérations sur la mémorisation

Illustrons les différents concepts à l'aide de la *suite de Fibonacci* :

$$ u_n = u_{n-1} + u_{n-2} $$

??? warning "Attention"

    Même si la problématique de la mémorisation est la même, la suite de Fibonacci n'est pas un exemple de programmation dynamique (ce n'est pas un problème d'optimisation).

=== "Sans mémorisation"

    ??? info "Implémentation"

        ```py title="Suite de Fibonacci récursive"
        def fibo_rec(n: int) -> int :
            if n==0 or n==1:
                return n
            else:
                return fibo_rec(n-1) + fibo_rec(n-2)
        ```

    ??? success "Conclusion"
    
        L'implémentation récursive (naïve) est très inefficace car il faut recalculer plusieurs fois les mêmes valeurs intermédiaires.

        ![Arbre de Fibonacci](img/arbre_fibonacci.png)

=== "Mémorisation ascendante"

    (ou *bottom-up*)

    Il s'agit de la méthode manuelle classique de calcul des éléments de la suite :

    ??? info "Implémentation"

        ```py title="Suite de Fibonacci itérative"
        def fibo_it(valeur:int) -> int:
            val = 0
            if valeur < 2:
                return valeur
            
            n_1,n_2 = 1,0 # n-1 et n-2
            n = 2
            while n <= valeur:
                val = n_1 + n_2
                n_2 = n_1
                n_1 = val
                n += 1
            return val
        ```

    On résout les sous-problèmes par taille croissante et on stocke à chaque fois leurs solutions (pour les réutiliser ultérieurement avec les plus gros sous-problèmes).

=== "Mémorisation descendante (**mémoisation**)"

    (ou *down-bottom*)

    ??? info "Implémentation"

        ```py title="Suite de Fibonacci récursive avec mémoïsation"
        def fibo_memo(n : int, memoire = None) -> int:
            if memoire is None:
                memoire = { 0: 0, 1: 1}
            
            if n not in memoire:
                memoire[n] = fibo_memo(n-1, memoire) + fibo_memo(n-2, memoire)
            return memoire[n]
        ```

    ??? info "Implémentation avec décorateur"

        *(hors programme TNSI)*

        ```py title="Suite de Fibonacci récursive avec mémoïsation et décorateur"
        def memoisation(mafonction):
            """Pour décorer n'importe quelle fonction 
            avec un paramètre entier en entrée et renvoyant une valeur"""
            memoire = {}
            def wrap(n):
                if n not in memoire:
                    memoire[n] = mafonction(n)
                return memoire[n]
            return wrap

        @memoisation
        def fibo_rec(n: int) -> int :
            if n==0 or n==1:
                return n
            else:
                return fibo_rec(n-1) + fibo_rec(n-2)
        ```

    On applique la récursivité sur le problème final et on stocke les solutions des sous-problèmes au fur et à mesure qu'on les rencontre.

## III. Exemples

### 1. Sac à dos

Revenons sur une version simplifiée de l'exemple de la première partie : on cherche une combinaison optimale ne dépassant pas 10 kg parmi les 6 objets suivants :

```python title="Données test"
# Liste de (nom, poids, valeur)
objets = [('A', 2, 3), ('B', 10, 8), ('C', 2, 2), ('D', 1, 8), ('E', 6, 4), ('F', 6, 6)]
```

On résout et mémorise (méthode ascendante) successivement les sous-problèmes :

- Par nombre d'objets utilisables

    > - 0 objet
    > - uniquement `A`, 
    > - uniquement `A` et `B`, 
    > - uniquement `A`, `B` et `C`
    > - .... 
    > - avec tous les objets.

- Par poids croissants (0 à 10 kg).

??? info "Tableau de valeurs optimales"

    A:2kg/3€, B:10kg/8€, C:2kg/2€, D:1kg/8€, E:6kg/4€, F:6kg/6€

    |    kg             | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 
    |:-----------------:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:--:|
    | O objet           | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |  0 |
    | A         | {{rg_qnum(0,0)}} | {{rg_qnum(0,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} |  {{rg_qnum(3,0)}} |
    | A, B | 0 | 0 | 3 | 3 | 3 | 3 | 3 | 3 | 3 | 3 |  8 |
    | A, B, C | {{rg_qnum(0,0)}} | {{rg_qnum(0,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(3,0)}} | {{rg_qnum(5,0)}} | {{rg_qnum(5,0)}} | {{rg_qnum(5,0)}} | {{rg_qnum(5,0)}} | {{rg_qnum(5,0)}} | {{rg_qnum(5,0)}} |  {{rg_qnum(8,0)}} |
    | A, B, C, D | {{rg_qnum(0,0)}} | {{rg_qnum(8,0)}} | {{rg_qnum(8,0)}} | {{rg_qnum(11,0)}} | {{rg_qnum(11,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}}  |
    | A, B, C, D, E | 0 | 8 | 8 | 11 | 11 | 13 | 13 | 13 | 13 | 15 | 15 |
    | A, B, C, D, E, F | {{rg_qnum(0,0)}} | {{rg_qnum(8,0)}} | {{rg_qnum(8,0)}} | {{rg_qnum(11,0)}} | {{rg_qnum(11,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(13,0)}} | {{rg_qnum(14,0)}} | {{rg_qnum(14,0)}} | {{rg_qnum(17,0)}} | {{rg_qnum(17,0)}}  |

    {{rg_corriger_page(titre="Corriger le tableau")}}

??? danger "Algorithme"

    Connaissant la solution optimale pour chaque poids entre 0 et `poids_max` à la ligne `i-1` objets. Alors la solution optimale pour `poids_max` à la ligne `i` correspond au maximum de valeur entre :

    - soit la solution optimale pour `poids_max` avec `i-1` objets ou
    - soit l'objet `i` et la solution optimale pour `poids_max - poids(objet i)` avec `i-1` objets.

    
??? success "Conclusion" 

    Avec la programmation dynamique, le contenu de chaque ligne s'obtient avec uniquement la connaissance :

    - Du poids/valeur du nouvel objet incorporable.
    - Des informations de la ligne précédente (en fait uniquement les colonnes à gauche de celle à remplir).

    L'illustration ci-après fait apparaître les sous-problèmes réellement nécessaires (par exemple en utilisant la mémoïsation) pour résoudre le problème initial (6 objets pour 10 kg) :

    ![Tableau final](img/src/sac_a_dos-6_objets.svg){ width="100%"}

<!-- ### 2. Réduction d'image

### 3. Rendu de monnaie

### 4. Alignement de séquences -->
