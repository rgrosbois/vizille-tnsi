---
hide:
  - navigation
---
# Algorithmes de graphes

!!! example "Exemple de graphe"

    <!-- ```dot
    Graph {
        layout = neato
        i -- d, h
        g -- a, b, d, f, h
        b -- c, e, f, a
        h -- a
        d -- f
    }
    ``` -->

# 1. Parcours 

Un parcours de graphe consiste à partir d'un nœud spécifique et *visiter* ensuite tous les autres. Comme pour les arbres, on retrouve 2 catégories de parcours :

- en largeur
- en profondeur

??? warning "Avertissement"

    Contrairement aux arbres binaires (ou les enfants sont ordonnées), il n'existe pas qu'une seule possibilité de parcourir un graphe en largeur (ou en profondeur).

=== "Largeur d'abord (BFS)"

    Il faut choisir un sommet de départ, puis on visite tous les autres selon cet ordre :

    - sommets à une distance de 1
    - sommets à une distance de 2 (=sommets voisins de ceux à une distance de 1)
    - sommets à une distance de 3 (sommets voisins de ceux à une distance de 2)
    - &hellip;(on ne s'arrête lorsque tous les sommets ont été visités)

    !!! danger "Algorithme"

        Pour repartir d'un sommet visité à l'itération (distance) précédente, il faut utiliser une **file**.

        - Placer le nœud de départ dans la file (=enfiler).
        - Tant que la file n'est pas vide et qu'il reste des nœuds à visiter :

            - défiler le nœud suivant (=le **visiter** si ce n'est pas déjà fait)
            - enfiler ses voisins (non encore visités)

    ??? example "Exemple"

        En partant de `i` dans l'exemple précédent, on obtient (si les voisins sont listés dans l'ordre alphabétiques) : `i` &rarr; `d` &rarr; `h` &rarr; `f` &rarr; `g` &rarr; `a` &rarr; `b` &rarr; `c` &rarr; `e`.

=== "Profondeur d'abord (DFS)"

    On part d'un sommet et on suit un chemin jusqu'à son bout. On rebrousse alors jusqu'à la première bifurcation possible pour démarrer un nouveau chemin. On ne s'arrête lorsque tous les sommets ont été visités.

    !!! danger "Algorithme itératif"

        Pour toujours repartir du dernier sommet avec voisins non visités, il faut utiliser une **pile**.

        - Placer le nœud de départ dans la pile (=empiler).
        - Tant que la pile n'est pas vide et qu'il reste des nœuds à visiter :

            - dépiler le nœud suivant (=le **visiter** si ce n'est pas déjà fait)
            - empiler ses voisins (non encore visités)

    ??? tip "Récursivité"

        La pile n'est plus nécessaire si on met en œuvre une implémentation récursive.

    ??? example "Exemple"

        En partant de `i` dans l'exemple précédent, on obtient (si les voisins sont listés dans l'ordre alphabétiques) : `i` &rarr; `d` &rarr; `f` &rarr; `b` &rarr; `a` &rarr; `g` &rarr; `h` &rarr; `c` &rarr; `e`.

# 2. Détection de cycle

*(à venir)*