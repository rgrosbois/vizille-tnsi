# Algorithmique

1. [Algorithmes d'arbres binaires et ABR](01-algo_arbre_binaire.md)
2. [Algorithmes de graphes](02-algo_graphes.md)
3. [Recherche textuelle](03-Recherche_texte.md)
4. [Diviser pour régner](diviser_regner.md)
5. [Programmation dynamique](programmation_dynamique.md)