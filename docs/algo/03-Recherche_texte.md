---
hide:
  - navigation
---
# Recherche textuelle

L'objectif est de trouver une ou toutes les positions d'un motif dans un texte, à l'aide d'algorithmes *performants*, même lorsque les données sont volumineuses.

=== "Traitement de texte"

    Recherche/remplacement de texte (++ctrl+f++)

    <figure>

    ![Rechercher dans texte](img/03-recherche_editeur.png){ width=50% }

    </figure>

=== "Séquence ADN"

    Recherche de motif composé de bases azotées (A: adénine, C: cytosine, G: guanine, T: thymine) dans une séquence d'ADN.

    ??? tip "Simulateur"

        <iframe style="border: 4px solid;" src="http://hmalherbe.fr/thalesm/gestclasse/documents/Terminale_NSI/2020-2021/Cours/Recherche_textuelle/Animation_Boyer_Moore/Animation_Boyer_Moore.html" width="1000" height="900"></iframe>

    <figure>

    ![Séquence d'ADN](img/03-sequence_ADN.webp){ width=50% }

    </figure>


=== "Autres"

    - Moteurs de recherche
    - ...
 
??? tip "Remarques" 

    - Le projet [Gutenberg](https://www.gutenberg.org/browse/languages/fr) permet de télécharger légalement des ouvrages libres de droits dans différents formats.

    - caractère, motif, textes sont tous représentés à l'aide du type/classe `str` en Python. 

!!! warning "Compléxité"

    L'étude du coût de l'algorithme de Boyer/Moore/Horspool est hors programme mais une première analyse quantitative peut être réalisée en mesurant les temps d'exécution (ex: `time.time()`)

## 1. Recherche d'un caractère (rappels)

Plusieurs variantes de recherche :

- Présence.
- Position de la première occurrence.
- Position de la dernière occurrence.
- Liste des positions de toutes les occurrences.
- Nombre d'occurrences de chaque caractère.
- ...

Dans chaque cas, il faut parcourir chaque caractère de la chaine (de longueur *n*) à l'aide d'une boucle.

=== "Position"

    ??? tip inline end "Remarque"
    
        Les méthodes `index` et `find` de la classe `str` (Python) sont aussi capables de réaliser cette opération.

    === "for (index)"

        ```py
        def position(caractere: str, chaine: str) -> int:
            """
            Donne l'index de la première apparition 
            de caractere dans chaine (ou -1 s'il n'est pas présent)
            """
            n = len(chaine)
            for i in range(n):
                if chaine[i] == caractere:
                    return i
            return -1
        ```

    === "while (index)"

        ```py
        def position(caractere: str, chaine: str) -> int:
            """
            Donne l'index de la première apparition 
            de caractere dans chaine (ou -1 s'il n'est pas présent)
            """
            n = len(chaine)
            i = 0
            while i < n:
                if chaine[i] == caractere:
                    return i
                i += 1
            return -1
        ```

=== "Présence"
    
    ??? tip inline end "Remarque" 
    
        L'opérateur `in` de Python est aussi capable de fournir cette réponse.

    ```py
    def presence(caractere: str, chaine: str) -> bool:
        """
        Indique si caractere est présent dans chaine.
        """
        for c in chaine:
            if c == caractere:
                return True
        return False
    ```

=== "Nombre d'occurrences"

    ```py
    lettres = {}
    for c in chaine:
        if lettres.get(c) is None:
            lettres[c] = 1
        else:
            lettres[c] += 1
    ```

??? info "Complexité"

    Le nombre de comparaisons de caractères à effectuer pour ces recherches est égal à :

    | Au mieux | Au pire | En moyenne    |
    |:--------:|:-------:|:-------------:|
    | *1*      | *n*     | $\frac{n}{2}$ |

    La complexité est dite en $O(n)$.

## 2. Recherche de motif

(on parle aussi de sous-chaîne)

Un motif est constitué d'un ou plusieurs caractères consécutifs. On note *m* sa longueur, supposée (très) inférieure à *n* (longueur de la chaîne).

### a. Algorithme naïf

```py
def recherche_naive(texte: str, motif: str) -> int:
    """
    Renvoie la liste des positions des occurrences de motif
    dans texte.
    """
    occurrences = []
    m = len(motif)
    n = len(texte)
    for i in range(n - m + 1):
        j = 0
        while texte[i + j] == motif[j]:
            if j == m-1:
                occurrences.append(i)
                break
            j += 1
    return occurrences
```

??? info "Complexité"

    - au mieux : $O(n)$ &rarr; proposer un exemple
    - au pire : $O(n\cdot m)$ &rarr; proposer un exemple

    <iframe style="border: 4px solid;" src="https://boyer-moore.codekodo.net/recherche_naive.php" width="800" height="800"></iframe>


### b. Algorithme de Boyer-Moore

??? tip inline end "R. Boyer et S. Moore"

    Robert S. Boyer (informaticien/mathématicien américain) et J Strother Moore (informaticien/logicien américain) ont créé cet algorithme en 1977 à l'université d'Austin au Texas.

Les 2 idées derrière l'algorithme original sont les suivantes : 

1. Parcours du texte en effectuant des *sauts optimisés* à l'aide d'une table de décalage (heuristique du *mauvais caractère*).
2. Utilisation de la *règle du bon suffixe*.

Les versions étudiées ci-après ne se basent que sur la première idée.

=== "Horspool"

    ??? tip inline end "Nigel Horspool"

        Informaticien britannique qui a proposé, en 1980, une version simplifiée de l'algorithme complet.

    ```py
    def rechercher_Horspool(txt, motif):
        n = len(txt)
        m = len(motif)

        # Précalcul pour le saut optimisé
        a_droite = {motif[i]:i for i in range(m)}

        # Algorithme de recherche
        i=0
        while i <= n-m: 

            # Identification de correspondance depuis fin de motif
            j = m-1
            while j>=0 and motif[j] == txt[i + j]: 
                j -= 1

            if j == -1: # motif trouvé
                return i

            # identification du prochain saut
            if a_droite.get(txt[i+m-1]) is None: # dernière caractère en face du motif
                decalage = m
            else:
                decalage = ...

            i += decalage
        return -1
    ```

=== "Boyer-Moore simplifié"

    ```py
    def rechercher_Horspool(txt, motif):
        n = len(txt)
        m = len(motif)

        # Précalcul pour le saut optimisé
        a_droite = {motif[i]:i for i in range(m)}

        # Algorithme de recherche
        i=0
        while i <= n-m: 

            # Identification de correspondance depuis fin de motif
            j = m-1
            while j>=0 and motif[j] == txt[i + j]: 
                j -= 1

            if j == -1: # motif trouvé
                return i

            # identification du prochain saut
            if a_droite.get(txt[i+j]) is None: # 1er caractère ne coincidant pas avec le motif
                decalage = m
            else:
                decalage = ...

            i += decalage
        return -1
    ```


??? info "Complexité"

    - Meilleur cas : $O(n/m)$
    - Pire cas : $O(n\cdot m)$, mais bien souvent meilleure ($O(3m)$ en moyenne)

    <iframe style="border: 4px solid;" src=https://boyer-moore.codekodo.net/recherche_boyer.php  width="800" height="800"></iframe>

## 3. Pour aller plus loin

- Algorithme de Boyer-Moore complet
- recherche de motifs avec des caractères joker (`*`, `?`) voire des expressions régulières