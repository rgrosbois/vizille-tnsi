---
hide:
  - navigation
---
# Diviser pour régner

## I. Analyse de la complexité d'un algorithme

Prenons l'exemple d'un algorithme de tri et comptons le nombre de comparaisons entre éléments à chaque itération.

=== "Tri par insertion"

    <div align="center">
    <iframe width="660" height="303" marginheight="0" marginwidth="0" src="../tri_insertion.html">
    IFrame non supportée
    </iframe>
    </div>

    Compléter cette implémentation de l'algorithme et afficher le nombre de comparaisons effectuées entre éléments durant l'exécution:
    
    ```py title="Implémentation du tri par insertion"
    def tri_insertion(liste: list[int]) -> int:
        """
        Trie la liste par par insertion et renvoie le nombre de comparaisons.
        """
        # Parcourt tous les éléments depuis le deuxième
        for j in range(1, len(liste)):
            e = liste[j]
            i = j
            
            # Remonter parmi les précédents pour trouver l'index d'insertion
            while i > 0 and liste[j] < liste[i-1]:
                i = i-1
            
            if i != j: # faire de la place en décalant à droite
                for k in range(j,i,-1):
                    liste[k] = liste[k-1]
                # placer l'élément courant
                liste[i] = e    
    ```

    ??? info "Nombre de comparaisons (liste de $n$ éléments)"

        - au mieux: $n$ comparaisons si la liste est déjà triée &rarr; la complexité est en $o(n)$.
        - au pire $1+2+3+\ldots+(n-1)=\frac{n(n-1)}{2}$ comparaisons si la liste est déjà triée dans l'ordre inverse &rarr; la complexité est en $o(n^2)$.

=== "Tri par sélection" 

    <div align="center">
    <iframe width="660" height="303" marginheight="0" marginwidth="0" src="../tri_selection.html">
    IFrame non supportée
    </iframe>
    </div>

    Compléter cette implémentation de l'algorithme et afficher le nombre de comparaisons effectuées entre éléments durant l'exécution:
    
    ```py title="Implémentation du tri par sélection"
    def tri_selection(liste: list[int]) -> int:
        """
        Trie la liste par sélection et renvoie le nombre de comparaisons.
        """
        # Parcourir tous les éléments jusqu'à l'avant dernier de la liste
        for i in range(len(liste)-1):
            # Rechercher la position du minimum parmi les indices restants.
            j_min = i
            for j in range(i+1,len(liste)):
                if liste[j] < liste[j_min]:
                    j_min = j
            # Permuter les positions i et j_min
            liste[i],liste[j_min] = liste[j_min],liste[i]
    ```

    ??? info "Nombre de comparaisons (liste de $n$ éléments)"

        Il est de $1+2+3+\ldots+(n-1)=\frac{n(n-1)}{2}$ &rarr; la complexité est en $o(n^2)$

??? warning "Nombre de permutations"

    Si on s'intéresse plutôt au nombre de permutations (au lieu du nombre de comparaisons), la complexité de l'algorithme de tri :

    - par insertion est en $o(n)$ au mieux et en $o(n^2)$ au pire.
    - par sélection est en $o(n)$.

!!! tip "Comparatif des ordres de grandeur"

    ![Complexité](img/complexite.png)

    On constate que, très rapidement, un algorithme en $o(n^2)$ va s'avérer nettement plus *complexe* qu'un algorithme en $o(log(n))$.

## II. Principe

C'est une approche de résolution de qui vise à diminuer la complexité de certains problèmes en se basant sur 3 concepts:

**Diviser**
: partager le problème en sous-problèmes indépendants de même nature (souvent de taille moitié, par récursivité).

**Régner**
: résoudre indépendamment les différents sous-problèmes (souvent déjà résolus si les sous-problèmes sont petits).

**Combiner**
: fusionner les solutions des sous-problèmes pour obtenir la solution du problème initial.

## II. Exemples d'algorithmes

### 1. Tri d'une liste

Algorithme de **tri par fusion** d'une liste de $n$ éléments: 

- **Diviser** : la longueur de la liste est divisée par 2 à chaque étape &rarr; arbre à $\simeq log_2(n)$ niveaux.
- **Régner** : aucun coût.
- **Combiner**: au pire $n$ comparaisons à chaque niveau de l'arbre


<div align="center">
<iframe width="705" height="300" marginheight="0" marginwidth="0" src="../tri_fusion.html">
IFrame non supportée
</iframe>
</div>

La complexité de cet algorithme est en $o(nlog_2(n))$ dans le pire des cas.

??? tip "Algorithme dit *rapide*"

    *(ou quick-sort)*

    - **Diviser** :
    
        - choisir un pivot dans la liste à trier (par exemple celui du milieu) et permuter tous les éléments inférieurs au pivot en début de liste (i.e. avant le pivot) et tous les éléments supérieurs en fin de liste (i.e. après le pivot)
        
        - Recommencer récursivement l'étape précédente sur le début de liste et sur la fin de liste → on s'arrête lorsque ces listes sont élémentaires (i.e. de longueur 0 ou 1), c.à.d déjà triées (**régner**).

    - **Combiner** : rien à faire (les éléments de la partie gauche sont déjà tous inférieurs à ceux de la partie droite).

    L'algorithme n'est optimal (coût en $o(nlog_2(n))$) que si les pivots *divisent* en 2 chaque liste mais dans le pire des cas on retrouve un coût en $o(n^2)$.


### 2. Exponentiation rapide

Par définition calculer $x^n$, lorsque *n* est un entier, revient à multiplier *n*-fois *x* par lui-même:

$$\begin{aligned}x^2 &= x\times x\\x^3 &= x\times x\times x\\x^n &= \underset{\text{n fois}}{\underbrace{x\times x\times\ldots\times x}} = x\times x^{n-1}\end{aligned}$$

=== "Implémentation itérative"

    ```py 
    def exp_it(x: float, n: int) -> float:
        resultat = 1
        for i in range(n):
            resultat = resultat * x
        return resultat
    ```

=== "Implémentation récursive"

    ```py 
    def exp_rec(x: float, n: int) -> float:
        if n==0:
            return 1
        else:
            return x * exp_rec(x, n-1)
    ```

Dans une approche *diviser pour régner*, on peut distinguer les cas où *n* est :

- **pair :** $n=2p$ et on peut faire: $x^n = (x^p) \times (x^p)$, soit $p=\frac{n}{2}$ multiplications.

- **impair :** $n=2p+1$ et on peut alors faire: $x^n = (x^p) \times (x^p) \times x$, soit $p+1=\lfloor\frac{n}{2}\rfloor + 1$ multiplications.

Une implémentation peut être la suivante:

```py title="Implémentation rapide de l'exponentielle"
def exp_rapide(x: float, n: int) -> float:
    if n == 0:
        return 1
    elif n % 2 == 0: # n est pair
        tmp = exp_rapide(x, n//2)
        return tmp * tmp
    else: # n est impair
        tmp = exp_rapide(x, (n-1)//2)
        return x * tmp * tmp
```

### 3. Rotation d'une image d'1/4 de tour

- **Diviser/Combiner** : À chaque étape, l'image est divisée en 4 sous-images qui sont permutées (ici dans le sens horaire).
- **Régner** : les images 1x1 sont *déjà tournées* (rien à faire).

<div align="center">
<iframe width="512" height="512" marginheight="0" marginwidth="0" src="../rotation_image.html">
IFrame non supportée
</iframe>
</div>

Avantage : coût en mémoire constant.

Inconvénient : algorithme peu efficace (pour une image $2^n\times 2^n$, chaque pixel est déplacé $n$ fois).

### 4. Recherche d'élément dans une liste

#### a. Recherche d'un élément quelconque

Lorsqu'aucune hypothèse ne peut être faite sur la liste, la technique consiste à *visiter* toute la liste jusqu'à trouver (ou non) cet élément:

```py
def recherche(element:int, liste: list[int]) -> bool:
    for i in range(len(liste)):
        if element==liste[i]:
            return True
    return False
```
 
!!! info "Complexité"

    Pour une liste de *n* éléments, il faut effectuer entre 1 et *n* comparaisons. La complexité moyenne est en $o(n)$.


Une approche *diviser pour régner* consiste à scinder la liste en 2 parties et rechercher l'élément dans ces 2 parties. Par divisions successives, on aboutit à des listes de base de 0 (élément non trouvé) où 1 élément (1 comparaison à effectuer). On recombine toutes les parties à l'aide d'un ou logique.

```py
def recherche_dpr(element, liste):
    """
    Attention: ici la division effectue des copies de liste et ne gère donc la mémoire
    de manière optimale.
    """
    longueur = len(liste)
    
    if longueur==0: # régner (cas élémentaire)
        return False
    elif longueur==1: # régner (cas élémentaire)
        if element==liste[0]:
            return True
        else:
            return False
    else: # diviser puis combiner
        i_milieu = longueur//2
        # Dans partie gauche ou dans partie droite
        return recherche_dpr(element,liste[:i_milieu]) or recherche_dpr(element,liste[i_milieu:])
```

On montre que la complexité de cette approche reste en *o(n)*.

??? info "Cas l'une liste triée"

    La complexité de la recherche peut être réduite à *o(n)* en utilisant l'algorithme de dichotomie.

    ```py
    def dichotomique_recursive(element: int, liste: list[int], i_debut:int =0, i_fin:int = None) -> int:
        """
        Recherche si element est présent dans liste entre l'index i_debut (inclu) et i_fin (non inclu).
        """
        
        # Préconditions
        if i_fin is None: # initialisation pour premier appel
            i_fin = len(liste)        
        if i_debut==i_fin: # liste vide
            return False

        # Algorithme
        i_milieu = (i_debut+i_fin)//2
        if element == liste[i_milieu]: # élément trouvé
            return True
        elif i_fin == i_debut+1: # L'élément n'est pas présent
            return False
        elif element rechercher à gauche
            return dichotomique_recursive(element, liste, i_debut, i_milieu)
        else: # l'élément est plus grand -> rechercher à droite
            return dichotomique_recursive(element, liste, i_milieu, i_fin)
    ```

    Cette technique est parfois assimilée à une approche *diviser pour régner*. Il faut néanmoins observer que la division du problème mène à 2 sous-problèmes qui ne sont pas de même nature (une des 2 sous-parties est automatiquement résolue): on rencontre parfois le terme d'approche *réduire pour régner*.

#### b. Recherche d'extrema

??? info "Cas d'une liste triée"

    La complexité est en *o(1)*: il s'agit soit du premier, soit du dernier élément.

L'algorithme basique (*n* comparaisons) reste le plus efficace si ne recherche que le maximum (ou le minimum):

```py
def recherche_max(liste: list[int]) -> int:
    maxi = liste[0]
    for i in range(1, len(liste)):
        compteur += 1
        if liste[i]>maxi:
            maxi = liste[i]
    return maxi
```

Par contre, une approche *diviser pour régner* peut s'avérer plus efficace si on recherche à la fois le minimum et le maximum:

=== "Recherche basique"

    ```py
    def min_max(liste:list[int]) -> tuple:
        compteur = 0
        
        mini = liste[0]
        for i in range(1, len(liste)):
            compteur += 1
            if liste[i]<mini:
                mini = liste[i]
        maxi = liste[0]
        for i in range(1, len(liste)):
            compteur += 1
            if liste[i]>maxi:
                maxi = liste[i]
        
        print(f"{compteur} comparaisons")
        return mini, maxi
    ```
    
=== "Recherche diviser pour régner"

    ```py
    def recherche_min_max(liste:list[int]) -> tuple:
        """
        Attention: ici la division réalise des copies de liste, ce qui 
        n'est pas optimal d'un point de vue mémoire.
        """
        if len(liste)==1:
            return liste[0], liste[0]
        elif len(liste)==2:
            if liste[1]>liste[0]:
                return liste[0], liste[1]
            else:
                return liste[1], liste[0]
        else:
            i_milieu = len(liste)//2
            
            min1,max1 = recherche_min_max([liste[i] for i in range(0, i_milieu)])
            min2,max2 = recherche_min_max([liste[i] for i in range(i_milieu, len(liste))])
            
            if min2 < min1:
                min1 = min2
            if max2>max1:
                max1 = max2
                
            return min1, max1
    ```

L'approche *diviser pour régner* réduit le nombre de comparaison à effectuer mais la complexité reste quand même en *o(n)*.


??? example "Exercice"

    Effectuer les modifications nécessaires dans toutes ces implémentations d'algorithmes de recherche afin de comptabiliser à chaque fois le nombre de comparaisons effectuées.


<!-- - Juste prix
- Recherche d'un sous-tableau de somme maximale
- Algo de Karatsuba (multiplication de grands nombres)
-->

