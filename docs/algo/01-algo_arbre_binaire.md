---
hide:
  - navigation
---

# Algorithmes d'arbres binaires et ABR

## 1. Parcours

Parcourir un arbre consiste à *visiter* chacun de ses nœuds.

```dot
graph G {
	A -- B, C  
	B -- D, E  
	C -- F, G 
}
```

Il existe plusieurs catégories de parcours :

- En **largeur d'abord** (*BFS: Breadth First Search*) : on visite tous les nœuds d'un même niveau avant de passer aux (enfants) nœuds du niveau suivant.
  
    ??? info "$\rightarrow A, B, C, D, E, F, G$"
		
		Une file est nécessaire pour effectuer les opérations de mémorisation(=enfiler) et visite(=défiler): 

		- On enfile la racine de l'arbre
		- À chaque fois qu'on défile un nœud, on le visite et on enfile d'abord son enventuel enfant
		gauche puis son éventuel enfant droit[^1]. 
		- On s'arrête lorsque la file est vide.

		[^1]: La file assure que la visite des nœuds s'effectue dans l'ordre des profondeurs croissantes.

		Ce qui donne:
		
		|Étape | Action | État de la file |
		|------|--------|-----------------|
		| 0 | Enfiler A |  ![Étape 0](img/bfs_etape0.svg) |
		|   <td align="center" colspan="2">Niveau 0</td> |
		| 1 | Défiler A et enfile B puis C | ![Étape 1](img/bfs_etape1.svg) |
		|   <td align="center" colspan="2">Niveau 1</td> |
		| 2 | Défiler B et enfiler D puis E | ![Étape 2](img/bfs_etape2.svg) |
		| 3 | Défiler C et enfiler F puis G | ![Étape 3](img/bfs_etape3.svg) |
		|   <td align="center" colspan="2">Niveau 2</td> |
		| 4 | Défiler D (rien à enfiler) | ![Étape 4](img/bfs_etape4.svg) |
		| 5 | Défiler E (rien à enfiler) | ![Étape 5](img/bfs_etape5.svg) |
		| 6 | Défiler F (rien à enfiler) | ![Étape 6](img/bfs_etape6.svg) |
		| 7 | Défiler F (rien à enfiler) | ![Étape 7](img/bfs_etape7.svg) |
		|   <td align="center" colspan="2">La file est vide</td> |
					
	```title="Algorithme BFS"
	file.enfiler(racine)
	tant que file != ∅ faire:
		n <- file.defiler();
		visiter(n);
		si n.gauche != ∅ alors:
			file.enfiler(n.gauche);
		si n.droit != ∅ alors:
			file.enfiler(n.droit);
	```

- En **profondeur d'abord** (*DFS: Deep-First Search*) : s'implémente
  facilement avec une fonction récursive. Plusieurs versions sont possibles :

	1. On visite le parent avant de parcourir les enfants (de gauche à 
	droite).
	
		```title="Algorithme profondeur d'abord préfixé (récursif)"
		fonction prefixe(arbre):
			visiter(arbre);
			si arbre.gauche != ∅ alors:
				prefixe(arbre.gauche)
			si arbre.droit != ∅ alors:
				prefixe(arbre.droit)
		```

		??? success "Résultat" 

			$\rightarrow A, B, D, E, C, F, G$

		??? tip "Version itérative"

			Une pile est nécessaire pour effectuer les opérations de mémorisation(=empiler) et visite(=dépiler): 

			- On empile la racine de l'arbre
			- À chaque fois qu'on dépile un nœud, on le visite et on empile d'abord son enventuel enfant gauche puis son éventuel enfant droit. 
			- On s'arrête lorsque la pile est vide.
		  
	2. On visite les enfants avant de visiter le parent.
	
		```title="Algorithme profondeur d'abord postfixé ou suffixé (récursif)"
		fonction postfixe(arbre):
			si arbre.gauche != ∅ alors:
				postfixe(arbre.gauche)
			si arbre.droit != ∅ alors:
				postfixe(arbre.droit)
			visiter(arbre);
		```

		??? success "Résultat" 

			$\rightarrow D, E, B, F, G, C, A$
	  
	3. On visite le parent après l'enfant gauche, mais avant l'enfant droit 
	*(uniquement pour les arbres binaires)*

		```title="Algorithme profondeur d'abord infixé (récursif)"
		fonction infixe(arbre):
			si arbre.gauche != ∅ alors:
				infixe(arbre.gauche)
			visiter(arbre);
			si arbre.droit != ∅ alors:
				infixe(arbre.droit)
		```

		??? success "Résultat" 

			$\rightarrow D, B, E, A, F, C, G$
	  

??? tip "Parcours en profondeur d'abord infixé d'un ABR"

	Ce type de parcours revient à visiter les nœuds dans l'ordre croissant.

	Exemple: on trouve 3, 5, 7, 10, 12, 15 et 17.

    ```dot
	graph G {
		10 -- 5, 15  
		5 -- 3, 7  
		15 -- 12, 17 
	}
	```

## 2. Taille et hauteur

*(on conviendra ici que la hauteur d'un arbre correspond à son nombre de niveaux)*

Il faut parcourir entièrement l'arbre pour récupérer ces 2 informations :

- on compte le nombre de nœuds visité pour obtenir la taille
- On mémorise le maximum de profondeur de chaque feuille pour obtenir la hauteur (remarque : les feuilles les plus profondes se trouvent nécessairement à la fin lors d'un BFS)

### a. Version itérative

Utilisons, par exemple, un parcours en largeur (BFS) :

```python title="Taille et hauteur d'un arbre (version itérative)"
compteur = 0
file.enfiler([racine, 1]) # La racine est à une profondeur de 1

while not est_vide(file):
	noeud, profondeur = file.defiler()
	compteur += 1

	if not est_vide(sous_arbre_gauche(noeud)):
		file.enfiler([sous_arbre_gauche(noeud), profondeur+1])
	if not est_vide(sous_arbre_droit(noeud)):
		file.enfiler([sous_arbre_droit(noeud), profondeur+1])

print("Taille=", compteur, ", hauteur=", profondeur)
```

### b. Version récursive

La taille d'un arbre est la somme :

- la taille de son sous-arbre gauche
- la taille de son sous-arbre droit
- et 1 nœud (sa racine)

D'où les implémentations suivantes :

```python title="Taille d'un arbre (version récursive)"
def taille(arbre): # cas de base
	if est_vide(arbre):
		return 0
	return 1 + taille(sous_arbre_gauche()) + taille(sous_arbre_droit())
```
	
```python title="Hauteur d'un arbre (version récursive)"
def hauteur(arbre):
	if est_vide(arbre): # cas de base
		return 0
	return 1 + max(hauteur(sous_arbre_gauche()), hauteur(sous_arbre_droit()))
```

## 3. Recherche et insertion de clé dans un ABR

*(on considère ici un ABR où l'étiquette de chaque nœud est supérieure ou égale à toutes celles du sous-arbre gauche et strictement inférieure à celles du sous-arbre droit)*

Contrairement aux algorithmes précédents, il n'y a pas besoin ici de parcourir complètement l'arbre (complexité en $n$), mais seulement de suivre le chemin qui va de la racine à l'emplacement cible (complexité en $log(n)$, si l'arbre est complet)

Pour chaque nœud depuis la racine, il y a 4 possibilités :

- Le nœud courant est vide :

	- si recherche : on renvoie `False`. 
	- si insertion : on crée un nouveau nœud avec cette clé.

- La clé du nœud courant est égale à celle recherchée.

	- si recherche : on renvoie `True`.
	- si insertion : on poursuit avec le sous-arbre gauche.

- L'étiquette du nœud courant est strictement supérieure à celle donnée.

	On poursuit avec le sous-arbre gauche.

- L'étiquette du nœud courant est strictement inférieure à celle donnée.

	On poursuit avec le sous-arbre droit.


Ce qui donne les implémentations récursives suivantes :

=== "Insertion"

	```python

	def inserer(arbre, cle):
		if est_vide(arbre):
			arbre = creer_arbre(cle)
		elif valeur(racine(arbre)) >= cle:
			return inserer(sous_arbre_gauche(arbre), cle)
		else:
			return inserer(sous_arbre_droit(arbre), cle)
	```

=== "Recherche"

	```python title="Recherche de clé"

	def est_present(arbre, cle):
		if est_vide(arbre):
			return False
		elif valeur(noeud) == cle:
			return True
		elif valeur(noeud) > cle:
			return est_present(sous_arbre_gauche(noeud), cle)
		else:
			return est_present(sous_arbre_droit(noeud), cle)
	```

	??? tip "Alternative moins efficace"

		En oubliant la nature ordonnée de ABR, la recherche s'écrirait :

		```python title="Recherche de clé"

		def est_present(arbre, cle):
			if est_vide(arbre):
				return False
			elif valeur(noeud) == cle:
				return True
			else:
				return est_present(sous_arbre_gauche(noeud), cle) or est_present(sous_arbre_droit(noeud), cle)
		```
