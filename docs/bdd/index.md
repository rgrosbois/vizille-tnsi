# Bases de données relationnelles

- [Introduction](01-Introduction.md)
- [Modélisations de bases de données relationnelles](02-BDDR.md)
- [SGBD et Langage SQL](03-SGBD_SQL.md)
- [Programmation et application](04-Programmation.md)

<small>(sources: vidéos [1](https://www.youtube.com/watch?v=pqoIBiM2AvE), [2](https://www.youtube.com/watch?v=jDXU8PvsG3U), [3](https://www.youtube.com/watch?v=T7AxM7Vqvaw&list=PLdo5W4Nhv31b33kF46f9aFjoJPOkdlsRc), [4](https://youtu.be/Iau0UY7UT8w?list=PLXGXMIp685itfuvUgP6-T7iM5CnqDsCjP), [5](http://nsi4noobs.fr/Modele-relationnel-Principe))</small>


Ce chapitre a pour objectifs de :

- sensibiliser à usage critique et responsable des données
- Savoir distinguer la structure d'une base de données de son contenu

    > La structure est un ensemble de schémas relationnels qui respecte les contraintes du modèle relationnel

- Récupérer des anomalies dans le schéma d'une base de données

    > Les anomalies peuvent être des redondances de données ou des anomalies d'insertion, de suppression, de mise à jour.

> On privilégie la manipulation de données nombreuses et réalistes.

