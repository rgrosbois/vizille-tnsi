---
hide:
  - navigation
---
# Introduction aux bases de données

!!! danger "Définition"

    Les bases de données[^1] permettent l'organisation, le stockage, la mise à jour et l'interrogation de données[^2] structurées (volumineuses) par différents programmes ou utilisateurs.

Types de bases de données: hiérarchique, réseau, **SQL/relationnelle**, orientée objet, texte, distribuée, cloud, noSQL, orientée graph...

[^1]: Base de donnée: collection de données cohérentes représentant des entités réelles.

[^2]: Donnée: *quelque chose* qui peut être enregistrée.

## 1. Historique

=== "1725" 
    ![Métier à tisser](img/01-ruban.png){ align=left width=55% }

    !!! info inline end
        Le français Basile Bouchon programme un métier à tisser 
		grâce à un ruban perforé.
		
		(codage d'instructions en binaire)
		
=== "1928"
	![Carte perforée](img/01-carte_perforee.jpg){align=left width=55% }
	
	!!! info inline end
		La société IBM brevète la carte perforée à 80 colonnes.
		
		(mémoire de masse binaire)
		
=== "1956"
	![Disque dur](img/01-hdd.jpg){align=left width=55% }
	
	!!! info inline end
		1er disque dur magnétique (IBM 350): 
		
		- ~1 tonne, 
		- 5 Mo, 
		- 1 200 tr/min,
		- 50 disques de 24 pouces, 
		- 2 têtes de lecture/écriture, 
		- 8.8 ko/s.

=== "1970"
	![Edgar Codd](img/01-E_Codd.jpg){align=left width=55% }
	
	!!! info inline end
		Edgar Franck Codd (1923-2003): mathématicien/informaticien britannique (IBM), pose les bases du modèle relationnel

=== "1979"
	![Visicalc](img/01-Visicalc.png){align=left width=55% }
	
	!!! info inline end
		Dan Bricklin créé le tableur Visicalc.


??? info "Autres dates significatives"

    1974
    : Création du langage SQL

    1979
    : Création premier SGBD: *Oracle* (Société de Larry Ellison)

    1980
    : Volume mondiale de données stockées: $10^{18}$ octets

    1990
    : Volume mondiale de données stockées: $10^{19}$ octets

    1995
    : Première version du SGBD *MySQL* (GPL) par Michael Widenius.

    1997
    : «Big Data»

    2002
    : Volume mondiale de données stockées: $10^{20}$ octets

    2009
    : Open Data (charte du G8 en 2013). Rachat de MySQL par Oracle. Création de MariaDB par Michael Widenius.

    2010
    : Volume mondiale de données stockées: $10^{21}$ octets

    2014
    : Volume mondiale de données stockées: $10^{22}$ octets

## 2. Données tabulaires

C'est une méthode qui consiste à **structurer** les données dans une table (tableau, liste 2D).

=== "Table"

    ![Table de données](img/01-Table.png)

=== "Descripteur"

    ![Descripteurs](img/01-Table-descripteurs.png)

    &rarr;  titre de colonne (nom des catégories)

=== "Objet"

    ![Objet](img/01-Table-Objet.png)

    &rarr; une ligne de la table (hors descripteurs)

=== "Valeur"

    ![Valeur](img/01-Table-Valeur.png)

    &rarr; une cellule de la table (hors descripteurs)

=== "Collection"

    ![Collection](img/01-Table-Collection.png)

    &rarr;  ensemble des lignes (hors descripteurs)

On les manipule généralement à l'aide de tableurs (Excel, Calc, Gnumeric...) et on les stocke/transfère dans des fichiers spécifiques ([CSV](img/01-tableau.txt), [XML](img/01-tableau.xml), [JSON](img/01-tableau.json), odt, xlsx...)

??? info "Format CSV"

    > (source: Wikipedia)
    >
    > Comma-separated values, connu sous le sigle CSV, est un format informatique ouvert représentant des données tabulaires sous forme de valeurs séparées par des virgules.
    >
    > Un fichier CSV est un fichier texte, par opposition aux formats dits « binaires ». 
    > 
    > Chaque ligne du texte correspond à une ligne du tableau et les virgules correspondent aux séparations entre les colonnes. 
    > Les portions de texte séparées par une virgule correspondent ainsi aux contenus des cellules du tableau.

## 3. Relations

C'est une ensemble de tables qui portent des noms et qui sont reliées entres elles.  Elles s'utilisent dans des bases de données (relationnelle).

=== "Relation"

    <figure markdown="span">
        <figcaption>Acteurs</figcaption>
        ![Relation](img/01-Relation.png)
    </figure>
    
=== "En-tête"

    <figure markdown="span">
        <figcaption>Acteurs</figcaption>
        ![En-tête](img/01-Relation-entete.png)
    </figure>

    &rarr; 1ère ligne (avec les noms d'attributs)

=== "Corps"

    <figure markdown="span">
        <figcaption>Acteurs</figcaption>
        ![Corps](img/01-Relation-Corps.png)
    </figure>

    &rarr; constitué d'un ou plusieurs tuples (ou enregistrements)

=== "Attribut"

    <figure markdown="span">
        <figcaption>Acteurs</figcaption>
        ![Attribut](img/01-Relation-Attribut.png)
    </figure>

    &rarr; une colonne de la relation, avec un nom (première cellule) et des valeurs (cellules suivantes). 
    
    Chaque valeur a un type et un domaine (liste de valeurs autorisées).

=== "tuple/enregistrement"

    <figure markdown="span">
        <figcaption>Acteurs</figcaption>
        ![Enregistrement](img/01-Relation-Enregistrement.png)
    </figure>

    &rarr; une ligne du corps la relation

Exemple: 

![Relations entre tables](img/01-relations_tables.png)

