---
hide:
  - navigation
---
# Bases de données dans le développement d'applications

## 1. Python

Presque tous les langages de programmation possèdent des modules/bibliothèques qui facilitent l'interaction avec des SGBD et donc le développement d'applications utilisant une base de données.

!!! example "Exemple simple" 

    Le module `sqlite3` permet d'intéragir avec une BDD SQLite (Cf. les 5 étapes ci-après)

    ```python
    import sqlite3

    # 1. Connexion
    connexion = sqlite3.connect('mabdd.db')

    # 2. Récupération d'un curseur
    c = connexion.cursor()

    # 3. Exécution de commande via le curseur
    c.execute("""
        CREATE TABLE IF NOT EXISTS notes(
        Nom TEXT,
        Note INT);
        """)


    while True :
        nom = input('Nom ? ')
        if nom in ['Q','q'] :
            break
        note = input('Note ? ')
        data = (nom, note)
        p = "INSERT INTO notes VALUES ('" + nom + "','" + note + "')"

        c.executescript(p)


    # 4. Enregistrement des modifications
    connexion.commit()


    # 5. Déconnexion
    connexion.close()
    ```

??? info "Autres modules"

    - psycopg (pour Postgresql)
    - ...

## 2. Injection de code

L'injection SQL est une technique consistant à écrire du code SQL à un endroit qui n'est pas censé en recevoir (problème de *cybersécurité*).

??? example "Exemple d'injection"

    On fournit la chaîne de caractère suivante pour le nom: `g','3'); DROP TABLE notes;--` (peu importe ce que l'on rentre pour la note).

    ??? info "Explications"

        Ce code a été fait spécifiquement pour être vulnérable à l'injection SQL. Il suffit d'ailleurs de remplacer le `c.executescript(p)` par `c.execute(p)` pour que le code reste fonctionnel mais refuse l'injection SQL. Ceci dit, de nombreux serveurs sont encore attaqués par cette technique, au prix de manipulations bien sûr plus complexes que celles que nous venons de voir. 
