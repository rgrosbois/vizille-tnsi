---
hide:
  - navigation
---
# SGBD et SQL

Ces deux outils sont devenus indispensables depuis la création des bases de données relationnelles en 1970.

## 1. SGBD relationnelles

??? info "Exemples de SGBD" 

    ![Évolution des SGBD](img/03-evolution-SGBD.png)

    <small>Quelques interfaces graphiques: *DB Browser for SQLite*,  [sqliteonline](https://sqliteonline.com/), *phpMyAdmin*...</small>

*[SGBD]: Système de Gestion de Bases de Données
*[BDD]: Base De Données

Ils sont apparus dans les années 1980 et servent d'intermédiaire entre les applications (via le langage SQL) et les BDD : 

!!! important "Modèles d'architecture"

    - Client/serveur:

        <figure markdown="span">
        ![Architecture client/serveur](img/src/03-BDD-client_server.svg)
        </figure>

    - 3 niveaux: client/serveur + interface
    - BDD dans le cloud (où sont stockées les données). 

Selon les implémentations, ils offrent :

*[CRUD]: Create, Read, Update and Delete

- un moyen pratique et fiable pour accéder et manipuler les données (CRUD);
- la persistance et la protection des données (réplication...);
- une gestion des accès concurrents (transactions, verrous, journalisation...);
- une efficacité de traitement des requêtes;
- la sécurisation des accès (authentification, autorisation, chiffrement...);
- l'intégrité des données

    !!! info "Contraintes d'intégrité"

        Règles qui permettant de garantir la cohérence des données lors de la mise à jour de la base. On distingue principalement 3 catégories :

        1. intégrité de **relation** (ou de clés, d'entité) : tout enregistrement est unique (pas de doublons), la clé primaire n'est jamais nulle.
        2. intégrité de **domaine** : tout attribut doit respecter le domaine indiqué (prédéfini ou non).
        3. intégrité de **référence**: la valeur d'une clé étrangère doit obligatoirement pouvoir être retrouvée dans la table où cet attribut est clé primaire (il ne faut aucune référence vers un enregistrement supprimé)

        ??? info "4ème catégorie (Hors programme NSI)"
        
            Contrainte d'intégrité **utilisateur** : contrainte qui n'entre pas dans les 3 catégories précédentes. 
        
            Exemple: numéro de portable doit débuter par `06` ou `07`

## 2. Langage SQL

*[SQL]: Structured Query Language

Ce langage a été créé en 1970 et est devenu un standard ANSI/ISO en 1986. Il définit la manière d'intéragir (requête, réponse) avec les SBGD[^1].

[^1]: Dans la pratique, les instructions SQL peuvent légèrement varier d'un SGBD à un autre.

!!! warning "Conventions générales"

    - Chaque instruction se termine par `;`. 
    - Les indentations et retour à la ligne servent à clarifier le code mais sont optionnels. 
    - Les mots clés sont écrits en majuscules.
    - Si nécessaire, ajouter des commentaires (précédé par `--`)

=== "Consultation"
    
    Une requête se construit à l'aide des mots-clés `SELECT`, `FROM`, `WHERE` et `JOIN`. La réponse est (généralement) une table.

    ```sql title="Requête générale (Terminale NSI)"
    SELECT liste d’attributs (et fonctions d’agrégation)
        FROM ma_table1
            INNER JOIN ma_table2 ON égalité d’attributs
        WHERE condition(s)
        ORDER BY attributs de tri (chaque attribut suivi de DESC ou ASC)
    ```

    === "Une seule relation"

        1. Requêtage simple: `SELECT *` 

            (`*` indique que l'on veut les valeurs de tous les attributs)

            ```sql
            SELECT * 
                FROM ma_table;
            ```  

            ??? tip "Limiter les lignes"

                Pour **limiter** le nombre de lignes affichées (ici les 5 premiers):
            
                ```sql
                SELECT *
                FROM ma_table
                LIMIT 5;
                ```
            
            ??? tip "Ordonner" 
            
                Pour ordonner les lignes selon un attribut ou un numéro de colonne (tri par valeurs croissantes par défaut): 

                ```sql
                SELECT * 
                    FROM ma_table
                    ORDER BY mon_attribut;
                ```

                ??? tip "Décroissant"

                    ```sql
                    SELECT * 
                        FROM ma_table
                        ORDER BY mon_attribut DESC;
                    ```

        2. Filtrage/Restrictions: `WHERE` suivi d'un test logique 
            
            ```sql
            SELECT * 
                FROM ma_table
                WHERE mon_attribut >= 5;
            ```

            ??? info "Opérateurs/Fonctions"
            
                - comparaisons: `=`, `<>` (ou `!=`), `<`, `<=`, `>`, `>=`, 
                - logiques: `AND`, `OR`, `IS NULL`, `IS NOT NULL`, 
                - plages/ensembles: `BETWEEN ... AND ...`, `IN`, 
                - chaînes de caractères: `LIKE` (+ caractères `%` et `_`), `UPPER`, `LOWER`, `||`, `SUBSTR`, `LENGTH`, `REPLACE`, `INSTR`
                - dates: `DATE`, `STRFTIME`

        3. Projection: `SELECT Attr1, Attr2`
        
            Pour limiter l'affichage à quelques attributs.

            ```sql
            SELECT Attr1, Attr2
                FROM ma_table;
            ```

            ??? tip "Nouvel attribut (calculé)"

                ```sql
                SELECT Attribut1, Attribut1/Attribut2
                    FROM ma_table;
                ```

            ??? tip "Doublons: `DISTINCT`"

                Ils peuvent apparaître lorsqu'on n'affiche pas tous les attributs. Pour les supprimer:

                ```sql
                SELECT DISTINCT Attr1, Attr2
                    FROM ma_table;
                ```
        
            ??? tip "Renommage: `AS`"

                ```sql
                SELECT Attribut1 AS Att1
                    FROM ma_table;
                ```

        4. Agrégation `COUNT`

            Cette fonction permet de comptabiliser le nombre d'enregistrements correspondant à la requête.

            ```sql
            SELECT COUNT(*)
                FROM ma_table;
            ```

            ??? tip "Autres fonctions"

                Il existe aussi les fonctions d'agrégation `SUM` (somme), `AVG` (valeur moyenne), `MAX` (maximum)...

    === "Plusieurs relations"

        On appelle **jointure** l'action de fusionner tout ou partie de plusieurs relations. 
        
        Il existe plusieurs types de jointures (*naturelle*, *produit cartésien*, *externe*...), mais nous nous limiterons, en NSI, aux jointures **internes** (`JOIN ... ON ...`):

        ```sql
        SELECT *
            FROM ma_table1 JOIN ma_table2 
                ON ma_table1.attr1 = ma_table2.attr2;
        ```

        ??? info "INNER JOIN"

            Pour la distinguer des autres types de jointures, il est aussi possible d'utiliser ce mot clé:
            ```sql
            SELECT *
                FROM ma_table1 INNER JOIN ma_table2 
                    ON ma_table1.attr1 = ma_table2.attr2;
            ```

        (où `attr1` et `attr2` sont les attributs utilisés pour fusionner les relations respectives `ma_table1` et `ma_table2`)

        - Alias: on peut renommer temporairement les tables pour simplifier l'écriture

            ```sql
            SELECT *
                FROM ma_table1 AS t1 JOIN ma_table2 AS t2
                    ON t1.attr1 = t2.attr2;
            ```

        - Jointures multiples: les jointures peuvent concerner plus que 2 relations

            ```sql
            SELECT *
                FROM ma_table1 AS t1 
                    JOIN ma_table2 AS t2 ON t1.attr11 = t2.attr2
                    JOIN ma_table3 AS t3 ON t1.attr13 = t3.attr3;
            ```

        - Les mots clés avancés vus pour une seule relation (`COUNT`, `WHERE`, `ORDER BY`...) restent utilisables pour les requêtes utilisant plusieurs relations.

    === "Sous-requêtes"

        SQL permet d'encapsuler une sous-requête (qui renvoie une table) dans une requête. Ceci permet:

        - d'éviter les jointures qui peuvent être parfois longues à effectuer,
        - de faire des calculs et d'utiliser le résultat dans une autre requête,
        - ...

        Exemple: dans la clause `WHERE`[^2]

        [^2]: une sous-requête peut aussi s'utiliser dans une clause `FROM`.

        ```sql
        SELECT *
            FROM ma_table1
            WHERE attr1 IN (SELECT attr2
                                FROM ma_table2
                                WHERE attr3 = 'valeur')
        ```

        (`IN` peut être remplacé par `=` si la sous-requête ne renvoie qu'une seule valeur)

=== "Maintenance" 

    ??? warning "Création/suppression de relation (Hors programme NSI)"

        ```sql title="Création"
        CREATE TABLE ma_table (
            Attribut1 type de données, (contrainte(s))
            …
            PRIMARY KEY attribut
            FOREIGN KEY attribut REFERENCES autre_table.attribut
        );
        ```

        - Types de données (selon SGBD):

            - Entiers: INT/INTEGER (TINYINT, SMALLINT, MEDIUMINT, BIGINT)
            - Décimaux: NUMERIC/DECIMAL (=chaine de carac), FLOAT/REAL/DOUBLE
            - Chaînes de caractères: CHAR/VARCHAR, TEXT (TINYTEXT, SMALLTEXT, LONGTEXT)
            - DATE, TIME, DATETIME
            - BLOB (Binary Large OBject): image, vidéo, audio...
            - NULL

        - Contraintes d'intégrité: 

            - clé primaire: `PRIMARY KEY`
            - clé étrangère: `REFERENCES <nom de table>` (`FOREIGN KEY`)
            - unicité: `UNIQUE`
            - non-nullité : `NOT NULL`
            - contrainte utilisateur : `CHECK`


        ```sql title="Suppression de table"
        DROP table (IF EXISTS);
        ```

    === "Insertion de données"

        ```sql title="INSERT INTO"
        INSERT INTO ma_table 
            (liste d’attributs facultative) 
        VALUES liste de valeurs;
        ```

        *(certains attributs peuvent ne pas avoir besoin de valeur &rarr; NULL)*

    === "Mise à jour"

        ```sql title="UPDATE ... SET"
        UPDATE ma_table 
        SET att1 = val1, att2 = val2,… 
            WHERE condition;
        ```

    === "Suppression"

        ```sql title="DELETE FROM"
        DELETE FROM ma_table 
            WHERE condition;
        ```

=== "Utilisateurs"

    La gestion peut varier d'un SGBD à l'autre (pas d'utilisateur dans SQLite)

    !!! info "MariaDB (Hors programme NSI)"

        ??? example inline end "Exemple d'installation"

            [Installer et Configurer un LAMP](https://eskool.gitlab.io/tnsi/bdd/sql/td/install_lamp/)

        Ils sont déclarés dans une BDD interne. 

        ```{.console .rgconsole title="Connexion en tant qu'administrateur"}
        [nsi@fedora ~]$ mysql -u root -p
        Enter password: *****
        MariaDB [(none)]>
        ```

        ??? tip inline end "Interface graphique"

            Il existe diverses interfaces graphiques qui facilitent la gestion des utilisateurs:

            <figure mardown="span">
                ![phpMyAdmin](img/03-phpMyAdmin.png)
                <figcaption>Application Web phpMyAdmin</figcaption>
            <figure>

        Chaque utilisateur possède un:

        - identifiant/mot de passe (**authentification**)

            ```sql title="Création d'un nouvel utilisateur local en indiquant son mot de passe"
            MariaDB [(none)]> CREATE USER monutilisateur@localhost IDENTIFIED BY '*****';
            ```

        - des privilèges (**autorisation**)

            ```sql title="Rajouter tous les privilèges pour `monutilisateur` sur `mabdd`"
            MariaDB [(none)]> GRANT ALL ON mabdd.* TO monutilisateur@localhost;
            MariaDB [(none)]> FLUSH PRIVILEGES;
            ```

        - Liste des bases de données internes

            ```console title=""
            MariaDB [(none)]> SHOW Databases;
            +--------------------+
            | Database           |
            +--------------------+
            | information_schema |
            | mysql              |
            | performance_schema |
            | sys                |
            +--------------------+
            ```

        - Base de données `mysql` contenant les utilisateurs (table `user`)

            ```console title=""
            MariaDB [(none)]> USE mysql; 
            MariaDB [(none)]> SHOW TABLES;
            +---------------------------+
            | Tables_in_mysql           |
            +---------------------------+
            | column_stats              |
            | columns_priv              |
            ...
            | user                      |
            +---------------------------+
            ```

        - Schéma de la relation `user`: *nom de machine, utilisateur, mot de passe, privilèges...*

            ```console title=""
            MariaDB [mysql]> SHOW COLUMNS FROM user;
            +------------------------+---------------------+------+-----+----------+-------+
            | Field                  | Type                | Null | Key | Default  | Extra |
            +------------------------+---------------------+------+-----+----------+-------+
            | Host                   | char(255)           | NO   |     |          |       |
            | User                   | char(128)           | NO   |     |          |       |
            | Password               | longtext            | YES  |     | NULL     |       |
            | Select_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Insert_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Update_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Delete_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Create_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Drop_priv              | varchar(1)          | YES  |     | NULL     |       |
            | Reload_priv            | varchar(1)          | YES  |     | NULL     |       |
            | Shutdown_priv          | varchar(1)          | YES  |     | NULL     |       |
            ...
            ```
