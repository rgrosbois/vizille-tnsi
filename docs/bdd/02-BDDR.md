---
hide:
  - navigation
---
# Modélisations de bases de données

??? example "Exemple"

    <small>(sources: vidéos [1](https://www.youtube.com/watch?v=mAMOWvbxMkE), [2](https://www.youtube.com/watch?v=j1fB8RKAZJc), [3](https://www.youtube.com/watch?v=nzD-cXS3bcM), [4](https://www.youtube.com/watch?v=ThuKCep7hwE))</small>

    Une plateforme de formation en ligne propose des cours (payants) à des abonnés (appelés étudiants).

    === "Données tabulaires"

        Les données sont initialement stockées dans une table (et manipulées à l'aide d'un tableur):

        | Étudiant | Inscription site | Email | Nom du cours | Professeur | Inscription cours | Prix | Note | 
        |----------|-----------------------|-------|--------------|------------|------------------------|--------------|------|
        | ...      | ...                   | ...   | ...          | ...          | ... | ... | ... | ... |
        | Chris MURRAY | 04-06-2021 | Chrismurray.26@gmail.com | Algèbre Linéaire pour la Data | Louise FAUVILLE | 06-06-2021 | 13.99€ | C- |
        | Amine NAHAL | 25-05-2021 | Amine.nahal@yahoo.fr | Programmation Python | Océane DUBOIS | 27-05-2021 | 8.57€ | B+ |
        | François VERGER |  23-04-2021 | Francois.verger.03@gmail.com | Introduction aux Bases de Données | David FIEL | 23-04-2021 | 5.59€ | A- |
        | Adam LEPONT | 13-07-2021 | Adam.lepont.56@gmail.com | Anglais pour débutants | Paul BARDIN | 13-07-2021 | 20.99€ | B- |
        | François VERGER | 23-04-2021 | Francois.verger.03@gmail.com | Apprendre le SQL | Steve SAMELSON | 12-05-2021 | 14.99€ | B+ |
        | Boris BROWN | 26-06-2021 | Boris.brown@yahoo.fr | Programmation Python | Océane DUBOIS | 28-06-2021 | 8.57€ | C+ |
        | ...      | ...                   | ...   | ...          | ...          | ... | ... | ... | ... |

        Mais le développement de l'activité (augmentation du volume de données, plusieurs utilisateurs du tableau, sauvegarde...) fait très rapidement apparaître des limites à cette structure.

    === "Ambiguités"

        Le premier problème est celui de l'ambiguité de certaines valeurs :

        - Les personnes pourraient avoir des homonymes (les adresses email ne sont pas forcément des valeurs pérennes).
        - Les cours pourraient être réactualisés sans vouloir pour autant changer leur dénomination.

        On peut lever ses *ambiguités* en **rajoutant des attributs** avec des valeurs **uniques et définitives**[^1] pour les cours et les personnes: `N° Étudiant`, `Code Cours` et `Code Professeur`.

        [^1]: On verra, un peu plus loin, que cela consiste à définir une clé primaire dans la table.

        ??? success "Résultat"

            | ==N° Étudiant== | Étudiant | Inscription site | Email | ==Code Cours== | Nom du cours | ==Code Professeur== | Professeur | Inscription cours | Prix | Note | 
            |-------------|----------|-----------------------|-------|------------|--------------|-----------------|------------|------------------------|--------------|------|
            | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
            | ==530029== | Chris MURRAY | 04-06-2021 | Chrismurray.26@gmail.com | ==MT256== | Algèbre Linéaire pour la Data | ==P563007== | Louise FAUVILLE | 06-06-2021 | 13.99€ | C- |
            | ==486325== | Amine NAHAL | 25-05-2021 | Amine.nahal@yahoo.fr | ==IPY367== | Programmation Python | ==P756235== | Océane DUBOIS | 27-05-2021 | 8.57€ | B+ |
            | ==004864== | François VERGER |  23-04-2021 | Francois.verger.03@gmail.com | ==IBD610== | Introduction aux Bases de Données | ==P300741== | David FIEL | 23-04-2021 | 5.59€ | A- |
            | ==146357== | Adam LEPONT | 13-07-2021 | Adam.lepont.56@gmail.com | ==ANG236== | Anglais pour débutants | ==P289963== | Paul BARDIN | 13-07-2021 | 20.99€ | B- |
            | ==004864== | François VERGER | 23-04-2021 | Francois.verger.03@gmail.com | ==SQL745== | Apprendre le SQL | ==P246982== | Steve SAMELSON | 12-05-2021 | 14.99€ | B+ |
            | ==008600== | Boris BROWN | 26-06-2021 | Boris.brown@yahoo.fr | ==IPY367== | Programmation Python | ==P756235== | Océane DUBOIS | 28-06-2021 | 8.57€ | C+ |
            | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
        
    === "Atomicité"

        La maintenance d'une relation est grandement facilité si elle ne contient que des valeurs *atomiques*[^2].

        Ici les attributs `Étudiant` et `Professeur` peuvent êtres chacun scindés en 2 sous-attributs `nom` et `prénom`.

        ??? success "Résultat"

            | N° Étudiant | ==Prénom Étudiant== | ==Nom Étudiant== | Inscription site | Email | Code Cours | Nom du cours | Code Professeur | ==Prénom Professeur== | ==Nom Professeur== | Inscription cours | Prix | Note | 
            |-------------|-----------------|--------------|------------------|-------|------------|--------------|-----------------|-------------------|----------------|-------------------|------|------|
            | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
            | 530029 | ==Chris== | ==MURRAY== | 04-06-2021 | Chrismurray.26@gmail.com | MT256 | Algèbre Linéaire pour la Data | P563007 | ==Louise== | ==FAUVILLE== | 06-06-2021 | 13.99€ | C- |
            | 486325 | ==Amine== | ==NAHAL== | 25-05-2021 | Amine.nahal@yahoo.fr | IPY367 | Programmation Python | P756235 | ==Océane== | ==DUBOIS== | 27-05-2021 | 8.57€ | B+ |
            | 004864 | ==François== | ==VERGER== |  23-04-2021 | Francois.verger.03@gmail.com | IBD610 | Introduction aux Bases de Données | P300741 | ==David== | ==FIEL== | 23-04-2021 | 5.59€ | A- |
            | 146357 | ==Adam== | ==LEPONT== | 13-07-2021 | Adam.lepont.56@gmail.com | ANG236 | Anglais pour débutants | P289963 | ==Paul== | ==BARDIN== | 13-07-2021 | 20.99€ | B- |
            | 004864 | ==François== | ==VERGER== | 23-04-2021 | Francois.verger.03@gmail.com | SQL745 | Apprendre le SQL | P246982 | ==Steve== | ==SAMELSON== | 12-05-2021 | 14.99€ | B+ |
            | 008600 | ==Boris== | ==BROWN== | 26-06-2021 | Boris.brown@yahoo.fr | IPY367 | Programmation Python | P756235 | ==Océane== | ==DUBOIS== | 28-06-2021 | 8.57€ | C+ |
            | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |

        [^2]: Une valeur est atomique si elle ne contient pas plusieurs informations.

## 1. Attributs

*Rappel: les attributs d'une relation correspondent aux colonnes de la table. Ils possèdent un nom et ont des valeurs.*

!!! warning "Redondances dans une relation"

    Elle compliquent le stockage et la maintenance et sont sources d'erreurs (mises à jour, duplicats non identiques...). 

    ??? example "Exemple"

        - Les étudiants qui suivent plusieurs cours apparaissent sur plusieurs lignes.
        - Les cours suivis par plusieurs étudiants font apparaitre les mêmes intitulés (et souvent professeurs) sur plusieurs lignes.
        - Des professeurs donnant plusieurs cours
        - ...

### a. Clés

!!! danger "Clé primaire" 

    C'est un attribut (ou un ensemble d'attributs) dont chaque valeur identifie un enregistrement de manière unique.

??? example "Exemple"

    Nous considérons ici une clé primaire composée de 3 attributs (`N° Étudiant`, `Code cours`, `Code Professeur`)[^3]. 

    [^3]: `Code Professeur` pourrait être enlevé de la clé si on considère que chaque cours est toujours dispensé par le même professeur.
    
    === "Clé primaire"

        | ==N° Étudiant== | Prénom Étudiant | Nom Étudiant | Inscription site | Email | ==Code Cours== | Nom du cours | ==Code Professeur== | Prénom Professeur | Nom Professeur | Inscription cours | Prix | Note | 
        |-------------|-----------------|--------------|------------------|-------|------------|--------------|-----------------|-------------------|----------------|-------------------|------|------|
        | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
        | **530029** | Chris | MURRAY | 04-06-2021 | Chrismurray.26@gmail.com | **MT256** | Algèbre Linéaire pour la Data | **P563007** | Louise | FAUVILLE | 06-06-2021 | 13.99€ | C- |
        | **486325** | Amine | NAHAL | 25-05-2021 | Amine.nahal@yahoo.fr | **IPY367** | Programmation Python | **P756235** | Océane | DUBOIS | 27-05-2021 | 8.57€ | B+ |
        | **004864** | François | VERGER |  23-04-2021 | Francois.verger.03@gmail.com | **IBD610** | Introduction aux Bases de Données | **P300741** | David | FIEL | 23-04-2021 | 5.59€ | A- |
        | **146357** | Adam | LEPONT | 13-07-2021 | Adam.lepont.56@gmail.com | **ANG236** | Anglais pour débutants | **P289963** | Paul | BARDIN | 13-07-2021 | 20.99€ | B- |
        | **004864** | François | VERGER | 23-04-2021 | Francois.verger.03@gmail.com | **SQL745** | Apprendre le SQL | **P246982** | Steve | SAMELSON | 12-05-2021 | 14.99€ | B+ |
        | **008600** | Boris | BROWN | 26-06-2021 | Boris.brown@yahoo.fr | **IPY367** | Programmation Python | **P756235** | Océane | DUBOIS | 28-06-2021 | 8.57€ | C+ |
        | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |

    === "Sous-relations"

        Dans une relation *bien conçue*, chaque attribut (non clé) doit dépendre complètement de la clé primaire.

        ??? success "Résultat"

            Il faut décomposer la relation en 4 sous-relations (*sous-tables*), chacune avec sa clé primaire.


            === "Étudiants"

                === "Brute"

                    | N° Étudiant | Prénom Étudiant | Nom Étudiant | Inscription site | Email | 
                    |-------------|-----------------|--------------|------------------|-------|
                    | ...         | ...             | ...          | ...              | ...   |
                    | **530029**  | Chris           | MURRAY       | 04-06-2021 | Chrismurray.26@gmail.com |
                    | **486325**  | Amine           | NAHAL        | 25-05-2021 | Amine.nahal@yahoo.fr |
                    | ==**004864**== | ==François== | ==VERGER==   | ==23-04-2021== | ==Francois.verger.03@gmail.com== | 
                    | **146357**  | Adam            | LEPONT       | 13-07-2021 | Adam.lepont.56@gmail.com | 
                    | ==**004864**== | ==François== | ==VERGER==   | ==23-04-2021== | ==Francois.verger.03@gmail.com== |  
                    | **008600**  | Boris           | BROWN        | 26-06-2021 | Boris.brown@yahoo.fr | 
                    | ...         | ...             | ...          | ...              | ...   |

                === "Sans redondances"

                    | N° Étudiant | Prénom Étudiant | Nom Étudiant | Inscription site | Email | 
                    |-------------|-----------------|--------------|------------------|-------|
                    | ...         | ...             | ...          | ...              | ...   |
                    | **530029**  | Chris           | MURRAY       | 04-06-2021 | Chrismurray.26@gmail.com |
                    | **486325**  | Amine           | NAHAL        | 25-05-2021 | Amine.nahal@yahoo.fr |
                    | **004864**  | François        | VERGER       | 23-04-2021 | Francois.verger.03@gmail.com | 
                    | **146357**  | Adam            | LEPONT       | 13-07-2021 | Adam.lepont.56@gmail.com | 
                    | **008600**  | Boris           | BROWN        | 26-06-2021 | Boris.brown@yahoo.fr | 
                    | ...         | ...             | ...          | ...              | ...   |

            === "Cours"

                === "Brute"

                    | Code Cours | Nom du cours |
                    |------------|--------------|
                    | ...        | ...          |
                    | **MT256**  | Algèbre Linéaire pour la Data |
                    | ==**IPY367**== | ==Programmation Python== |
                    | **IBD610** | Introduction aux Bases de Données |
                    | **ANG236** | Anglais pour débutants |
                    | **SQL745** | Apprendre le SQL |
                    | ==**IPY367**== | ==Programmation Python== |
                    | ...        | ...                  |

                === "Sans redondance"

                    | Code Cours | Nom du cours |
                    |------------|--------------|
                    | ...        | ...          |
                    | **MT256**  | Algèbre Linéaire pour la Data |
                    | **IPY367** | Programmation Python |
                    | **IBD610** | Introduction aux Bases de Données |
                    | **ANG236** | Anglais pour débutants |
                    | **SQL745** | Apprendre le SQL |
                    | ...        | ...                  |

            === "Professeurs"

                === "Brute"

                    | Code Professeur | Prénom Professeur | Nom Professeur |
                    |-----------------|-------------------|----------------|
                    | ...             | ...               | ...            |
                    | **P563007**     | Louise            | FAUVILLE       |
                    | ==**P756235**== | ==Océane==        | ==DUBOIS==     |
                    | **P300741**     | David             | FIEL           |
                    | **P289963**     | Paul              | BARDIN         |
                    | **P246982**     | Steve             | SAMELSON       |
                    | ==**P756235**== | ==Océane==        | ==DUBOIS==     |
                    | ...             | ...               | ...            |

                === "Sans redondance"

                    | Code Professeur | Prénom Professeur | Nom Professeur |
                    |-----------------|-------------------|----------------|
                    | ...             | ...               | ...            |
                    | **P563007**     | Louise            | FAUVILLE       |
                    | **P756235**     | Océane            | DUBOIS         |
                    | **P300741**     | David             | FIEL           |
                    | **P289963**     | Paul              | BARDIN         |
                    | **P246982**     | Steve             | SAMELSON       |
                    | ...             | ...               | ...            |


            === "Etudiants/Cours/Professeurs"

                | N° Étudiant | Code Cours | Code Professeur | Inscription cours | Prix | Note | 
                |-------------|------------|-----------------|-------------------|------|------|
                | ...         | ...        | ...             | ...               | ...  | ...  |
                | **530029**  | **MT256**  | **P563007**     | 06-06-2021        | 13.99€ | C- |
                | **486325**  | **IPY367** | **P756235**     | 27-05-2021        | 8.57€  | B+ |
                | **004864**  | **IBD610** | **P300741**     | 23-04-2021        | 5.59€  | A- |
                | **146357**  | **ANG236** | **P289963**     | 13-07-2021        | 20.99€ | B- |
                | **004864**  | **SQL745** | **P246982**     | 12-05-2021        | 14.99€ | B+ |
                | **008600**  | **IPY367** | **P756235**     | 28-06-2021        | 8.57€  | C+ |
                | ...         | ...        | ...             | ...               | ...  | ...  |
            

!!! danger "Clé étrangère"

    C'est un attribut qui est une clé primaire dans une autre relation.

### b. Domaine 

Pour assurer l'*intégrité* d'une base de données et optimiser le stockage, il est important d'identifier le **domaine** de chaque attribut, c'est à dire l'ensemble (fini ou infini) des valeurs acceptées et leur type:

- Nombres entiers (sur *n* bits) ou flottants
- chaînes de caractères (*N* caractères au maximum)
- dates
- ...

## 2. Création d'une BDDR

<small>(sources: vidéos [1](https://www.youtube.com/watch?v=iVGS4zA_ETA), [2](https://www.youtube.com/watch?v=e9nsUUWWryk), [3](https://www.youtube.com/watch?v=BoMNHJq9YDg))</small>

Les opérations[^4] que nous venons d'effectuer pour *optimiser* la base de donné du site de formation en ligne résultent du fait que la relation initiale était initialement mal conçue.

*[BDDR]: Base De Données Relationnelle

[^4]: Les modifications effectuées s'appellent des normalisations.

!!! info "Les 5 étapes de création d'une BDDR"

    1. Définition des besoins
    2. Écriture d'un texte

        ??? example "Exemple 2"

            Une entreprise réalise des projets. Chaque projet a une date de début, une date de fin, un nom et un descriptif. Un ou plusieurs employés sont affectés à un projet, pour une durée variable. Les employés travaillent sur un ou plusieurs projets soit en même temps, soit successivement dans le temps. L'entreprise est divisée en services. Chaque employé travaille dans un et un seul service.

            Chaque employé travaillant dans l'entreprise a un numéro de matricule, un nom, une adresse et une fonction. Chaque service au sein de l'entreprise a un code, un nom et un chef.

    3. Création d'un MCD 
    4. Obtention d'un modèle relationnel
    5. Normalisation des relations

*[MCD]: Modèle Conceptuel de Données

### a. Modèle E/A (MCD)

*(Modèle Entité-Association)*

<small>Logiciels utiles: [MoCoDo](https://www.mocodo.net/), [dbconcept](https://dbconcept.tuxfamily.org/) , [Looping](https://www.looping-mcd.fr/) (sous Windows), AnalyseSI, Looping, JMerise, Dbdiagram...</small>

??? warning "Diagramme de type *Merise* (Hors programme NSI)"

    - **Entité** (objet abstrait ou concret de la réalité dont on veut stocker les données)
    
        Représentée par un rectangle avec son nom dans la partie supérieure et ses attributs dans la partie inférieure (la clé primaire est donnée en premier, en gras ou soulignée).
        
    - **Association** (*c'est un verbe*) 
    
        Elle est représentée par un trait entre 2 entités avec une ellipse contenant le nom de l'association dans sa partie supérieure

        - Il y a parfois des attributs d'association: propriété reliée aux 2 entités connectées, écrit dans la partie inférieure de l'ellipse
        - les cardinalités (0, 1 ou N) sont indiquées au format `min, max` sur les traits à gauche et droite de l'association (attention à l'emplacement)


    ??? example "Exemple 2"

        ![MCD exemple 2](img/02-MCD_exemple2.svg)

        <small>([modif](https://www.mocodo.net/?mcd=eNpFjTEOwjAMRfecIgfo0rUbA2JDlWBHwfkRQUlcuUmlHolzcDGqKBHTf9-yn2fhN_KkU4kQHnTiOGhrMh4Wz5IbO58Owkril-w5qZNzoAwZ9HjVc3VUPMcl8P79TNoWAdS_R5PFUwloPxZBTWMF63pMHSeq7ruYzfgQmr0rarlBNk9QLSdNbLuRXnBKXSD1cOy7lbvkBwZiVDo=))</small>

### b. Modèle relationnel

Il sert à construire la base de données. Il n'est composé que des tables (relations) et peut s'obtenir à partir du MCD. 

- Représentation graphique (ou *graphe relationnel*):

    - Une relation est un rectangle avec un nom (partie supérieure) et des attributs (partie inférieure). 
    
    - Une clé primaire est indiquée en gras (et/ou soulignée), une clé étrangère est précédée par `#`. 
    
    - On indique la provenance des clés étrangères à l'aide de flêches.

    ??? example "Exemple initial"

        === "Basique"

            ![Modèle relationnel](img/src/02-MR_etudiants-cours-prof.svg)

        === "Avec domaines"

            ![Modèle relationnel](img/src/02-MR_etudiants-cours-prof-domaines.svg)

- Représentation textuelle (ou *schéma relationnel*):

    !!! example "Exemple initial"

        <small>
        Étudiants (<u>**e_numero**</u> : INT, e_prenom : STR, e_nom : STR, e_inscription : DATE, e_email : STR)<br>
        Cours (<u>**c_code**</u> : STR, c_nom : STR)<br>
        Professeurs (<u>**p_code**</u> : STR, p_prenom : STR, p_nom : STR)<br>
        Étudiants-Cours-Profs ( <u>**#e_numero**</u> : INT, <u>**#c_code**</u> : STR, <u>**#p_code**</u> : STR, ecp_inscription : DATE, ecp_prix : FLOAT, ecp_note: STR)<br>
        </small>

??? warning "Règles de passage du modèle E/A au modèle relationnel (Hors Programme NSI)"

    <small>(sources: vidéos [1](https://www.youtube.com/watch?v=5MkOWO2bt-4), [2](https://www.youtube.com/watch?v=hmILJfYI9cw), [3](https://www.youtube.com/watch?v=D2gcK0i7mK0), [4](https://www.youtube.com/watch?v=JzkUzVJxCmE))</small>

    1. Une entité correspond à une table, ses attribuent constituent les colonnes
    2. Associations binaires:

        -  (x,1)-(x,N) avec x valant 0 ou 1: la clé primaire issue de l'entité côté cardinalité (0,N) ou (1,N) est dupliquée dans la table issue de l'entité côté cardinalité (0,1) ou (1,1) où elle devient clé étrangère.
        - (0,1)-(1,1): la clé primaire issue de l'entité côté cardinalité (0,1) est dupliquée dans la table issue de l'entité côté cardinalité (1,1) où elle devient clé étrangère.
        - (0,1)-(0,1): on choisit la clé primaire d'une des table pour la dupliquer en clé étrangère dans l'autre.
        - (1,1)-(1,1): (rare) voir à fusionner les 2 tables
        - (x,N)-(x,N) avec x valant 0 ou 1: création d'une troisième table qui a comme clé primaire, une clé composée des clés primaires de 2 autres tables (on ajoute les éventuels attributs de l'association en tant qu'attribut de la table). 

### c. Normalisation

??? warning end "Formes normales (Hors programme NSI)"

    Les formes normales d'une relation s'*emboîtent*:

    - 1ère forme normale (1FN): tous les attributs possèdent des valeurs atomiques 

        - *diviser* des colonnes
        - *dupliquer* des lignes

    - 2ème forme normale (2FN): 1FN + les attributs non clés doivent dépendre complètement (non partiellement) de la clé primaire 

        - décomposer en sous-tables séparées
        - recopier les clés primaires
        - déplacer les attributs dans les bonnes sous-tables.

    - 3ème forme normale (3FN): 2FN + les attributs non clés ne dépendent pas d'un ou plusieurs attributs ne participant pas à la clé 

        - déplacer ces attributs dans une nouvelle table
        - recopier l'attribut dont ils dépendent en tant que clé primaire
