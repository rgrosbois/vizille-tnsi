---
hide:
  - navigation
---
# Les graphes

Quelques exemples déjà rencontrés:

=== "Réseau routier"

    ![Iténéraire routier](img/04-iteneraire_routier.svg)    

=== "Routage informatique"

    ![Routage](img/04-reseau_routeurs.svg)

=== "Réseau social"

    ![Réseau social](img/04-reseau_social.png)

=== "Web"

    ![Web](img/04-microweb.png)

=== "Labyrinthe"

    | Problème | Modélisation |
    |:--------:|:------------:|
    | ![Labyrinthe](img/src/04-labyrinthe.svg) | ![Labyrinthe](img/src/04-labyrinthe-noeuds.svg) |

## I. Vocabulaire

Un graphe est constitué :

```mermaid
flowchart LR
    a((a)) --- b((b)) & c((c)) --- d((d))
```

- De **sommets** (ou **nœuds** ou *vertex*) qui contiennent (ou non) une donnée.
- D'**arêtes** (ou *edge*) qui relient deux sommets. Ces sommets sont alors *voisins* (ou *adjacents*).

### 1. Arêtes

Une arête peut :

- Avoir une orientation : on parle alors de **graphe orienté** et l'arête est un **arc**.

    ```mermaid
    flowchart LR
        a((a)) --> b((b)) & c((c)) --> d((d))
    ```

- Porter un **poids** (ou *valuation* ou *coût*) : on parle alors de **graphe pondéré**.

    ```mermaid
    flowchart LR
        a((a))-- 1 --- b((b)) -- 4 --- c((c)) -- 2 --- f((f))
        a-- 3 --- d((d)) -- 3 --- e((e)) -- 1 --- f
    ```

- Relier un sommet à lui-même : on parle alors de **boucle**.
    
    ```mermaid
    flowchart LR
        a((a)) --> b((b)) & c((c)) --> d((d))
        a --> a
    ```

Des arêtes **multiples** sont deux arêtes, ou plus, qui joignent les mêmes sommets :

```mermaid
flowchart LR
    a((a)) --> b((b)) 
    a --> b
```

!!! info "Graphe simple"

    C'est un graphe qui ne contient ni boucle ni arêtes multiples.

### 2. Sommets 

- Le **degré** d'un sommet est le nombre d'arêtes *incidentes* (les boucles non orientées sont comptabilisées 2 fois). 

    ```mermaid
    flowchart LR
        a((a)) --> b((b)) 
        c((c)) --> a
        d((d)) --> a
        a --> a
    ```

    (ici `a` est de degré 3)

    ??? info "Nœud isolé"

        Un sommet de degré 0 est isolé.

        ```mermaid
        flowchart TD
            a((a)) 
            b((b)) --- c((c)) & d((d)) --- e((e))
        ```

- Les **successeurs** d'un sommet `x` est l'ensemble des sommets `y` tels qu'il existe une arête allant de `x` vers `y`.
- Les **prédécesseurs** (ou *antécédents*) d'un sommet `x` est l'ensemble des sommets `y` tels qu'il existe une arête allant de `y` vers `x`.

### 3. Chaîne

Il s'agit d'une succession d'arêtes reliant 2 sommets (on parle de **chemin** pour un graphe orienté).

- On appelle **distance** le nombre d'arêtes de la chaîne la plus courte entre 2 sommets.
- Un **cycle** est une chaîne dont les 2 extrémités sont identiques. On parle de **circuit** pour un graphe orienté.

??? warning "Arbre"

    Les arbres font partie de la famille des graphes. Plus exactement, un arbre est un graphe *acyclique* qui :
    
    - possède un nœud particulier unique, la *racine*, et tel que
    - tous les nœuds, sauf la racine, ont un unique prédécesseur (appelé *parent*).


## II. Structure de donnée abstraite

!!! example "Exemple de graphe"

    ```mermaid
    flowchart LR
        a((a)) --- c((c)) --- d((d))
        a --- b((b)) --- f((f))
        b --- c & e((e))
        f --- e --- d
    ```

Pour caractériser ce graphe, il faut spécifier :

- Ses sommets : ensemble `{ a, b, c, d, e, f }`, ou liste `[ a, b, c, d, e, f ]`.
- Ses arêtes :

    - méthode 1 : un ensemble `{ (a,c), (a,b), (c,d), (d,e), (b,e), (b,f), (e,f) }`
    - méthode 2 : une matrice d'adjacence (on utilise l'ordre de la liste des sommets)

        $$\begin{pmatrix}
        % a  b   c   d   e   f
        0 & 1 & 1 & 0 & 0 & 0\\ %a 
        1 & 0 & 1 & 0 & 1 & 1\\ %b
        1 & 1 & 0 & 1 & 0 & 0\\ %c 
        0 & 0 & 1 & 0 & 1 & 0\\ %d 
        0 & 1 & 0 & 1 & 0 & 1\\ %e 
        0 & 1 & 0 & 0 & 1 & 0\\ %f 
        \end{pmatrix}$$


        Un `1` indique la présence d'une arête entre les sommets de la ligne et la colonne correspondantes.

        !!! info "Propriété utile"

            Pour un graphe non orienté, cette matrice est symétrique.

    - méthode 3 : les ensembles de voisinage (ou successeurs, ou antécédents) de chaque sommet.

        ```
        voisins(a) = {b, c}
        voisins(b) = {a, c, e}
        voisins(c) = {a, b, d}
        voisins(d) = {c, e}
        voisins(e) = {b, d, f}
        voisins(f) = {b, e}
        ```

