# DS01: Partie pratique

Répondre aux questions et rendre un fichier PDF (*Ctrl+p*, puis
imprimer dans un fichier)

- Penser à bien laisser apparaître toutes vos réponses avant de
  générer ce fichier.

- Vous pouvez ajouter autant de cellules de code ou de texte que
  nécessaire

## Partie 1: arbre binaire <a name="arbre"></a>

**Q8:** on souhaite stocker le sous-arbre *user1/* dans un tableau
`t1` (type `list`) de longueur **7**.

![Sous-arbre user1](img/01-arbre_user1.png)

Compléter l'instruction d'initialisation du tableau: *(utiliser des
chaînes de caractères pour les noms de fichiers/répertoire et la
valeur `None` pour les noeuds vides)*

```python
t1 = 
```

Vérifier la longueur de `t1`:

```python
len(t1)
```

La fonction suivante calcule l'indice de l'enfant droit dans le
tableau `t1` (ou retourne -1 si l'enfant n'existe pas):


```python
def enfant_droit(i):
    d = 2*i+1
    if d>=len(t1):
        return -1
    else:
        return d
```

**Q9:** Corriger cette fonction pour que les exemples suivants donnent les bons résultats:


```python
t1[enfant_droit(0)] # doit afficher 'Downloads'
```


```python
enfant_droit(enfant_droit(0)) # doit afficher -1
```

## Partie 2: documentation et tests <a name="doctest"></a>

On donne le code-source de la fonction suivante:

```python
import math

def discriminant(a,b,c):
    return b*b-4*a*c
```

**Q10:** ajouter une *docstring* qui explique son utilité.

*(tester l'affichage avec la commande ci-après)*


```python
help(discriminant)
```

**Q11:** ajouter 3 *doctest* pour tester respectivement chacun des cas suivants
- discriminant négatif
- discriminant nul
- discriminant positif

*(tester avec la commande suivante)*


```python
import doctest
doctest.run_docstring_examples(discriminant, globals())
```
