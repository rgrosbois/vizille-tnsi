---
hide:
  - navigation
---

# Arbre binaire

Il s'agit d'un arbre vide ou d'arité 2.

!!! example "Exemple : tournoi entre 7 équipes"

	![Tournoi 7 équipes](img/tournoi_7_equipes.svg)

## 1. Définition

!!! danger "Version récursive"

	$$\text{arbre binaire} = \begin{cases}\text{arbre vide ($\emptyset$)}\\
	\text{un nœud racine et 2 (sous-)arbres binaires gauche et droit}\end{cases}$$

	<center>

	![TAD Arbre](img/tad_arbre.svg)

	</center>

!!! info "Attention" 

	Un arbre binaire est parent de soit 0 (arbre vide) soit de 2 
	(sous-)arbres binaires. Mais un nœud peut avoir 0, 1 ou 2 nœuds enfants (il 
	n'existe pas de nœud vide).

## 2. Formes particulières

- Arbre **entier** : tous les nœuds parents possèdent 0 ou 2 nœuds enfants.

	![Arbre entier](img/arbre_entier.svg)

- Arbre **parfait** : toutes les feuilles sont à la même profondeur.

	![Arbre parfait](img/arbre_parfait.svg)

- Arbre **tassé** : tous les niveaux sont remplis, sauf peut-être le 
dernier.
	
	![Arbre presque complet](img/arbre_presque_complet.svg)

On définit la hauteur d'un arbre par le nombre maximal d'arêtes entre sa racine et ses feuilles.
		
??? question "Quelle est la taille *N* d'un arbre binaire parfait de hauteur *h* ?"
    $N=2^{h+1}-1$
	
??? question "Quelle est la hauteur *h* d'un arbre binaire parfait de taille *N* ?"
	$h=\log_2(N+1)-1$ (attention : la fonction `log` n'est pas étudiée en mathématique 
	au lycée)

??? question "Quelles sont les limites de taille *N* d'un arbre quelconque de hauteur *h* ?"
	$h+1 \leq N \leq 2^{h+1} - 1$

## 3. ABR: Arbre Binaire de Recherche

> La recherche d'une valeur quelconque stockée dans un arbre nécessite de 
> parcourir complètement ce dernier (dans le pire des cas) : 
> coût en o(n) pour un arbre de taille n.
>
> Nous avons vu en première année que, dans le cas d'une structure linéaire, 
> la recherche peut être beaucoup plus rapide si les éléments sont triés 
> (algorithme de recherche dichotomique).
>
> Les arbres binaires de recherche visent à reproduire cette propriété au niveau 
> des arbres.

!!! danger "Définition"
	Un arbre binaire de recherche (**ABR**) est un arbre binaire où les étiquettes 
	sont appelées **clés** et où chaque clé est :
		
	- **strictement supérieure** à toutes les clés du sous-arbre gauche;
	- **inférieure ou égale** à toutes les clés du sous-arbre droit;

Exemple :

<center>

```dot
graph G {
	node [shape=none]
	26 -- {3, 42}
	3 -- 1 [weight=2]
	3 -- 15
	15 -- 13 [weight=2]
	15 -- 19
	19 -- null [color=none]
	42 -- 29
	42 -- null2 [color=none, weight=2]
	29 -- null [color=none]
	29 -- 32
	32 -- 30
	32 -- 37 [weight=2]

	null [label="", color=none]
	null2 [label="", color=none]
}
```

</center>

??? example "Insertion d'une nouvelle clé : `25`"

	- `25` est inférieur à la racine (`26`), on l'insère dans l'enfant gauche.
	- `25` est supérieur à la racine (`3`), on l'insère dans l'enfant droit.
	- `25` est supérieur à la racine (`15`), on l'insère dans l'enfant droit.
	- `25` est supérieur à la racine (`19`), on l'insère dans l'enfant droit.

    ```dot
	graph {
		node [shape=none]

		26 -- 3 [color=red]
		26 -- 42
		3 -- 1 [weight=2]
		3 -- 15 [color=red]
		15 -- 13 
		15 -- 19 [color=red]
		19 -- null3 [color=none]
		19 -- 25
		42 -- 29
		42 -- null2 [color=none, weight=2]
		29 -- null [color=none]
		29 -- 32
		32 -- 30
		32 -- 37 

		26, 3, 15, 19 [fontcolor=red]
		25 [fontcolor=green]
		null, null2, null3 [label="", color=none, fontcolor=none]
	}
	```

??? question "Clés possibles d'un sous-arbre : à gauche de `29`"

	Les clés possibles doivent être strictement inférieurs à `29` mais aussi supérieures ou égales à `26`. Ce sont donc : `26`, `27` et `28`.

??? tip "Propriétés"

	Une telle structure permet d'optimiser la recherche de clés : le coût est en 
	o(log(n)) si l'arbre est *bien équilibré*.

	- Recherche d'extremum 

		??? success "Minimum"
			L'élément le plus petit se trouve le plus à gauche.
		??? success "Maximum" 
			L'élément le plus grand se trouve le plus à droite.

	- parcours DFS infixé

		??? success "Résultat"
	
			Le parcours en profondeur d'abord **infixé** d'un ABR entraine la visite 
			des valeurs selon l'ordre croissant.

## 4. Implémentations

!!! example "Exemples"

	Nous allons étudier différentes implémentations d'arbre pour les
	exemples suivants :

	<center>

	```mermaid
	flowchart TD
		subgraph Exemple 2
			a2((A)) --- b2((B)) & c2((C))
			c2 --- f2((F))
			c2 --- g2(( ))
			style g2 opacity:0
			linkStyle 3 opacity:0
		end
		subgraph Exemple 1
			a((A)) --- b((B)) & c((C))
			b --- d((D)) & e((E))
			c --- f((F)) & g((G)) 
		end
	```

	</center>

### a. Structure linéaire

??? tip "Rappels sur les tableaux"

	Les éléments d'un tableau sont repérés par un index entre
	crochets. En Python, on utilise essentiellement le type `list` qui 
	représente un tableau *dynamique*.

	!!! tip inline end
	
		Le type `array` de Python a un comportement de tableau classique.

	```python
	['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
	```

	Stockage en mémoire (simplifié) :

	<center>

    ```dot
	digraph G {
		label="semaine"
		nodeSemaine [shape="none", space="0.0", margin="0.01", 
		fontcolor="darkgray",
		label=<<table cellborder="1" cellspacing="0" border="0">
			<tr>
			  <td sides="b"><font point-size="9">0</font></td>
			  <td sides="b"><font point-size="9">1</font></td>
			  <td sides="b"><font point-size="9">2</font></td>
			  <td sides="b"><font point-size="9">3</font></td>
			  <td sides="b"><font point-size="9">4</font></td>
			  <td sides="b"><font point-size="9">5</font></td>
			  <td sides="b"><font point-size="9">6</font></td>
			</tr>
			<tr>
			  <td bgcolor="#fefecd">'lundi'</td>
			  <td bgcolor="#fefecd">'mardi'</td>
			  <td bgcolor="#fefecd">'mercredi'</td>
			  <td bgcolor="#fefecd">'jeudi'</td>
			  <td bgcolor="#fefecd">'vendredi'</td>
			  <td bgcolor="#fefecd">'samedi'</td>
			  <td bgcolor="#fefecd">'dimanche'</td>
			</tr>
		</table>>];
	}
	```

	</center>

??? question "Question"

	Quelle doit être la longueur d'un tableau pour stocker tous les nœuds 
	d'un arbre binaire parfait de hauteur *n* ?
    
	??? success "Réponse"
	
		$2^{n+1}-1$
	
**Principe :** on insère successivement chaque élément de l'arbre 
rencontré selon un parcourt en *largeur* (c.-à.-d niveau par niveau depuis la 
racine)

??? example "Code Python"

	On peut utiliser 2 listes `t1` et `t2` (avec `None` pour les nœuds vides) :

	```{.python linenums=1}
	t1 = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
	t2 = ['A', 'B', 'C', None, None, 'F', None]
	```

<center>

```dot
digraph {
	subgraph ex1 {
		label="Exemple 1"
		exemple1 [shape="none", space="0.0", margin="0.01", fontcolor="darkgray",
		label=<<table cellborder="1" cellspacing="0" border="0">
			<tr>
				<td sides="b"><font point-size="9">0</font></td>
				<td sides="b"><font point-size="9">1</font></td>
				<td sides="b"><font point-size="9">2</font></td>
				<td sides="b"><font point-size="9">3</font></td>
				<td sides="b"><font point-size="9">4</font></td>
				<td sides="b"><font point-size="9">5</font></td>
				<td sides="b"><font point-size="9">6</font></td>
			</tr>
			<tr>
				<td bgcolor="#fefecd">'A'</td>
				<td bgcolor="#fefecd">'B'</td>
				<td bgcolor="#fefecd">'C'</td>
				<td bgcolor="#fefecd">'D'</td>
				<td bgcolor="#fefecd">'E'</td>
				<td bgcolor="#fefecd">'F'</td>
				<td bgcolor="#fefecd">'G'</td>
			</tr>
		</table>>];
	}

	subgraph ex2 {
		label="Exemple 2"
		exemple2 [shape="none", space="0.0", margin="0.01", fontcolor="darkgray",
		label=<<table cellborder="1" cellspacing="0" border="0">
			<tr>
				<td sides="b"><font point-size="9">0</font></td>
				<td sides="b"><font point-size="9">1</font></td>
				<td sides="b"><font point-size="9">2</font></td>
				<td sides="b"><font point-size="9">3</font></td>
				<td sides="b"><font point-size="9">4</font></td>
				<td sides="b"><font point-size="9">5</font></td>
				<td sides="b"><font point-size="9">6</font></td>
			</tr>
			<tr>
				<td bgcolor="#fefecd">'A'</td>
				<td bgcolor="#fefecd">'B'</td>
				<td bgcolor="#fefecd">'C'</td>
				<td bgcolor="#fefecd"></td>
				<td bgcolor="#fefecd"></td>
				<td bgcolor="#fefecd">'F'</td>
				<td bgcolor="#fefecd"></td>
			</tr>
		</table>>];
	}
}
```

</center>

L'accès à un élément stocké dans une telle structure est très rapide (mais 
nécessite un calcul préalable de l'index). Par contre, il est très coûteux 
d'augmenter la taille de stockage ou de rajouter un nœud interne supplémentaire.

??? question "Questions"

	- Quel est l'indice de l'enfant gauche (s'il existe) d'un nœud d'indice *i* ? 
    
		??? success "Réponse"
		
			$2*i+1$

	- Quel est l'indice de l'enfant droit (s'il existe) d'un nœud d'indice *i* ? 
    
		??? success "Réponse"
		
			$2*i+2$

	- Quel est l'indice du parent (s'il existe) d'un nœud d'indice *i* ?
	
		??? success "Réponse"
		
			$\lfloor i/2\rfloor$


### b. Structure chaînée

??? tip "Rappels sur la liste chaînée"

	=== "Définition"

		!!! danger "Version récursive"

			Une liste est :

			- soit vide,
			- soit constituée d'un élément (une **tête**) et d'une (sous-)liste
			(ou **queue**).

		Chaque élément est mémorisé dans une *structure* qui contient un lien
		vers la *structure* suivante.

	=== "Liste/tuple"

		Utilisation de listes de 2 éléments ou de 2-uplets :

        ```dot
		digraph G {
				nodesep=.1;
				ranksep=.3;
				rankdir=LR;
				node [penwidth="0.5", shape=box, width=.1, height=.1];

			// LIST or ITERATABLE of atoms
		node11113800 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'lundi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node11844392 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'mardi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node14565560 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'mercredi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node10384024 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'jeudi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node11906144 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'vendredi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node15448496 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'samedi'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node10376888 [shape="box", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<table BORDER="0" CELLBORDER="0" CELLSPACING="0">
		<tr>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="br" valign="top"><font color="#444443" point-size="9">0</font></td>
		<td cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" valign="top"><font color="#444443" point-size="9">1</font></td>
		</tr>
		<tr>
		<td port="0" bgcolor="#fefecd" border="1" sides="r" align="center"><font point-size="11">'dimanche'</font></td>
		<td port="1" bgcolor="#fefecd" border="0" align="center"><font point-size="11">  </font></td>
		</tr></table>
		>];
		// LIST or ITERATABLE of atoms
		node8545088 [shape="none", space="0.0", margin="0.01", fontcolor="#444443", fontname="Helvetica", label=<<font face="Times-Italic" color="#444443" point-size="9">tuple vide</font>>];
		node11113800:1:c -> node11844392 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node11844392:1:c -> node14565560 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node14565560:1:c -> node10384024 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node10384024:1:c -> node11906144 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node11906144:1:c -> node15448496 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node15448496:1:c -> node10376888 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node10376888:1:c -> node8545088 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
		```

		??? example "Code Python"

			=== "Liste"

				```py
				semaine = ['lundi', ['mardi', ['mercredi', ['jeudi', ['vendredi', ['samedi', ['dimanche', [] ]]]]]]]
				```

			=== "Tuple"

				```py
				semaine = ('lundi', ('mardi', ('mercredi', ('jeudi', ('vendredi', ('samedi', ('dimanche', () )))))))
				```

			??? tip "Accès aux éléments"

				```py
				semaine[0] # lundi
				semaine[1][0] # mardi
				semaine[1][1][0] # mercredi
				semaine[1][1][1][0] # jeudi
				...
				```


	=== "Dictionnaire"

		On utilise des dictionnaires à 2 clés : `élément`, `prochain`.

        ```dot
		digraph G {
			nodesep=.1;
			ranksep=.3;
			rankdir=LR;
			node [penwidth="0.5", shape=box, width=.1, height=.1];
			
		// DICT
		node22392568 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'lundi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22392952 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'mardi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22394104 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'mercredi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22394008 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'jeudi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22391896 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'vendredi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22390840 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'samedi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node26075288 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'dimanche'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'prochain' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22393528 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		node22392568:1:c -> node22392952 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22392952:1:c -> node22394104 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22394104:1:c -> node22394008 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22394008:1:c -> node22391896 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22391896:1:c -> node22390840 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22390840:1:c -> node26075288 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26075288:1:c -> node22393528 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
		```

		??? example "Code Python"

			```python linenums="1"
			semaine = {
				'élément' : 'lundi',
				'prochain' : {
					'élément' : 'mardi',
					'prochain' : {
						'élément' : 'mercredi',
						'prochain' : {
							'élément' : 'jeudi',
							'prochain' : {
								'élément' : 'vendredi',
								'prochain' : {
									'élément' : 'samedi',
									'prochain' : {
										'élément' : 'dimanche',
										'prochain' : {}
									}
								}
							}
						}
					}
				}
			}		
			```

			??? tip "Accès aux éléments"

				```py
				semaine['élément'] # lundi
				semaine['prochain']['élément'] # mardi
				semaine['prochain']['prochain']['élément'] # mercredi
				...
				```

	=== "Objet"

		On peut utiliser 2 classes[^2] `Maillon` et `Liste` avec les interfaces suivantes :

		```mermaid
		classDiagram
			class Liste~T~ {
				Maillon~T~ premier_maillon
				est_vide() bool
				tete() T
				queue() Liste~T~
			}

			class Maillon~T~ {
				T element 
				Maillon~T~ prochain
			}
		```

		[^2]: Une `Liste` peut être vide mais pas un `Maillon`

        ```dot
			digraph G {
		nodesep=.1;
		ranksep=.3;
		rankdir=LR;
		node [penwidth="0.5", shape=box, width=.1, height=.1];
		
		subgraph cluster1 {style=invis penwidth=.7 pencolor="#cfe2d4"
		// Maillon OBJECT with fields
		node9475136 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'dimanche'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node12969624 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'jeudi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node12316840 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'vendredi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node11988880 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'mercredi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node13629800 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'samedi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node21433744 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'lundi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Maillon OBJECT with fields
		node9062368 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Maillon</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'mardi'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="prochain_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">prochain </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="prochain" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];

		}
		// Liste OBJECT with fields
		node15330928 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">Liste</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="premier_maillon_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">premier_maillon </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="premier_maillon" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		node15330928:premier_maillon:c -> node21433744 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node21433744:prochain:c -> node9062368 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node9062368:prochain:c -> node11988880 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node11988880:prochain:c -> node12969624 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node12969624:prochain:c -> node12316840 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node12316840:prochain:c -> node13629800 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node13629800:prochain:c -> node9475136 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
        ```

		??? example "Code Python"

			```py linenums="1"
			class Maillon():
				def __init__(self, element, prochain: Maillon):
					self.element = element
					self.prochain = prochain
					
				def __str__(self):
					return str(self.element)

			class Liste():
				def __init__(self, maillon=None):
					self.premier_maillon = maillon
					
				def est_vide(self):
					return self.premier_maillon is None
				
				def tete(self):
					return self.premier_maillon.element
				
				def queue(self):
					return Liste(self.premier_maillon.prochain)

			semaine = Liste(Maillon('lundi', Maillon('mardi', Maillon('mercredi', None))))
			```

Un arbre binaire peut être implémenté par une structure chaînée plus
*générale* où chaque élément a 2 successeurs ordonnés (un sous-arbre gauche et 
un sous-arbre droit).

=== "Dictionnaire"

	On utilise des dictionnaires à 3 clés : `élément`, `gauche` et `droite`. 
	L'arbre vide est représenté par `{}`.

	=== "Exemple 1"

        ```dot
		digraph G {
			nodesep=.1;
			ranksep=.3;
			rankdir=LR;
			node [penwidth="0.5", shape=box, width=.1, height=.1];
			
		// DICT
		node20781744 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'A'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node26053184 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'B'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node20785896 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'D'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node26053280 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node20782832 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node26053088 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'E'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node24240752 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node26052992 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node25798024 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'C'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node27321592 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'F'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node25798120 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node20784088 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node24428048 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'G'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node27321688 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node24240848 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		node20781744:1:c -> node26053184 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node20781744:2:c -> node25798024 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26053184:1:c -> node20785896 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26053184:2:c -> node26053088 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node20785896:1:c -> node26053280 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node20785896:2:c -> node20782832 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26053088:1:c -> node24240752 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26053088:2:c -> node26052992 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node25798024:1:c -> node27321592 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node25798024:2:c -> node24428048 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node27321592:1:c -> node25798120 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node27321592:2:c -> node20784088 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node24428048:1:c -> node27321688 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node24428048:2:c -> node24240848 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
		``` 

		??? example "Code Python"

			```python linenums="1"
			arbre1 = {
				'élément': 'A',
				'gauche' : {
					'élément': 'B',
					'gauche' : {
						'élément': 'D',
						'gauche' : {},
						'droite' : {}
					},
					'droite' : {
						'élément': 'E',
						'gauche' : {},
						'droite' : {}
					}
				},
				'droite' : {
					'élément': 'C',
					'gauche' : {
						'élément': 'F',
						'gauche' : {},
						'droite' : {}
					},
					'droite' : {
						'élément': 'G',
						'gauche' : {},
						'droite' : {}
					}
				}
			}
			```

	=== "Exemple 2"

        ```dot
		digraph G {
			nodesep=.1;
			ranksep=.3;
			rankdir=LR;
			node [penwidth="0.5", shape=box, width=.1, height=.1];
			
		// DICT
		node26052704 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'A'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22389400 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'B'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22389592 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node22389496 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node26052608 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'C'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22389784 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td port="0_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'élément' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="0" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'F'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="1_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'gauche' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="1" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="2_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="0" align="right"><font face="Helvetica" color="#444443" point-size="11">'droite' </font></td>
		<td cellpadding="0" border="0" valign="bottom"><font color="#444443" point-size="9">&rarr;</font></td><td port="2" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// DICT
		node22389976 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node22389880 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		// DICT
		node22389688 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" cellpadding="0" border="0"><font point-size="9"> ... </font></td></tr>
		</table>
		>];
		node26052704:1:c -> node22389400 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26052704:2:c -> node26052608 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22389400:1:c -> node22389592 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22389400:2:c -> node22389496 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26052608:1:c -> node22389784 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node26052608:2:c -> node22389688 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22389784:1:c -> node22389976 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node22389784:2:c -> node22389880 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
		```

		??? example "Code Python"

			```python linenums="1"
			arbre2 = {
				'élément': 'A',
				'gauche' : {
					'élément': 'B',
					'gauche' : {},
					'droite' : {}
				},
				'droite' : {
					'élément': 'C',
					'gauche' : {
						'élément': 'F',
						'gauche' : {},
						'droite' : {}
					},
					'droite' : {}
				}
			}
			``` 	

	??? tip "Accès aux éléments"

		```py
		arbre1['element'] # A
		arbre1['gauche']['element'] # B
		arbre1['droite']['element'] # C
		arbre1['gauche']['gauche']['element'] # D
		...
		```

=== "Objet"

	Selon les cas, on définit:
	
	- 1 seule classe: `NoeudBinaire`
	
		```mermaid
		classDiagram
			class NoeudBinaire~T~ {
				T element
				NoeudBinaire~T~ gauche
				NoeudBinaire~T~ droite
			}
		```
	
	- ou 2 classes: `NoeudBinaire` et `ArbreBinaire`

		```mermaid
		classDiagram
			class ArbreBinaire~T~ {
				NoeudBinaire~T~ racine
				estVide() bool 
				element_racine() T
				gauche() ArbreBinaire~T~ 
				droite() ArbreBinaire~T~ 
			}

			class NoeudBinaire~T~ {
				T element
				NoeudBinaire~T~ gauche
				NoeudBinaire~T~ droite
			}
		```

		(un `ArbreBinaire` peut être vide mais pas un `NoeudBinaire`)

	=== "Exemple 1"

        ```dot
		digraph G {
			nodesep=.1;
			ranksep=.3;
			rankdir=LR;
			node [penwidth="0.5", shape=box, width=.1, height=.1];
			
		// Noeud OBJECT with fields
		node14292160 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'A'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node24428280 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'D'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node25514760 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'E'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node20069704 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'B'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node24976720 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'F'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node25937760 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'C'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node20060632 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'G'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Arbre OBJECT with fields
		node23694704 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">ArbreBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="racine_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">racine </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="racine" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		node23694704:racine:c -> node14292160 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node14292160:gauche:c -> node20069704 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node14292160:droite:c -> node25937760 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node20069704:gauche:c -> node24428280 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node20069704:droite:c -> node25514760 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node25937760:gauche:c -> node24976720 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node25937760:droite:c -> node20060632 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}
		```

	=== "Exemple 2"

        ```dot
		digraph G {
		nodesep=.1;
		ranksep=.3;
		rankdir=LR;
		node [penwidth="0.5", shape=box, width=.1, height=.1];
		
		// Noeud OBJECT with fields
		node16278288 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'A'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node20792504 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'F'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node23385952 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'B'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Noeud OBJECT with fields
		node25298456 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">NoeudBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="element_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">element </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="element" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11"> 'C'</font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="gauche_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">gauche </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="gauche" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="droite_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">droite </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="droite" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		// Arbre OBJECT with fields
		node16546592 [margin="0.03", color="#444443", fontcolor="#444443", fontname="Helvetica", style=filled, fillcolor="#fefecd", label=<<table BORDER="0" CELLPADDING="0" CELLBORDER="1" CELLSPACING="0">
		<tr><td cellspacing="0" colspan="3" cellpadding="0" bgcolor="#fefecd" border="1" sides="b" align="center"><font color="#444443" FACE="Times-Italic" point-size="11">ArbreBinaire</font></td></tr>
		<tr><td colspan="3" cellpadding="1" border="0" bgcolor="#fefecd"></td></tr><tr><td port="racine_label" cellspacing="0" cellpadding="0" bgcolor="#fefecd" border="1" sides="r" align="right"><font face="Helvetica" color="#444443" point-size="11">racine </font></td>
		<td cellspacing="0" cellpadding="0" border="0"></td><td port="racine" cellspacing="0" cellpadding="1" bgcolor="#fefecd" border="0" align="left"><font color="#444443" point-size="11">    </font></td>
		</tr>
		</table>
		>];
		node16546592:racine:c -> node16278288 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node16278288:gauche:c -> node23385952 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node16278288:droite:c -> node25298456 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		node25298456:gauche:c -> node20792504 [dir=both, tailclip=false, arrowtail=dot, penwidth="0.5", color="#444443", arrowsize=.4]
		}```

	??? example "Code Python"

		```py linenums="1"
		class Noeud():
			def __init__(self, element, gauche, droite):
				self.element = element
				self.gauche = gauche
				self.droite = droite

		class Arbre():
			def __init__(self, noeud=None):
				self.racine = noeud
				
			def est_vide(self):
				return self.racine is None

			def element_racine(self):
				""" Renvoie la valeur stockée dans la racine """
				return self.racine.element
			
			def gauche(self):
				""" Renvoie le sous-arbre gauche. """
				return Arbre(self.racine.gauche)

			def droite(self):
				""" Renvoie le sous-arbre à droite. """
				return Arbre(self.racine.droite)

		exemple1 = Arbre(Noeud('A',
							   Noeud('B', 
									 Noeud('D', None, None), 
									 Noeud('E', None, None)),
							   Noeud('C', 
									 Noeud('F', None, None), 
									 Noeud('G', None, None))))

		exemple2 = Arbre(Noeud('A', 
							   Noeud('B', None, None),
							   Noeud('C', 
									 Noeud('F', None, None), 
									 None)))
		```

- **Avantage** : optimisation de l'espace de stockage. 
	
- **Inconvénient** : il faut parcourir tous les parents 
pour arriver à un nœud particulier (complexité en *o(n)* au lien de *o(1)* 
pour un tableau).