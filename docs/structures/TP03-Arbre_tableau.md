# Implémentation d'arbre dans un tableau

Rappel : Les éléments d'un tableau sont contigus dans la mémoire et repérés par un indice (entre crochets).

En Python, on utilise généralement le type `list` :

```python
tableau = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
print(len(tableau), tableau[1])
```

## I. Principe<a name="principe"></a>

Les nœuds de l’arbre sont placés successivement dans le tableau selon le **parcours en largeur** :

- Niveau par niveau (depuis la racine vers les feuilles) ;
- Chaque niveau est lu de la gauche vers la droite.

*Remarques :*

- *La racine se trouve donc à l'indice 0 et le dernier élément correspond à la feuille la plus à droite dans le dernier niveau (si
  elle existe).*
- *Certains éléments du tableau peuvent être inutilisés (`None` en Python) si l’arbre n’est pas complet.*

Écrire les instructions Python pour stocker les arbres suivants respectivement dans des tableaux t1 et t2 : 

<img alt="Arbres pour t1 et t2" src="https://snlpdo.fr/tnsi/img/01-ex_arbres.svg" width="600"/>


```python
t1 = 
t2 = 
```

Vérifier que les 2 tableaux ont bien la même longueur :


```python
len(t1) == len(t2)
```

### 1. Fonctions de déplacement <a name="deplacement"></a>

Écrire 3 fonctions qui permettent de se déplacer dans un arbre `arbre` donné (les nœuds sont identifiés par leurs indices) :

- `left` : retourne l'indice de l'enfant gauche du nœud d'indice `i` (-1 si la fonction est appelée pour une feuille).
- `right` : retourne l'indice de l'enfant droit du nœud d'indice `i` (-1 si la fonction est appelée pour une feuille).
- `up` : retourne l'indice du parent (-1 si la fonction est appelée pour la racine).

Compléter les fonctions ci-après (ajouter les paramètres nécessaires).


```python
def left():
    pass  
```

Tests significatifs sur la fonction `left()`. Appeler la fonction pour :

- afficher l'indice de l'enfant gauche de 0 pour t1 &rarr; 1
- afficher la valeur de l'enfant gauche de 0 pour t1 &rarr; 'B'
- afficher l'indice de l'enfant gauche de 5 pour t1 &rarr; -1
- afficher l'indice de l'enfant gauche de 1 pour t2 &rarr; -1

(corriger la fonction si vous n'obtenez pas les bons résultats)


```python

```


```python
def right():
    pass
```

Tests significatifs sur la fonction `right()` :


```python

```


```python
def up():
    pass
```

Tests significatifs sur la fonction `up()` :


```python

```

Vérifier le fonctionnement de ces fonctions sur les exemples suivants :

- enfant droit de l'enfant gauche de la racine de t1
- enfant droit de l'enfant gauche de la racine de t2


```python
enfant_gauche = left(0, t1)
if enfant_gauche != -1:
    enfant_droit = right(enfant_gauche, t1)
    if enfant_droit != -1:
        print(t1[enfant_droit])
    else:
        print("Pas d'enfant droit pour l'enfant gauche")
else:
    print("Pas d'enfant gauche pour la racine")
```


```python
enfant_gauche = left(0, t2)
if enfant_gauche != -1:
    enfant_droit = right(enfant_gauche, t2)
    if enfant_droit != -1:
        print(t2[enfant_droit])
    else:
        print("Pas d'enfant droit pour l'enfant gauche")
else:
    print("Pas d'enfant gauche pour la racine")
```

### 2. Docstring d'une fonction <a name="docstring"></a>

La *docstring* d'une fonction en Python est une chaîne de caractères sur plusieurs lignes spécifiée juste après la déclaration (mot clé `def`) et la première ligne d'instruction de la fonction. Consulter [cette page](https://www.python.org/dev/peps/pep-0257/) pour plus d'information.

Exemple :


```python
def ma_fonction(a,b):
    """
    Retourne la somme de a et b.
    """
    return a+b
```

Cette chaîne s'affiche, entre autre, lorsqu'on demande de l'aide sur la fonction :


```python
help(ma_fonction)
```

Recopier les 3 fonctions `right`, `left` et `up` précédentes en ajoutant des *docstring*


```python
def left():
    pass

def right():
    pass

def up():
    pass
```

Afficher l'aide pour vérifier le fonctionnement :


```python
help(left)
```


```python
help(right)
```


```python
help(up)
```

### 3. Doctest d'une fonction <a name="doctest"></a>


Ce module Python permet de vérifier le bon fonctionnement d'une fonction en spécifiant un exemple (avec `>>>`) et son résultat (sur la ligne suivante) dans la *docstring*. Consulter [cette page](https://docs.python.org/3/library/doctest.html) pour plus
d'information.

Par exemple :


```python
def ma_fonction(a,b):
    """
    Retourne la somme de a et b.
    
    >>> ma_fonction(1,4)
    6
    """
    return a+b
```

Pour effectuer le test (aucun résultat ne s'affiche si le test
s'exécute sans erreurs) :


```python
import doctest
doctest.run_docstring_examples(ma_fonction, globals())
```

Corriger le résultat du test dans la docstring pour que la cellule
précédente s'exécute sans afficher de message d'erreur.

Ajouter un ou plusieurs tests dans les 3 fonctions `right`, `left` et
`up` et vérifier le bon fonctionnement :


```python
def left():
    pass

def right():
    pass

def up():
    pass
```


```python
doctest.run_docstring_examples(left, globals())
doctest.run_docstring_examples(right, globals())
doctest.run_docstring_examples(up, globals())
```

*Note : si toutes les fonctions sont placées dans un fichier Python `monfichier.py`, il est possible d'exécuter tous les tests de toutes les fonctions en une seule instruction (dans le shell) :*

```console
$ python -m doctest -v monfichier.py
```

### 4. Exemple d'un arbre d'ascendance familiale <a name="ascendance"></a>


```python
famille = ['Alice', 'Béatrice', 'Christian', 'Delphine', 'Éric', 'Françoise', 'Gabriel', 'Hélène', 'Ivan', 'Julie', 'Kévin', 'Lucie', 'Marc', 'Noémie', 'Otto']
```

**Attention :** dans l'arbre d'ascendance, les *enfants droit et gauche* (=terminologie des arbres en informatique) correspondent
respectivement à la mère et ou père de la personne (=terminologie de la généalogie).

Utiliser les fonctions précédentes pour répondre aux questions suivantes :

- Chercher le grand-père maternel d'Alice


    ```python

    ```

- Lister les ascendants féminins de Béatrice (créer une fonction pour lister depuis n'importe quel membre)


    ```python

    ```

- Identifier si Ivan est un enfant de Christian (créer une fonction pour tester 2 membres quelconques)


    ```python

    ```

- Identifier si Delphine et Lucie ont un lien de parenté direct (créer une fonction pour 2 membres quelconques)


    ```python

    ```

- Compter le nombre de générations séparant Noémie de Christian (créer une fonction pour 2 membres quelconques)


    ```python

    ```

## II. Approche OO (Orientée Objet) <a name="OO"></a>

**Principe :** Le tableau et les fonctions de déplacement sont placées dans un même objet.

<img alt="Classe arbre"
src="https://snlpdo.fr/tnsi/img/01-arbre_POO.svg" width="200">

### Étape 1 : transposition directe de l'approche *impérative* <a name="OOe1"></a>

Écrire une classe `Arbre` qui stocke un arbre binaire dans un tableau :

- un seul attribut `tableau`
- réutiliser les 3 méthodes de déplacement

et ne pas oublier :

- le constructeur avec un tableau en tant que paramètre
- les docstrings de la classe et des méthodes


```python
class Arbre:
    pass
```


```python
a1 = Arbre(['A', 'B', 'C', 'D', 'E', 'F', 'G'])
a2 = Arbre(['A', 'B', 'C', None, None, 'F', None])
```

Quelques tests:

- afficher l'indice de l'enfant gauche de 0 pour a1 &rarr; 1
- afficher la valeur de l'enfant gauche de 0 pour a1 &rarr; 'B'
- afficher l'indice de l'enfant gauche de 5 pour a1 &rarr; -1
- afficher l'indice de l'enfant gauche de 1 pour a2 &rarr; -1

(corriger la classe si vous n'obtenez pas les bons résultats)


```python
a1.left(0), a1.tableau[a1.left(0)], a1.left(5), a2.left(1)
```

Ajouter d'autres tests utiles:


```python

```

### Étape 2 : cacher les détails de l'implémentation (i.e. l'utilisation d'un tableau) <a name="OOe2"></a>

Écrire une nouvelle version de la classe `Arbre` avec :

- *encapsulation* : 2 attributs cachés (le tableau `t` et l'index du nœud courant `pos`)
- un constructeur avec un tableau en tant que paramètre.
- Les méthodes suivantes (sans paramètre) : 

    - `is_empty` : teste si l'arbre est vide, 
    - `root` : aller à la racine
    - `has_left` : teste si le nœud courant a un enfant gauche
    - `left` : aller à l'enfant gauche du nœud courant
    - `has_right` : teste si le nœud courant a un enfant droit
    - `right` : aller à l'enfant droit du nœud courant
    - `has_up` : teste si le nœud courant a un parent
    - `up` : aller au parent du nœud courant
    - `__repr__` : indique qu'il s'agit d'un arbre et retourne les contenus de la racine et du nœud courant
    - `__str__` : retourne le contenu du nœud courant


```python
class Arbre:
    """
    Arbre binaire avec encapsulation.
    Le stockage interne utilise un tableau.
    """
    pass
```


```python
a1 = Arbre(['A', 'B', 'C', 'D', 'E', 'F', 'G'])
a2 = Arbre(['A', 'B', 'C', None, None, 'F', None])
```

Test : afficher, s'il existe, la valeur de l'enfant gauche de la racine de a1

(corriger la classe si vous n'obtenez pas les bons résultats)


```python
if not a1.is_empty():
    a1.root() # se replacer à la racine
    if a1.has_left():
        a1.left()
        print(a1)
```

Ajouter d'autres tests vous paraissant utiles :


```python

```

Reprendre l'exemple de l'arbre d'ascendance avec la classe de l'étape 2


```python

```
