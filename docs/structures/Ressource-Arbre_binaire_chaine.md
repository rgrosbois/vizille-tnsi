# Arbre binaire chaîné

## 1. Rappels/compléments sur les listes <a name="liste"></a>

Une liste est une suite d'éléments ordonnés. La position de chaque
élément est repérée par un indice:

- **explicite** (ex: tableau=l'indice est fourni entre crochets)
    ```python
    >>> semaine = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
    >>> print(semaine[1])
    mardi
    ```
    
    ![Liste tableau](https://snlpdo.fr/tnsi/img/04-Liste_tableau.png)
    
    *(les éléments sont contigus en mémoire)*
    
- **implicite** (ex: liste chaînée)

    ![Liste chaînée](img/liste_chainee.png)
    
    *(les éléments peuvent occuper des emplacements disjoints en mémoire)*

L'accès aux éléments d'un tableau est généralement plus rapide (i-ème
élément se trouve à l'emplacement i*`taille_element`) que dans une
liste chaînée, mais cette dernière permet d'optimiser l'espace de
stockage, surtout lorsque le nombre d'éléments de la liste est amené à
varier.

!!! definition "Définition commune (récursive)"
    
	Une liste est:
	
	- soit vide.
	- soit un élément (=tête) suivi d'une sous-liste.

Le type `list` de Python fournit une implémentation de liste sous la
forme de tableau *dynamique*. Nous allons construire ici une classe
`Liste` basée sur une structure chaînée.

### Exemple d'implémentation d'une liste chaînée:


```python
class Liste():
    """
    Implémentation en une seule classe qui définit les méthodes spéciales de Python
    pour la longueur (avec len), les indexes (avec []), la supression (avec del), 
    et l'itération
    """
    
    ########################
    # Méthodes indispensable    
    def __init__(self, element=None, sublist=None):
        """
        Constructeur:
        
        >>> l = Liste() # liste vide
        """
        self.head = element
        self.tail = sublist
    
    def __len__(self):
        """
        Retourne la longueur`
        (note: la liste vide a une longueur 0).
        
        >>> l = Liste()
        >>> len(l)
        0
        """
        if self.head is None:
            return 0
        elif self.tail is None:
            return 1
        else:
            return 1 + self.tail.__len__()
        
    def __getitem__(self, i):
        """
        Élément d'index i:
        
        >>> l[1]
        """
        if i==0:
            return self.head
        else:
            assert self.tail is not None, "No such element"
            return self.tail.__getitem__(i-1)
        
    def insert(self, i, element):
        """
        Insérer un élément à l'index i
        
        >>> l.insert(4, 'Bonjour')
        """
        if i==0:
            if self.head is not None:
                self.tail = Liste(self.head, self.tail)
            self.head = element
        else:
            assert self.tail is not None, "No such position to insert in this List"
            self.tail.insert(i-1, element)
            
    def __delitem__(self, i):
        """
        Supprimer l'élément d'index i 
        
        >>> del(l[4])
        """
        if i==0:
            if self.tail is not None:
                self.head = self.tail.head
                self.tail = self.tail.tail
            else:
                self.head = None
        else:
            assert self.tail is not None, "No such position to remove in this List"
            self.tail.__delitem__(i-1)
        
    ########################
    # Autres méthodes utiles
    def __setitem__(self, i, element):
        """
        Remplacer l'élément d'index i spécifié.
        
        >>> l[4] = 'Au revoir'
        """
        if i==0:
            self.head = element
        else:
            assert self.tail is not None, "No such position to replace in this List"
            self.tail.__setitem__(i-1, element)
        
    def __str__(self):
        """
        Affichage de la tête.
        
        >>> str(l)
        """
        if self.head is None:
            return ''
        else:
            return str(self.head)
        
    def __repr__(self):
        """
        Affichage du contenu.
        
        >>> repr(l)
        """
        if self.head is None:
            return ''
        
        s = str(self.head)
        
        if self.tail is not None:
            s2 = self.tail.__repr__()
            if s2!='':
                s += ' -> ' + s2
            
        return  s
    
    def find(self, element):
        """
        Recherche la position d'un élément (-1 si absent).
        
        >>> l.find('Bonjour')
        """
        if element == self.head:
            return 0
        else: # chercher dans le reste
            if self.tail is None:
                return -1
            else:
                pos = self.tail.find(element)
                if pos!=-1:
                    pos += 1
                return pos
    
    def __iter__(self):
        """
        Créer un nouvel itérateur
        
        >>> i = iter(l)
        """
        self.it_idx = -1
        return self
    
    def __next__(self):
        """
        Itérer sur les éléments.
        
        >>> next(i)
        """
        self.it_idx += 1
        if self.it_idx >= self.__len__():
            raise StopIteration
        return self.__getitem__(self.it_idx)
    
    def append(self, element):
        """
        Ajouter à la fin.
        """
        if self.head is None:
            self.head = element
        elif self.tail is None:
            self.tail = Liste(element)
        else:
            self.tail.append(element)
```

Exemples:


```python
l = Liste() 
for j in ['dimanche', 'samedi', 'vendredi', 'jeudi', 'mercredi', 'mardi', 'lundi']:
    l.insert(0,j)
l[0]
```


```python
del(l[6])
l
```

!!! note "Remarque"

	Historiquement (dans le langage *Lisp*), la tête et la sous-liste 
	s'appelaient respectivement:

	- `car` : **c**ontent of **a**ddress **r**egister (=élément en tête de 
	liste)
	- `cdr` : **c**ontent of **d**ecrement **r**egister (=sous-liste)
    
	La construction d'une liste s'effectuaient à l'aide d'une fonction
    `cons(element, sous-liste)` et la sous-liste du dernier élément
    s'appelait `nil`
	
	```python
    def cons(element, sublist):
        """
        Fonction de construction historique.
        """
        return Liste(element, sublist)

    nil = Liste()
    l = cons('L', cons('u', cons('n', cons('d', cons('i', nil)))))
    ```
 
!!! note "Remarque"

	Il existe aussi les listes
	
	- doublement chaînée (par opposition à la liste *simplement chainée* 
	ci-avant).
	- circulaire simplement chaînée.
	- circulaire doublement chaînée.

## 2. Arbre binaire dans une structure chaînée <a name="arbre"></a>

!!! definition "Définition récursive (rappel)" 

	un arbre binaire est

	- soit vide
	- soit un **n&oelig;ud** = élément (racine) + arbre gauche + arbre droit

On peut donc s'inspirer de l'implémentation de la liste pour créer des
arbres binaires:

| Arbre parfait | Arbre quelconque |
|---------------|------------------|
| ![Arbre parfait](https://snlpdo.fr/tnsi/img/04-arbre_parfait.png) | ![Arbre quelconque](https://snlpdo.fr/tnsi/img/04-arbre_quelconque.png) |


```python
class Arbre:
    """
    Implémentation d'arbre binaire dans une structure chaînée.
    
    Exemple d'utilisation:
    >>> arbre = Arbre('A',None,None)
    >>> arbre.append_left('B')
    >>> arbre.append_right('C')
    >>> arbre.left().append_left('D')
    >>> arbre.left().append_right('E')
    >>> arbre.right().append_left('F')
    >>> arbre.right().append_right('G')
    """
    def __init__(self, element=None, gauche=None, droit=None):
        """
        Arbre = élément + sous-arbre gauche + sous-arbre droit
        """
        self.root = element
        self.l = gauche
        self.r = droit
        
    def is_empty(self):
        """
        Teste si l'arbre est vide (i.e. n'a pas d'élément à sa racine)
        """
        return self.root is None
    
    def has_left(self):
        """
        Teste s'il existe un sous-arbre gauche
        """
        return self.l is not None
    
    def left(self):
        """
        Retourne le sous-arbre gauche.
        """
        return self.l

    def append_left(self, element):
        """
        Ajouter une feuille gauche (attention: n'insère pas mais écrase)
        """
        self.l = Arbre(element, None, None)
        
    def has_right(self):
        """
        Teste s'il existe un sous-arbre droit.
        """
        return self.r is not None
    
    def right(self):
        """
        Retourne le sous-arbre droit.
        """
        return self.r
    
    def append_right(self, element):
        """
        Ajouter une feuille droite (attention: n'insère pas mais écrase)
        """
        self.r = Arbre(element, None, None)
        
    def value(self):
        """
        Retourne l'élément à la racine de l'arbre
        """
        return self.root
        
    def height(self):
        """
        Retourne la hauteur de l'arbre (méthode récursive)
        """
        tg, td = 0,0
        if self.has_left():
            tg = self.left().height() + 1
        if self.has_right():
            td = self.right().height() + 1
        return max(tg, td)
    
    def size(self):
        """
        Retourne la taille de l'arbre (méthode récursive)
        """
        sg, sd = 0,0
        if self.has_left():
            sg = self.left().size()
        if self.has_right():
            sd = self.right().size()
        return sg + sd + 1

    def afficher(self, niv=0, prefix=''):
        """
        Afficher le contenu de l'arbre sous forme textuelle (méthode récursive)
        """
        if self.is_empty():
            return
        
        if niv==0:
            print(self)
        else:
            print(' '*5*(niv-1) + '|'+prefix+'-' + str(self))

        if self.has_left():
            self.left().afficher(niv+1, '(l)')
        if self.has_right():
            self.right().afficher(niv+1, '(r)')

    def __str__(self):
        """
        Retourne l'élément à la racine de l'arbre sous la forme d'une chaîne
        """
        return str(self.root)
```


```python
arbre = Arbre('A',None,None)
# Niveau 1
arbre.append_left('B')
arbre.append_right('C')
# Niveau 2
arbre.left().append_left('D')
arbre.left().append_right('E')
arbre.right().append_left('F')
arbre.right().append_right('G')

arbre.afficher()
```

    A
    |(l)-B
         |(l)-D
         |(r)-E
    |(r)-C
         |(l)-F
         |(r)-G



```python
arbre2 = Arbre('A',None,None)
# Niveau 1
arbre2.append_left('B')
arbre2.append_right('C')
# Niveau 2
arbre2.right().append_left('F')

arbre2.afficher()
```

    A
    |(l)-B
    |(r)-C
         |(l)-F


## 3. Parcours d'arbre <a id="parcours"></a>

Le parcours d'un arbre consiste à visiter tous ses n&oelig;uds.

### a. Parcours en largeur <a id="largeur"></a>

On a besoin d'une **file** pour mémoriser les n&oelig;uds pour lesquels il reste encore des enfants à parcourir


```python
class File:
    """
    (sans utiliser l'héritage)
    """
    def __init__(self):
        """
        Création d'une file vide.
        """
        self.__data = Liste(None)
        
    def empty(self):
        """
        Teste si la file n'a plus d'éléments.
        """
        return len(self.__data)==0
    
    def enfiler(self,v):
        """
        Ajouter un élément à la fin de la file.
        """
        self.__data.append(v)
        
    def defiler(self):
        """
        Extraire l'élément en tête de la file.
        """
        element = self.__data[0]
        del(self.__data[0])
        return element
    
    def __repr__(self):
        return repr(self.__data)
```


```python
f = File()
f.enfiler(1)
f.enfiler(2)
f.enfiler(3)
print(f)
a = f.defiler()
a, f
```

    1 -> 2 -> 3





    (1, 2 -> 3)



Les n&oelig;uds sont visités dans l'ordre croissant des niveaux (et de la gauche vers la droite dans chaque niveau):


```python
def parcours_largeur(a):
    '''
    Algorithme de parcours d'un arbre en largeur
    '''
    
    if a.is_empty():
        return    
    f = File()
    
    f.enfiler(a)
    while not f.empty():
        a = f.defiler()
        print(a, end=' ')
        if a.has_left():
            f.enfiler(a.left())
        if a.has_right():
            f.enfiler(a.right())
```


```python
parcours_largeur(arbre)
print()
parcours_largeur(arbre2)
```

    A B C D E F G 
    A B C F 

### b. Parcours en profondeur <a id="profondeur"></a>

On visite les sous-arbres avant de passant au prochain n&oelig;ud de
même niveau. Il existe 3 méthodes récursives:

- préfixe: pour chaque noeud, on affiche sa valeur puis on passe au
  sous-arbre gauche, puis au sous-arbre droit.
- postfixe: pour chaque noeud, on passe au sous-arbre gauche, on
  affiche sa valeur puis on passe au sous-arbre droit.
- infixe: : pour chaque noeud, on passe au sous-arbre gauche, puis au
  sous-arbre droit, puis on affiche sa valeur.


```python
def parcours_prefixe(a):
    """
    Valeur puis arbre gauche, puis arbre droit
    """
    if a.is_empty():
        return
    print(a, end=' ')
    
    if a.has_left():
        parcours_prefixe(a.left())
    if a.has_right():
        parcours_prefixe(a.right())
```


```python
parcours_prefixe(arbre)
print()
parcours_prefixe(arbre2)
```


```python
def parcours_postfixe(a):
    """
    arbre gauche, puis arbre droit, puis valeur
    """
    if a.is_empty(): 
        return    
    if a.has_left():
        parcours_postfixe(a.left())
    if a.has_right():
        parcours_postfixe(a.right())
    print(a, end=' ')
```


```python
parcours_postfixe(arbre)
print()
parcours_postfixe(arbre2)
```


```python
def parcours_infixe(a):
    """
    arbre gauche, valeur, puis arbre droit
    """
    if a.is_empty(): 
        return    
    if a.has_left():
        parcours_infixe(a.left())
    print(a, end=' ')
    if a.has_right():
        parcours_infixe(a.right())
```

```python
parcours_infixe(arbre)
print()
parcours_infixe(arbre2)
```
