---
hide:
  - navigation
---

# Arborescence

!!! example "Exemples"

	=== "Système de fichiers"

		Le contenu de l'archive du jeu d'[escape
		game](../archi/00-Escape_game.md){target="_blank"} peut s'obtenir à
		l'aide de la commande `tree`:

		```{.console .rgconsole .rgsmall}
		[alice@weblinux:~]$ tree escape_game
		escape_game
		├── Langages
		│   ├── C++
		│   │   ├── Binaire
		│   │   └── Source
		│   ├── Python
		│   │   ├── enigme1
		│   │   ├── enigme1.txt
		│   │   └── README.txt
		│   └── Web
		│       ├── CSS
		│       │   └── style.txt
		│       ├── HTML
		│       └── JS
		├── Materiel
		│   ├── Disques
		│   │   └── HD
		│   ├── Memoire
		│   │   └── serrure
		│   └── Processeur
		│       ├── x64
		│       │   └── intel.txt
		│       └── x86
		├── Personnes
		│   ├── Apple
		│   ├── Djikstra
		│   ├── Hopper
		│   ├── Microsoft
		│   ├── README.txt
		│   ├── Ritchie
		│   ├── Torvalds
		│   ├── Turing
		│   └── Von Neumann
		└── README.txt

		18 directories, 15 files
		```

		??? warning "Séparateur"

			*Linux* utilise le caractère `/` pour séparer
			les noms de répertoires alors que *Windows* utilise `\`. 
			
			Ce séparateur est optionnel en fin de chemin (on peut écrire 
			`/home/nsi/` ou `/home/nsi`).

	=== "Processus"

		La commande `pstree` montre les processus en cours d'exécution avec leurs
		liens de parenté :

		```{.console .rgconsole .rgsmall}
		[nsi@localhost:~]$ pstree
		systemd-+-ModemManager---3*[{ModemManager}]
			|-NetworkManager---2*[{NetworkManager}]
			|-abrt-dbus---2*[{abrt-dbus}]
			|-3*[abrt-dump-journ]
			|-abrtd---2*[{abrtd}]
			|-accounts-daemon---3*[{accounts-daemon}]
			|-alsactl
			|-auditd-+-sedispatch
			|        `-2*[{auditd}]
			|-avahi-daemon---avahi-daemon
			|-bluetoothd
			|-boltd---3*[{boltd}]
			|-chronyd
			|-colord---3*[{colord}]
			|-cupsd
			|-dbus-broker-lau---dbus-broker
			|-dnsmasq---dnsmasq
			|-firewalld---{firewalld}
			...
		```

	=== "DOM"

		```plaintext title="index.html"
		html
		|- head
		|    |- meta
		|    |- title
		|    |- link
		|    |- script
		|- body
			|- h1
			|- p
			|- ul
			|   |- li
			|   |- li
			|- img
		```

	=== "Analyse syntaxique"

		=== "Arbre"

			```mermaid
			flowchart TB 
				corps((Corps)) --- aff[Affectation] & for["Boucle\nfor"] & print["Fonction\nprint"]
				aff --- 5 & y
				for --- i & it["Itérable\nrange"] & corps2((Corps))
				it --- 4
				corps2 --- aff2[Affectation] --- y2[y] & x[*]
				x --- y3[y] & 2
				print --- y4[y]
			```

		=== "Code"
		
			```python
			y = 5
			for i in range(4):
				y = y * 2
			print(y)
			```

## 1. Arbre enraciné

Contrairement aux listes et dictionnaires qui mémorisent des 
données séquentielles, l'arbre est une structure de données 
**hiérarchique** : il est constitué de **nœuds** reliés par des 
**arêtes** (ou *branches*).

- Il est non vide et il possède un nœud particulier (une *origine*) qu'on 
appelle la **racine**.
  
    ![Vocabulaire arbres](img/01-arbre_vocab.svg)

- Hiérarchie : un nœud **parent**[^1] est relié à 0, 1 ou 
plusieurs nœuds **enfants** [^2].

	[^1]: celui qui est le plus proche de la racine.
	[^2]: ceux qui s'éloignent de la racine.

    ![Parenté dans un arbre](img/01-arbre_parente.svg)

	- La racine est le seul nœud qui n'a pas de parent.

	- Un nœud qui n'a pas d'enfant est une **feuille** (ou nœud externe).

	- Les nœuds qui ne sont ni une racine, ni une feuille sont dits *internes*.

- Un nœud peut être étiqueté par une information (son nom, une donnée&hellip;)


!!! warning "Attention"

	En informatique, l'arbre *pousse* généralement vers le bas.

??? info "Arbre non enraciné (Hors programme NSI)"

	Cas où aucun nœud n'est particularisé (remarque : ce type d'arbre peut donc
	être vide)

    ```dot
	graph G {
		size="3"
		layout=neato
		node [shape=circle, fontsize=28, penwidth=3]
		edge [penwidth=3]
		a -- b
		c -- b
		b -- d
		d -- {e, f}
		e -- {g, h}
		f -- i
		g -- j
	}
	```

## 2. Propriétés

**Profondeur** (ou niveau) d'un nœud 
: Nombre d'arêtes entre ce nœud et la racine

**Hauteur** de l'arbre 
: Profondeur maximale de ses nœuds

??? danger "Définitions alternatives"

	Selon l'utilisation, on utilise l'une des 2 conventions suivantes pour la profondeur d'un nœud et la hauteur d'un arbre :

	1. On compte le nombre de nœuds depuis la racine.

		Ce qui est équivalent à compter les niveaux.

		> il s'agit de la convention utilisée ci-précédemment.

	2. On compte le nombre d'arêtes entre la racine et les nœuds.

		Définition récursive: 
		
		- si ses sous-arbres gauche et droit sont vides, sa hauteur est 0 ;
		- si l’un des deux au moins est non vide, alors sa hauteur est égale à 1 + M , où M est la plus grande des hauteurs de ses sous-arbres (gauche et droit) non vides.

		> cette convention ne permet pas de distinguer l'arbre vide et l'arbre avec un seul nœud.


**Taille** d'un arbre
: Nombre total de nœuds

**Arité** d'un arbre
: Nombre maximum d'enfants par parent

??? question "Exercice"
	Dans l'arbre ayant `escape_game` comme racine :

	- Quelles sont les profondeurs des répertoires `Disques/` et `escape_game/` ?

    	- `Disque` = {{rg_qnum(2, 0) }}
		- `escape_game` = {{ rg_qnum(0,0) }}

	- Que valent la hauteur et la taille de l'arbre de l'escape game ?

		- hauteur = {{ rg_qnum(4,0) }}
		- taille = {{ rg_qnum(34,0) }}

## 3. Chemins

Alors qu'un parcours chercher à visiter tous les nœuds de l'arbre (Cf. chapitre 
suivant), un **chemin** s'effectue d'un nœud de départ vers un nœud d'arrivée. 

La notion de chemin est essentielle pour une arborescence de 
fichiers.

??? tip "Rappels de commandes du shell"

	Commandes du shell associées à l'arborescence des fichiers:

	- `pwd`: affiche le répertoire dans lequel se trouve l'utilisateur

	- `cd` : permet de se déplacer dans le système de fichiers

		- sans paramètre: la commande ramène l'utilisateur à son 
		répertoire personnel (ex: `/home/nsi/`)
	
			```{.console .rgconsole}
			[nsi@localhost:Documents]$ cd
			[nsi@localhost:~]$ pwd
			/home/nsi
			```
	
		- avec un paramètre (=le chemin vers un répertoire): pour se déplacer
		vers ce répertoire.

- Chemin **absolu** : juxtaposition de tous les noms depuis la racine (`/`) 
jusqu'au nœud cible (séparateur : `/`).

	```{.console .rgconsole}
	[nsi@localhost:Documents]$ cd /home/nsi/Public
	[nsi@localhost:Public]$ pwd
	/home/nsi/Public
	```

	![Chemin absolu](img/01-chemin_absolu.svg)

- Chemin **relatif** : juxtaposition de tous les noms depuis le répertoire 
courant (`./`, optionnel) jusqu'au nœud cible (séparateur : `/`).

	??? tip "Conventions de déplacement dans l'arborescence"

		- `../` permet d'accéder au répertoire parent
		- `./` laisse l'utilisateur dans le même répertoire (astuce utilisée 
		pour lancer un exécutable)

			![cd point](img/01-cd_point.svg)


	```{.console .rgconsole}
	[nsi@localhost:Disques]$ cd ../../Personnes/Apple/
	[nsi@localhost:Apple]$ 
	```

	![Chemin relatif](img/01-chemin_relatif.svg)

  
??? question "Exercice"
	Exemple avec l'arborescence des fichiers dans le jeu d'escape game.

	(*on suppose que la racine de l'arborescence du jeu d'escape game se trouve directement dans la zone personnelle `/home/nsi`*)



	- Donner le chemin absolu du fichier `enigme1.txt`: {{ rg_qsa(["/home/nsi/escape_game/Language/Python/enigme1.txt"]) }} 

	- Donner le chemin relatif le plus court du fichier `HD` depuis le répertoire `Binaire/`: 
	{{ rg_qsa(["../../../Materiel/Disques/HD"]) }}

