# Exercices sur les arbres

La classe ci-après fournit une implémentation d'arbre dans une
structure chaînée:


```python
class Arbre:
    """
    Implémentation d'arbre binaire dans une structure chaînée.
    
    Exemple d'utilisation:
    >>> arbre = Arbre('A',None,None)
    >>> arbre.append_left('B')
    >>> arbre.append_right('C')
    >>> arbre.left().append_left('D')
    >>> arbre.left().append_right('E')
    >>> arbre.right().append_left('F')
    >>> arbre.right().append_right('G')
    """
    def __init__(self, element=None, gauche=None, droit=None):
        """
        Arbre = élément + sous-arbre gauche + sous-arbre droit
        """
        self.root = element
        self.l = gauche
        self.r = droit
        
    def is_empty(self):
        """
        Teste si l'arbre est vide (i.e. n'a pas d'élément à sa racine)
        """
        return self.root is None
    
    def has_left(self):
        """
        Teste s'il existe un sous-arbre gauche
        """
        return self.l is not None
    
    def left(self):
        """
        Retourne le sous-arbre gauche.
        """
        return self.l

    def append_left(self, element):
        """
        Ajouter une feuille gauche (attention: n'insère pas mais écrase)
        """
        self.l = Arbre(element, None, None)
        
    def has_right(self):
        """
        Teste s'il existe un sous-arbre droit.
        """
        return self.r is not None
    
    def right(self):
        """
        Retourne le sous-arbre droit.
        """
        return self.r
    
    def append_right(self, element):
        """
        Ajouter une feuille droite (attention: n'insère pas mais écrase)
        """
        self.r = Arbre(element, None, None)
        
    def set_root(self, element):
        """
        Modifier la valeur de la racine
        """
        self.root = element
        
    def value(self):
        """
        Retourne l'élément à la racine de l'arbre
        """
        return self.root
```

Arbre à utiliser pour ces exercices:


```python
def fill(a, element):
    if a.is_empty():
        a.set_root(element)
    elif element <= a.value():
        if a.has_left():
            fill(a.left(), element)
        else:
            a.append_left(element)
    else:
        if a.has_right():
            fill(a.right(), element)
        else:
            a.append_right(element)

arbre = Arbre()
for e in [17, 22, 27, 13, 21, 28, 29, 5, 20, 9, 24, 2, 3, 15, 26, 12, 25, 16, 11, 19, 8, 10, 23, 4, 18, 14, 6, 7, 0, 1]:
    fill(arbre, e)
```

## Exercice 1:

Écrire 2 fonctions:

- `taille()`: retourne le nombre de n&oelig;uds dans l'arbre.
- `hauteur()`: retourne sa hauteur.

*conseil: utiliser une approche diviser pour régner*


```python

```

## Exercice 2:

Afficher la valeur du n&oelig;ud:

- le plus à gauche de l'arbre
- le plus à droite de l'arbre


```python

```

## Exercice 3:

Afficher toutes les valeurs selon le parcours infixé.


```python

```

## Exercice 4:

Afficher toutes les valeurs selon le parcours en largeur


```python

```
