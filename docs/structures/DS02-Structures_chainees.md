# Structures chaînées

## I. Listes chaînées <a id="listes"></a>

Une implémentation (partielle) de liste chaînée vous est donnée:


```python
from ressource02 import Liste
```

Affichage de l'aide sur la classe `Liste` à utiliser:


```python
help(Liste)
```

### Exercice 1 <a id="ex1"></a>

Deux instances de **Liste** `l1` et `l2` ont déjà été créées:


```python
from ressource02 import l1, l2
```

Affichage de leur contenu:


```python
l1
```


```python
l2
```

Créer une nouvelle **Liste** `l3` contenant tous les entiers
divisibles par 3 compris entre 50 et 100 (peu importe leur ordre).


```python
l3 = 
```


```python
l3
```

### Exercice 2 <a id="ex2"></a>

- Écrire une fonction non récursive `longueur_it` qui **retourne** la
  longueur d'une Liste:


	```python
	def longueur_it(liste):
		"""
		Fonction non récursive qui retourne la longueur d'une Liste
		"""
		pass
	```

Tests à effectuer:


```python
longueur_it(l1)
```


```python
longueur_it(l2)
```


```python
longueur_it(l3)
```

- Écrire une fonction récursive `longueur_rec` qui **retourne** la
  longueur d'une Liste:


	```python
	def longueur_rec(liste):
		"""
		Fonction récursive qui retourne la longueur d'une liste
		"""
		pass
	```

Tests à effectuer:


```python
longueur_rec(l1)
```


```python
longueur_rec(l2)
```


```python
longueur_rec(l3)
```

### Exercice 3 <a id="ex3"></a>

Écrire une fonction qui retourne la valeur maximale d'une Liste (non
vide et constituée de valeurs numériques)


```python
def maximum(liste):
    pass
```

Test à effectuer:


```python
maximum(l3)
```

## II. Arbres chaînés <a id="arbres"></a>

Une implémentation (partielle) d'arbre chaînée vous est donnée:


```python
from ressource02 import Arbre
```


```python
help(Arbre)
```

2 **Arbres** `arbre1` et `arbre2` ont déjà été créés:


```python
from ressource02 import arbre1, arbre2
```

### Exercice 4 <a id="ex4"></a>

Créer un **Arbre** `arbre3` avec le contenu suivant:

![Arbre binaire](https://snlpdo.fr/tnsi/img/02-arbre_binaire_DS02.png)


```python
arbre3 = 
```

### Exercice 5 <a id="ex5"></a>

Écrire une fonction `taille` qui calcule la taille d'un **Arbre**


```python
def taille(a):
    pass
```

Tests à effectuer:


```python
taille(arbre1)
```


```python
taille(arbre2)
```


```python
taille(arbre3)
```

### Exercice 6 <a id="ex6"></a>

Afficher le contenu de l'arbre selon un parcours préfixé


```python
def parcours(a):
    pass
```

Tests à effectuer:


```python
parcours(arbre1)
```


```python
parcours(arbre3)
```
