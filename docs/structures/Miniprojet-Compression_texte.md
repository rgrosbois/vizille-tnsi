# Compression de texte

L'idée est de remplacer les caractères du texte par des **codes de
longueurs variables** (on parle donc de *codage*) en réservant :

- les codes courts pour les caractères couramment utilisés (ex: `'e'`,
  `'a'`, `' '`....).
- les codes plus longs pour les caractères peu utilisés (ex: `'z'`,
  `'w'`...).

## 1. Occurrences <a id="freq"></a>

Pour mettre en place ce codage, il est essentiel de connaître le nombre d'occurrences de chaque caractère. Nous allons travailler, dans un premier temps, sur le texte suivant:


```python
texte = """
   Un jour, Cunégonde, en se promenant
   auprès du château, dans le petit bois qu'on appelait
   parc, vit entre des broussailles le docteur Pangloss qui donnait
   une leçon de physique expérimentale à la
   femme de chambre de sa mère, petite brune très jolie
   et très docile. Comme Mlle Cunégonde avait beaucoup
   de dispositions pour les sciences, elle observa, sans souffler,
   les expériences réitérées dont elle
   fut témoin; elle vit clairement la raison suffisante du
   docteur, les effets et les causes, et s'en retourna tout
   agitée, toute pensive, toute remplie du désir
   d'être savante, songeant qu'elle pourrait bien être la
   raison suffisante du jeune Candide, qui pouvait aussi être
   la sienne.
   """
```

**Questionnement:** 

- Quel est le nombre de caractères `nb_tot` constituant ce texte ?

	```python

	```

- Quel type de codage est actuellement utilisé pour ces caractères ?


	```python

	```

- Quel nombre d'octets faut-il pour stocker ce texte ?


	```python

	```

*Pour améliorer les performances de compression, nous n'allons créer
des codes que pour les caractères qui apparaissent dans le texte.*

- Est-il préférable de stocker la liste des caractères dans un
  *tableau* (type `list` en Python) ou une structure chaînée ?


	```python

	```

	*(recopier ou importer la classe `Liste` du document ressource et
	identifier les méthodes utiles)*


	```python

	```

- Créer et remplir `liste_carac`, une *Liste* contenant une seul fois
  chaque caractère du texte.


```python

```

- Créer et compléter une nouvelle *Liste* `liste_occurrences` où
  chaque élément est le nombre d'occurrences du caractère
  correspondant dans `liste_carac`. *(vérifier que la somme de tous
  ces nombres vaut `nb_tot`)*


	```python

	```


	```python
	# Calcul de la somme
	somme = 0
	# À compléter

	assert somme==nb_tot, "La liste d'occurrences n'est pas correctement remplie"
	```

- Créer un tableau `occurrences` (type `list` de Python) où chaque
  élément est un p-uplet au format `(c, nb(c))` (avec `nb(c)` le
  nombre d'occurrence du caractère `c`)


	```python

	```

- (Optionnel) Trier cette liste (avec la méthode de votre choix) selon
  les fréquences d'occurrence décroissantes. Quels sont les 5
  caractères qui apparaissent le plus souvent ?


	```python

	```

## 2. Codage des caractères <a id="codage"></a>

Le code de chaque caractère correspond à son chemin absolu dans l'arbre ci-après. Par convention, on lit un chemin en ajoutant :

- `0` si on se déplace vers l'enfant gauche.
- `1` si on se déplace vers l'enfant droit.

![Arbre de Huffman ](https://snlpdo.fr/tnsi/img/04-ex_arbre_huffman.png)

Exemples:

- Le caractère `'e'` est remplacé par le code `100` (=3 bits).
- Le caractère `'a'` est remplacé par le code `11111` (=5 bits)

**Questionnement:**

- Comment appelle-t-on les n&oelig;uds qui contiennent les caractères
  dans cet arbre ?


	```python

	```

- Quel code remplace la portion de texte `Un jour` en utilisant cet arbre ?


	```python

	```

- Combien de bits fait ce code ? Combien d'octets faut-il pour le stocker ?


	```python

	```

### a. Construction de l'arbre <a id="arbre"></a>

Pour compresser le plus possible, il faut que les caractères les plus
(resp. moins) utilisés soient situés sur les feuilles les plus proches
(resp. éloignées) de la racine.

L'arbre de Huffman peut s'obtenir en appliquant l'algorithme suivant:

!!! tip "Algorithme"
    
	1. Pour chaque caractère `c`, on crée un nouvel arbre (d'un seul 
	n&oelig;ud) dont la racine vaut `(c, nb(c))`.

	2. On fusionne les 2 arbres d'occurrences minimales `nb(c1)` et `nb(c2)` 
	en un seul nouvel arbre dont:
	
		- les enfants sont les 2 arbres précédents,
		- la racine a pour nouvelle occurrence `nb(c1)+nb(c2)` (le nouveau 
		caractère, pourra s'appeler `c1+c2`).

	3. On répète l'opération 2 jusqu'à n'avoir plus qu'un seul arbre.

	*(Recopier ou importer au préalable la classe `Arbre` fournie dans le 
	document ressources et identifier les méthodes utiles pour cette partie)*


	```python

	```

**Questionnement :**

- **Étape 1** : remplir une Liste `liste_arbres` avec tous les arbres
  à 1 n&oelig;ud (1 par caractère).


	```python

	```

- **Étape 2a** : Écrire la fonction `extract_min` qui extrait
  l'élément minimal d'une telle `Liste` (attention: les éléments de
  cette liste sont des Arbres et le tri se fait sur la 2ème valeur du
  tuple de leur racine)


	```python
	def extract_min(liste):
		pass
	```


	```python
	# Vérification
	a = extract_min(liste_arbres)
	assert a.root[0] == 'U', "Il ne s'agit pas du bon minimum"
	assert len(liste_arbres)==38, "Il faut supprimer le minimum de la liste des arbres"
	```

- **Étapes 2b et 3** : fusionner tous les arbres de cette liste selon
  l'algorithme de Huffman


	```python
	# Algorithme de Huffman
	while len(liste_arbres)>1: # tant qu'il reste plus d'un noeud
		# Extraire le 1er arbre de fréquence minimale
		
		# Extraire le 2ème arbre de fréquence minimale
    
		# Fusionner ces 2 arbres pour créer en un nouveau
    
		# Ajouter ce nouvel arbre dans la liste
    
	```


	```python
	# Dernier arbre restant
	arbre_huffman = liste_arbres[0]
	```

	Que valent la hauteur et la taille de `arbre_huffman` ?


	```python
	
	```

### b. Table de correspondance <a id="lut"></a>

Pour accélérer la phase de codage, on va remplir un *dictionnaire*
Python `lut` où les **clés** sont les caractères du texte et les
**valeurs** sont les codes de Huffman correspondants.

Exemple: `lut['e'] = '100'`

**Questionnement :**

- Quel type de parcours d'arbre permet d'identifier le chemin de
  chaque caractère ?


	```python

	```

- Mettre en &oelig;uvre ce parcours pour remplir la table de
  correspondance `lut`:


	```python

	```

### c. Compression du texte <a id="compression"></a>

- Utiliser la table de correspondance précédente pour générer le code
  correspondant au texte fourni au début de ce document


```python

```

- Quel taux de compression obtient-t-on ? (on suppose que le code obtenu est sauvegardé en binaire)


```python

```

## 3. Décodage <a id="decodage"></a>

!!! tip "Algorithme"

	1. On se place à la racine de l'arbre de Huffman 
	2. On lit un bit *b* du texte compressé &rarr; **arrêt** si plus de bit 
	disponible.
	3. On descend dans l'arbre:
	
		- à gauche si *b=0*
		- à droite si *b=1*
		
	3. Si le n&oelig;ud atteint:
		- est une feuille: on affiche le caractère décodé et on reprend à 
		l'**étape 1**.
		- est intermédiaire: on reprend à l'**étape 2**.


```python

```

## 4. Projet Python <a id="projet"></a>

(pour 1 binôme)

**Élève 1 :**

Mettre au point un codeur qui:

- créé l'arbre de Huffman d'un texte quelconque.
- génère un fichier avec:

    - le code obtenu pour ce texte (au format binaire)
    - Le contenu de l'arbre (indispensable pour le décodeur)
- affiche le taux de compression final.
        
**Éleve 2 :**

Mettre au point un décodeur qui:

- récupère l'arbre utilisé par le codeur.
- décompresse le texte.
- vérifie qu'il n'y a pas eu de perte.


```python

```
