import matplotlib.pyplot as plt
import math

abscisses = [n/10 for n in range(1,140)]

ordonnees1 = [x**2 for x in abscisses]
plt.plot(abscisses,ordonnees1, label='n²')

ordonnees3 = [x for x in abscisses]
plt.plot(abscisses,ordonnees3, label='n')

ordonnees2 = [x*math.log(x) for x in abscisses]
plt.plot(abscisses,ordonnees2, label='nlog(n)')

ordonnees4 = [math.log2(x) for x in abscisses]
plt.plot(abscisses,ordonnees4, label='log(n)')

plt.ylim(0,11)
plt.xlim(0,15)
plt.legend()
plt.grid()
plt.show()