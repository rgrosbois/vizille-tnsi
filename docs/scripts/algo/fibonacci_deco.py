def memoisation(mafonction):
    """Pour décorer n'importe quelle fonction retournant 
    avec un paramètre entier en entrée et renvoyant une valeur"""
    memoire = {}
    def wrap(n):
        if n not in memoire:
            memoire[n] = mafonction(n)
        return memoire[n]
    return wrap

@memoisation
def fibo_rec(n: int) -> int :
    if n==0 or n==1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)