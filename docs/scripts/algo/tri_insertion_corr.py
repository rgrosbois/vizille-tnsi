def tri_insertion(liste: list[int]) -> int:
    """
    Trie la liste par par insertion et renvoie le nombre de comparaisons.
    """
    compteur = 0
    
    # Parcourt tous les éléments depuis le deuxième
    for j in range(1,len(liste)):
        e = liste[j]
        i = j
        
        # Remonter parmi les précédents pour trouver l'index d'insertion
        while i > 0 and liste[j] &lt; liste[i-1]:
            i = i-1
            compteur += 1
            
        if i>0: 
            compteur += 1
        
        if i != j: # faire de la place en décalant à droite
            for k in range(j,i,-1):
                liste[k] = liste[k-1]
            # placer l'élément courant
            liste[i] = e
            
    return compteur