def fibo_it(valeur:int) -> int:
    val = 0
    if valeur<2:
        return valeur
    
    n_1,n_2 = 1,0 # n-1 et n-2
    n = 2
    while n<=valeur:
        val = n_1 + n_2
        n_2 = n_1
        n_1 = val
        n += 1
    return val