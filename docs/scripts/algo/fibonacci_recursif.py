def fibo_rec(n: int) -> int :
    if n==0 or n==1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)