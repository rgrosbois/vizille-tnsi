def exp_it(x: float, n: int) -> float:
    resultat = 1
    for i in range(n):
        resultat = resultat * x
    return resultat