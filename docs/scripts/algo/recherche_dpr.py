def recherche_dpr(element, liste):
    """
    Attention: ici la division effectue des copies de liste et ne gère donc la mémoire
    de manière optimale.
    """
    longueur = len(liste)
    
    if longueur==0: # régner (cas élémentaire)
        return False
    elif longueur==1: # régner (cas élémentaire)
        if element==liste[0]:
            return True
        else:
            return False
    else: # diviser puis combiner
        i_milieu = longueur//2
        # Dans partie gauche ou dans partie droite
        return recherche_dpr(element,liste[:i_milieu]) or recherche_dpr(element,liste[i_milieu:])