def min_max(liste:list[int]) -> tuple:
    compteur = 0
    
    mini = liste[0]
    for i in range(1, len(liste)):
        compteur += 1
        if liste[i]&lt;mini:
            mini = liste[i]
    maxi = liste[0]
    for i in range(1, len(liste)):
        compteur += 1
        if liste[i]>maxi:
            maxi = liste[i]
    
    print(f"{compteur} comparaisons")
    return mini, maxi