def recherche_min_max(liste:list[int]) -> tuple:
    """
    Attention: ici la division réalise des copies de liste, ce qui 
    n'est pas optimal d'un point de vue mémoire.
    """
    if len(liste)==1:
        return liste[0], liste[0]
    elif len(liste)==2:
        if liste[1]>liste[0]:
            return liste[0], liste[1]
        else:
            return liste[1], liste[0]
    else:
        i_milieu = len(liste)//2
        
        min1,max1 = recherche_min_max([liste[i] for i in range(0, i_milieu)])
        min2,max2 = recherche_min_max([liste[i] for i in range(i_milieu, len(liste))])
        
        if min2 &lt; min1:
            min1 = min2
        if max2>max1:
            max1 = max2
            
        return min1, max1