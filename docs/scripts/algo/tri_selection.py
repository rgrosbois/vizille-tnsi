def tri_selection(liste: list[int]) -> int:
    """
    Trie la liste par sélection et renvoie le nombre de comparaisons.
    """
    # Parcourir tous les éléments jusqu'à l'avant dernier de la liste
    for i in range(len(liste)-1):
        # Rechercher la position du minimum parmi les indices restants.
        j_min = i
        for j in range(i+1,len(liste)):
            if liste[j] < liste[j_min]:
                j_min = j
        # Permuter les positions i et j_min
        liste[i],liste[j_min] = liste[j_min],liste[i]