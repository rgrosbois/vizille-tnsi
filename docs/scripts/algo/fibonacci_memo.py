def fibo_memo(n : int, memoire = None) -> int:
    if memoire is None:
        memoire = {}
    
    if n==0 or n==1:
        return n
    elif memoire.get(n) is not None:
        return memoire[n]
    else:
        memoire[n] = fibo_memo(n-1, memoire) + fibo_memo(n-2, memoire)
        return memoire[n]