def exp_rapide(x: float,n: int) -> float:
    if n==0:
        return 1
    elif n%2==0: # n est pair
        tmp = exp_rapide(x,n//2)
        return tmp*tmp
    else: # n est impair
        tmp = exp_rapide(x,(n-1)//2)
        return x*tmp*tmp