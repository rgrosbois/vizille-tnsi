objets = [ ('A', 15, 500), ('B', 24, 400), ('C', 9, 350),  ('D', 25, 750), ('E', 5, 400),  ('F', 12, 800), ('G', 2, 1400), ('H', 18, 550) ]

meilleure_combinaison = 0
valeur_max = 0
poids_valeur_max = 0

for combinaison in range(2**len(objets)):
    poids = 0
    valeur = 0
    # La décomposition binaire de combinaison
    # indique la présence ou non de chaque objet
    for i in range(len(objets)):
        if (combinaison>>i) & 1 == 1:
            poids += objets[i][1]
            valeur += objets[i][2]

    if poids <= 40 and valeur > valeur_max: # considérer cette combinaison
        meilleure_combinaison = combinaison
        valeur_max = valeur
        poids_valeur_max = poids
        
# Détailler cette solution optimale
print(f"Valeur: {valeur_max}, poids: {poids_valeur_max} kg", end=': ')
for i in range(len(objets)):
    if (meilleure_combinaison>>i) & 1 == 1:
        print(objets[i][0], end=' ')
print()