def exp_rec(x: float, n: int) -> float:
    if n==0:
        return 1
    else:
        return x * exp_rec(x, n-1)