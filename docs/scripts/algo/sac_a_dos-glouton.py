rapport_valeur_poids = sorted([(o[0], o[1], o[2], o[2]/o[1]) for o in objets], key=lambda x:x[3], reverse=True)
rapport_valeur_poids

def selectionner(obj, poids_max):
    objets_tries = sorted(obj, key=lambda o:o[2]/o[1], reverse=True)

    selection = []
    poids = 0
    valeur = 0
    for o in objets_tries:
        if poids + o[1] <= poids_max: # ajouter l'objet
            selection.append(o[0])
            poids  += o[1]
            valeur += o[2]
    return (valeur, selection)

selectionner(objets, 40)