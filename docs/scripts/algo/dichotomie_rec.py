def dichotomique_recursive(element: int, liste: list[int], i_debut:int =0, i_fin:int = None) -> int:
    """
    Recherche si element est présent dans liste entre l'index i_debut (inclu) et i_fin (non inclu).
    """
    
    # Préconditions
    if i_fin is None: # initialisation pour premier appel
        i_fin = len(liste)        
    if i_debut==i_fin: # liste vide
        return False

    # Algorithme
    i_milieu = (i_debut+i_fin)//2
    if element == liste[i_milieu]: # élément trouvé
        return True
    elif i_fin == i_debut+1: # L'élément n'est pas présent
        return False
    elif element<liste[i_milieu]: # l'élément est plus petit -> rechercher à gauche
        return dichotomique_recursive(element, liste, i_debut, i_milieu)
    else: # l'élément est plus grand -> rechercher à droite
        return dichotomique_recursive(element, liste, i_milieu, i_fin)