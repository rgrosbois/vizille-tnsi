def itineraire(depart: str, arrivee: str, graphe: dict[dict]) -> list[tuple]:
    """
    Calcule et renvoie l'itinéraire optimal entre 2 villes.
    """

    # Générer l'arbre de dijkstra de la ville de départ 
    # (rem: non nécessaire si déjà calculé pour ce départ)
    distance, parent = # à compléter
    
    # Reconstruction de l'itinéraire (en partant de l'arrivée)
    parcours = []
    ville = arrivee
    while ville!=depart:
        ## à compléter
        
        ##
        
    # Ajouter la ville de départ
    parcours.insert(0,(depart, 0))
    
    return parcours
