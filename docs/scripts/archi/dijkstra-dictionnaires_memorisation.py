def init_algo(graphe: dict[dict]) -> (dict[str,float], dict[str,bool], dict[str,str]):
    """
    Créer et renvoyer 3 nouveaux dictionnaires pour mémoriser:
    - la distance à la ville de départ (infinie par défaut)
    - les villes visitées (vrai ou faux)
    - Le nom de la ville précédente dans le parcours (None si pas de parent)
    """
    dist = { ville:float('inf') for ville in graphe }
    vis = # à compléter
    par = # à compléter

    return dist, vis, par    