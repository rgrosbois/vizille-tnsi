def ajouter_route(graphe: list[list[str]], liste_villes:list[str],\
    depart:str, arrivee:str, distance:int):    
    # 1. Identifier les indices des villes de départ et arrivée
    pass

    # 2. Remplir les éléments correspondants
    pass

# Ajouter les 14 routes de la carte
ajouter_route(carte, villes, 'Lyon', 'Bourgoin-Jallieu', 50)
ajouter_route(carte, villes, 'Lyon', 'Valence', 102)
ajouter_route(carte, villes, 'Valence', 'Voiron', 87)
ajouter_route(carte, villes, 'Valence', 'Villard de Lans', 69)
ajouter_route(carte, villes, 'Villard de Lans', 'Grenoble', 39)
ajouter_route(carte, villes, 'Bourgoin-Jallieu', 'Voiron', 46)
ajouter_route(carte, villes, 'Bourgoin-Jallieu', 'Chambéry', 62)
ajouter_route(carte, villes, 'Voiron', 'Grenoble', 26)
ajouter_route(carte, villes, 'Grenoble', 'Chambéry', 60)
ajouter_route(carte, villes, 'Grenoble', 'Briançon', 115)
ajouter_route(carte, villes, 'Chambéry', 'Albertville', 53)
ajouter_route(carte, villes, 'Chambéry', 'Modane', 103)
ajouter_route(carte, villes, 'Albertville', 'Modane', 90)
ajouter_route(carte, villes, 'Modane', 'Briançon', 60)