def dijkstra(depart: str, liste_villes: list[str],\
    graphe: list[list[float]]) -> (list[float], list[int]):
    """
    Créer l'arbre de Dijkstra connaissant:
    - depart: la ville de départ 
    - liste_villes: la liste ordonnées des villes
    - graphe: la matrice d'adjacence de la carte.

    Renvoie la liste des distances et des parents de chaque ville.
    """
    # Initialiser l'algorithme
    distance, visite, parent = init_algo(liste_villes)
    
    # Indiquer une distance nulle pour la ville de départ
    id_depart = index(depart, liste_villes)
    ## à compléter
    
    while                            # Tant qu'il reste des villes non visités
        
        # 1) Sélectionner la (première) ville non visitée de distance minimale
        dist_min = float('inf')
        i_min = -1
        ## à compléter
        
        
        ##
        visite[i_min] = True

        # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
        # a. Mettre à jour la distance
        # b. Mémoriser la ville de distance minimale en tant que parent
        for i in range(len(graphe[i_min])):
            # à compléter
            
            ##
                
    return distance, parent