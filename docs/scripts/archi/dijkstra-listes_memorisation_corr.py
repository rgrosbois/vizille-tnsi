def init_algo(liste_villes: list[str]) -> (list[float], list[bool], list[int]):
    """
    Créer et renvoyer 3 nouvelles listes qui mémorisent:
    - la distance à la ville de départ (infinie par défaut)
    - les villes visitées (vrai ou faux)
    - l'index de la ville précédente dans le parcours (-1 si pas de parent)
    """
    dist = [float('inf') for v in liste_villes]
    vis = [False for v in liste_villes]
    prec = [-1 for v in liste_villes]

    return dist, vis, prec