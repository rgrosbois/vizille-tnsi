def itineraire(depart:str, arrivee:str, liste_villes:list[str], graphe:list[list]):
    """
    Calcule et renvoie l'itinéraire optimal entre 2 villes.
    """

    # Générer l'arbre de dijkstra de la ville de départ 
    # (rem: non nécessaire si déjà calculé pour ce départ)
    distance, parent = dijkstra(depart, liste_villes, graphe)
    
    # Index de la ville d'arrivée
    id_arrivee = index(arrivee, liste_villes)
    assert id_arrivee!=-1, f"La ville d'arrivée {arrivee} n'existe pas."
    
    # Reconstruction de l'itinéraire (en partant de l'arrivée)
    parcours = []
    i = id_arrivee
    id_depart = index(depart, liste_villes)
    while i!=id_depart: # Revenir jusqu'à la ville de départ
        parcours.insert(0,(liste_villes[i], distance[i]))
        i = parent[i]
        
    # Ajouter la ville de départ
    parcours.insert(0,(depart, 0))
    
    return parcours