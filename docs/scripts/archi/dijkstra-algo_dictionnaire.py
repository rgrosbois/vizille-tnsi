def dijkstra(depart: str, graphe: dict[dict]) -> (dict[str,float], dict[str,str]):
    """
    Créer l'arbre de Dijkstra connaissant:
    - depart: la ville de départ 
    - graphe: les dictionnaires de voisinages.

    Renvoie les dictionnaires des distances et des parents de chaque ville.
    """
    # Initialiser l'algorithme
    distance, visite, parent = init_algo(graphe)
    
    # Indiquer une distance nulle pour la ville de départ
    ## à compléter
    
    while                            # Tant qu'il reste des villes non visités
        
        # 1) Sélectionner la (première) ville non visitée de distance minimale
        dist_min = float('inf')
        ville_min = None
        for ville in graphe:
            ## à compléter
            
            
            ##
        visite[ville_min] = True

        # 2) Gestion des voisins immédiats. Si la distance cumulée est meilleure:
        # a. Mettre à jour la distance
        # b. Mémoriser la ville de distance minimale en tant que parent
        for ville_dst in graphe[ville_min]:
            # à compléter
            
            ##
                
    return distance, parent
