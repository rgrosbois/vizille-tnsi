carte = {}

def ajouter_route(dico: dict, depart: str, arrivee: str, distance: int):
    if dico.get(depart) is None:
        dico[depart] = {}
    dico[depart][arrivee] = distance
    
    if dico.get(arrivee) is None:
        dico[arrivee] = {}
    dico[arrivee][depart] = distance

# Ajouter les 14 routes
ajouter_route(carte, 'Lyon', 'Bourgoin-Jallieu', 50)
ajouter_route(carte, 'Lyon', 'Valence', 102)
ajouter_route(carte, 'Valence', 'Voiron', 87)
ajouter_route(carte, 'Valence', 'Villard de Lans', 69)
ajouter_route(carte, 'Villard de Lans', 'Grenoble', 39)
ajouter_route(carte, 'Bourgoin-Jallieu', 'Voiron', 46)
ajouter_route(carte, 'Bourgoin-Jallieu', 'Chambéry', 62)
ajouter_route(carte, 'Voiron', 'Grenoble', 26)
ajouter_route(carte, 'Grenoble', 'Chambéry', 60)
ajouter_route(carte, 'Grenoble', 'Briançon', 115)
ajouter_route(carte, 'Chambéry', 'Albertville', 53)
ajouter_route(carte, 'Chambéry', 'Modane', 103)
ajouter_route(carte, 'Albertville', 'Modane', 90)
ajouter_route(carte, 'Modane', 'Briançon', 60)