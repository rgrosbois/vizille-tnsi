def init_algo(liste_villes: list[str]) -> (list[float], list[bool], list[int]):
    """
    Créer et renvoyer 3 nouvelles listes (de même taille que `liste_villes`) pour mémoriser:
    - la distance à la ville de départ (infinie par défaut)
    - les villes visitées (faux par défaut)
    - l'index de la ville précédente dans le parcours (-1 si pas de parent)
    """
    dist = [float('inf') for v in liste_villes]
    vis = # à compléter
    prec = # à compléter

    return dist, vis, prec