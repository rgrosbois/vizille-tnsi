import os
import hashlib
from math import log10
import random
import re
from typing import Literal, Union
from urllib.parse import unquote

MAX_EMPTY_IDE = 10**8

def define_env(env):
    "Hook function"

    env.variables['dnd_counter'] = 0

    @env.macro
    def rg_dnd(question: str, liste: list[str], mel_prop=1, mel_empl=1) -> str:
        """
        Question de type glisser-déposer (Drag-and-Drop).

        La fonction génère le code HTML correspond à l'arbre suivant:
        * fieldset
        |-- legend (énoncé)
        |   |-- button (validation)
        |   |-- button (reset)
        |-- div (énoncé de la question)
        |-- div (pile de propositions)
        |   |-- div (proposition)
        |   |-- div (proposition)
        |   |-- ...
        |-- div (pile d'emplacements)
            |-- div (emplacement)
            |-- div (emplacement)
            |-- ...
        """
        # Remplacer les caractères spéciaux
        question = remplacer_caracteres_speciaux(question)
        for i in range(len(liste)):
            liste[i] = remplacer_caracteres_speciaux(liste[i][0]), remplacer_caracteres_speciaux(liste[i][1])

        html = f'<div class="enonce">{question}</div>'
        html += '<div class="pile-propositions">'

        # Liste de propositions (éventuellement mélangées)
        liste2 = liste.copy()
        if mel_prop==1:
            random.shuffle(liste2) 
        for src,dst in liste2:
            if src != '':
                html += f'<div class="proposition" id="i{env.variables["dnd_counter"]}" draggable="true" ondragstart="rg_dragstart(event)">{src}</div>'
                env.variables['dnd_counter'] += 1
            
        html += '</div><div class="pile-emplacements">'

        # Liste d'emplacements (éventuellement mélangés)
        if mel_empl==1:
            random.shuffle(liste) 
        for src,dst in liste:
            if dst != '':
                html += f'<div class="emplacement" ondragover="rg_dragover(event)" ondrop="rg_drop(event, \'{src}\')">{dst}</div>'

        html += '</div></fieldset>'

        dico = { key:val for val,key in liste}
        
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="corriger_dnd(this.parentNode.parentNode, {str(dico)})">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="reset_dnd(this.parentNode.parentNode, {str(dico)})">&#x21bb;</button></legend>'''

        return htmlStart + html

    @env.macro
    def rg_qcm(question: str, liste: list[str]) -> str:
        """
        Créer une question à choix multiple (une ou plusieurs bonne réponses sont possibles).

        La fonction génère le code HTML correspondant à l'arbre suivant :
        * fieldset
        |-- legend
        |   |-- button (validation)
        |   |-- button (reset)
        |-- pre (uniquement si [verbatim]...[/verbatim] dans l'énoncé)
        |-- div (énoncé de la question)
        |-- figure (uniquement si ![width=xx]nomfichier.xx! dans l'énoncé)
        |   |-- img
        |-- ul (propositions de réponses)
            |-- li
            |-- li
            |-- ...
        """
        # Remplacer les caractères spéciaux
        question = remplacer_caracteres_speciaux(question)
        for i in range(len(liste)):
            liste[i] = remplacer_caracteres_speciaux(liste[i])

        # Mélanger l'ordre des réponses
        melange = ([i for i in range(len(liste))])
        random.shuffle(melange)        
        bonnes_reponses = '['+','.join(['true' if liste[melange[i]][0]=='+' else 'false' for i in range(len(liste))])+']'

        # Légendes (boutons JavaScript)
        correct_clic = f'correction_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        reset_clic = f'reinitialiser_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="{correct_clic}">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="{reset_clic}">&#x21bb;</button></legend>'''

        ################
        # Énoncé de la question

        # Mode horizontal ?
        i_horiz = question.find('[horiz]')
        if i_horiz!=-1:
            horizontal = True
            question = question[i_horiz+len('[horiz]'):]
        else:
            horizontal = False

        # Code verbatim
        i_verb_start = question.find('[verbatim]')
        i_verb_end = question.find('[/verbatim]')
        if i_verb_start!=-1 and i_verb_end!=-1:
            tmp = question[:i_verb_start].rstrip()
            tmp += '<pre>'
            tmp += question[i_verb_start+len('[verbatim]'):i_verb_end].strip().replace('\n', '\n    ')
            tmp += '</pre>'
            tmp += question[i_verb_end+len('[/verbatim]'):]
            question = tmp

        # Image
        m = re.search(r"!\[[\w]+=(?P<largeur>\w+)\](?P<fichier>[^!]+)!", question)
        if m:
            tmp = question[:m.start()].rstrip()
            tmp += question[m.end():].lstrip()
            question = tmp

            htmlStart += '<div class="enonce">'+question.strip()+'</div>'
            htmlStart += '<figure style="display: flex; justify-content: center;"><img src="'+m.group('fichier')+'" style="width: '+m.group('largeur')+';"></figure>'
        else:
            htmlStart += '<div class="enonce">'+question.strip()+'</div>'

        ################
        # Propositions de réponses

        # Affichage (mélangé) des propositions
        ul = '<ul class="reponses">'
        for i in range(len(liste)):
            elem = liste[melange[i]][1:]
            if horizontal:
                ul += f'<li onclick="cocher(event)" style="display: inline;">{elem}</li>'
            else:
                ul += f'<li onclick="cocher(event)">{elem}</li>'
        ul += '</ul>'

        # Bouton de validation
        htmlEnd = '</fieldset>'

        resultat = htmlStart + ul + htmlEnd
        return resultat
    
    def remplacer_caracteres_speciaux(chaine: str):
        return chaine.replace("'", "′").replace('"', " ")


    def ajouter_question(questions: list[tuple], enonce: str, propositions: list[str]):
        """
        Rajouter une question (=énonce + propositions) dans un questionnaire.
        """

        if len(propositions)==0: # question non valide
            return

        # Caractères spéciaux
        enonce = remplacer_caracteres_speciaux(enonce)
        for i in range(len(propositions)):
            propositions[i] = remplacer_caracteres_speciaux(propositions[i])

        # Ajouter au questionnaire
        questions.append( (enonce, propositions) )

    @env.macro
    def rg_qnum(reponse, precision) -> str:
        """
        Créer une question attendant une réponse numérique.
        """
        largeur = 3+len(str(reponse))

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="number" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qnum(this.parentNode, {reponse}, {precision});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qnum(this.parentNode, {reponse}, {precision})">&#10003;</button>'
        tmp += '</span>'
        return tmp

    @env.macro
    def rg_qsa(reponses: list[str], largeur=-1) -> str:
        """
        Créer une question attendant une réponse textuelle courte.
        """
        # Prévoir une zone de saisie de 3 caractères de plus
        # que la réponse la plus longue
        if largeur==-1:
            maximum = 0
            for r in reponses:
                if len(r)>maximum:
                    maximum = len(r)
            largeur = 3+maximum

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="text" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qsa(this.parentNode, {reponses});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qsa(this.parentNode, {reponses})">&#10003;</button>'
        tmp += '</span>'
        return tmp


    @env.macro
    def rg_generer_qcm(nom_fichier) -> str:
        """
        Créer un questionnaire à partir d'un fichier au format ressemblant à l'AMC-TXT.
        """
        questions = []
        enonce = ''
        propositions = []

        # Extraire du fichier la liste des questions et leurs propositions
        with open(nom_fichier, 'rt') as f:
            continuer = True
            while continuer:
                ligne = f.readline()

                if ligne=='': # fin de fichier
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []
                    break

                ligne = ligne.strip() # supprimer la fin de ligne
                if ligne=='': # fin de question
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []

                elif ligne[0]=='+' or ligne[0]=='-': # proposition de réponse
                    propositions.append(ligne)
                else: # ligne d'énoncé de question
                    idx = 0
                    while ligne[idx]=='*': # Supprimer les * en début de ligne
                        idx += 1
                    if len(enonce)==0:
                        enonce = ligne[idx:]
                    else:
                        enonce += '\n'+ligne[idx:]

        # Générer le code HTML
        code_html = ''

        counter = 0
        for q in questions:
            counter += 1
            code_html += '=== "Q' + str(counter) + '"\n\t'
            code_html += rg_qcm(q[0], q[1]) + "\n"

        return code_html    
    
    @env.macro
    def rg_corriger_page(titre="Corriger toute la page") -> str:
        """
        Créer un bouton permettant de lancer l'opération de correction
        de tous les exercices de la page Web.
        """
        return f'<button class="md-button md-button--primary" onclick="corriger_page(this)">{titre}</button>'

    @env.macro
    def rgpopup(titre, contenu) -> str:
        tmp = f'<button class="md-button" onclick="this.parentNode.nextSibling.style.display = \'block\';">{titre}</button>'
        tmp += '<div class="rgmodal" onclick="this.style.display = \'none\';"><div class="rgmodal-content" onclick="event.stopPropagation();">'
        tmp += '<span class="fermer" onclick="this.parentNode.parentNode.style.display = \'none\';">&times;</span>'
        tmp += f'<h1>{titre}</h1><p>{contenu}</p></div></div>'
        return tmp
        
    ##################
    # pyodide-mkdocs #
    ##################
#     env.variables["term_counter"] = 0
#     env.variables["IDE_counter"] = 0
#     INFINITY_SYMBOL = "∞"

#     @env.macro
#     def terminal() -> str:
#         """
#         @brief : Create a Python Terminal.
#         @details : Two layers to avoid focusing on the Terminal. 1) Fake Terminal using CSS 2) A click hides the fake
#         terminal and triggers the actual Terminal.
#         """
#         id_ide = env.variables["term_counter"]
#         env.variables["term_counter"] += 1
#         return f"""<div onclick='start_term("id{id_ide}")' id="fake_id{id_ide}" class="py_mk_terminal_f"><label class="terminal"><span>>>> </span></label></div><div id="id{id_ide}" class="py_mk_hide"></div>"""

#     def read_external_file(script_name: str, path: str, extension: str = "py") -> str:
#         """
#         @brief : Read a Python file that is uploaded on the server.
#         @details : The content of the file is hidden in the webpage. Replacing \n, _ and * by a string enables
#         the integration in mkdocs admonitions.
#         """
#         docs_path = f"""docs/"""

#         try:
#             relative_path = "scripts" if path == "" else path
#             with open(
#                 f"""{docs_path}/{relative_path}/{script_name}.{extension}"""
#             ) as filename:
#                 content = "".join(filename.readlines())
#                 content = content + "\n"

#             return escape_problematic_characters(content)
#         except FileNotFoundError:
#             return ""

#     def escape_problematic_characters(script: str) -> str:
#         return (
#             script.replace("\n", "bksl-nl")
#             .replace("_", "py-und")
#             .replace("*", "py-str")
#         )

#     def get_image_path() -> str:
#         split_page_url = os.path.dirname(
#             convert_url_to_utf8(env.variables.page.url)
#         ).split("/")
#         prefix = "".join(["../" for folder in split_page_url if folder != ""])
#         return f"""{prefix}pyodide-mkdocs"""

#     # TODO : this issue concerning the urls must be closed ASAP
#     def get_filepath() -> str:
#         print("docs_dirs", env.conf["docs_dir"])
#         print(
#             "P1",
#             env.variables.page.abs_url,
#             "/".join(
#                 filter(
#                     lambda folder: folder != "",
#                     convert_url_to_utf8(env.variables.page.url).split("/")[:-2],
#                 )
#             ),
#         )
#         print(
#             "P2",
#             env.variables.page.abs_url,
#             "/".join(
#                 filter(
#                     lambda folder: folder != "",
#                     convert_url_to_utf8(env.variables.page.abs_url).split("/")[2:-2],
#                 )
#             ),
#         )
#         return "/".join(
#             filter(
#                 lambda folder: folder != "",
#                 convert_url_to_utf8(env.variables.page.abs_url).split("/")[2:-2],
#             )
#         )

#     # TODO : handle the case where the same files are loaded on the same page.
#     def generate_id_ide(content: str) -> str:
#         """
#         @brief : Return current number IDE {id_ide}.
#         """
#         if content not in [None, ""]:
#             id_ide = hashlib.sha1(content.encode("utf-8")).hexdigest()
#         else:  # non-existent file, empty file
#             id_ide = env.variables["IDE_counter"]
#             env.variables["IDE_counter"] += 1
#         return str(id_ide).zfill(int(log10(MAX_EMPTY_IDE)))

#     def blank_space(s: float = 0.3) -> str:
#         """
#         @brief : Return 5em blank spaces. Use to spread the buttons evenly
#         """
#         return f"""<span style="display: inline-block; width:{s}em"></span>"""

#     def format_unlimited_attempts(
#         max: Union[int, Literal["+"]]
#     ) -> Union[int, Literal["∞"]]:
#         return int(max) if max not in ["+", 1000] else INFINITY_SYMBOL

#     def get_max_from_file(content: str) -> str:
#         """
#         @brief : Allows to specify max number of attempt on first line of input file with #MAX=6
#         """
#         split_content = content.split("bksl-nl")
#         first_line = split_content[0]
#         if first_line[:4] != "#MAX":
#             return ""

#         used_max = first_line.split("=")[1].strip()
#         return used_max

#     def strip_empty_lines_after_max(content: str) -> str:
#         split_content = content.split("bksl-nl")
#         i = 1
#         while split_content[i] == "":
#             i += 1
#         content = "bksl-nl".join(split_content[i:])
#         return content

#     def get_allowed_number_of_attempts(
#         max_from_file: str, max_IDE: Union[int, Literal["+"]]
#     ) -> Union[int, Literal["∞"]]:

#         if max_from_file != "":
#             return format_unlimited_attempts(
#                 int(max_from_file) if max_from_file != "+" else "+"
#             )

#         return format_unlimited_attempts(max_IDE)

#     def test_style(script_name: str, element: str) -> bool:
#         quotes = ["'", '"']
#         ide_style = ["", "v"]
#         styles = [
#             f"""IDE{style}({quote}{script_name}{quote}"""
#             for quote in quotes
#             for style in ide_style
#         ]
#         return any([style for style in styles if style in element])

#     def convert_url_to_utf8(nom: str) -> str:
#         return unquote(nom, encoding="utf-8")

#     @env.macro
#     def IDEv(
#         script_name: str = "", MAX: Union[int, Literal["+"]] = 5, SANS: str = ""
#     ) -> str:
#         """
#         @brief : Helper macro to generate vertical IDE in Markdown mkdocs.
#         @details : Starts the IDE function with 'v' mode.
#         """
#         return IDE(script_name, mode="_v", MAX=MAX, SANS=SANS)

#     def generate_key(filepath: str):
#         try:
#             with open(f"docs/{filepath}/clef.txt", "r", encoding="utf8") as filename:
#                 key_ide = filename.read()
#         except FileNotFoundError:
#             key_ide = ""  # base case -> no clef.txt file
#         return key_ide

#     @env.macro
#     def create_button(
#         button_name: str, onclick_action: str, isTranslated: bool = True
#     ) -> str:
#         AVAILABLE_BUTTONS = {
#             "Play": "Lancer",
#             "Download": "Télécharger",
#             "Check": "Valider",
#             "Restart": "Recharger",
#             "Save": "Sauvegarder",
#             "Upload": "Téléverser",
#         }

#         # def define_action(*args: tuple[str, ...]) -> str:
#         #     options = '","'.join([f"{arg}" for arg in args[0]])
#         #     return f"""\'{button_name}("{options}")\'"""

#         tooltip_text = AVAILABLE_BUTTONS[button_name] if isTranslated else button_name

#         return f"""<button class="tooltip" onclick={onclick_action}>\
#             <img src="{get_image_path()}/icons8-{button_name.lower()}-64.png">\
#             <span class="tooltiptext">{tooltip_text}</span>\
#             </button>"""

#     def create_upload_button(editor_name: str) -> str:
#         """
#         @brief : Create upload button for a IDE number {id_ide}.
#         @details : Use an HTML input to upload a file from user. The user clicks on the button to fire a JS event
#         that triggers the hidden input.
#         """
#         onclick_action = (
#             f""""document.getElementById('input_{editor_name}').click()" """
#         )
#         return f"""{create_button("Upload", onclick_action)}\
#                 <input type="file" id="input_{editor_name}" name="file" enctype="multipart/form-data" class="py_mk_hide"/>"""

#     def create_unittest_button(
#         editor_name: str,
#         script_name: str,
#         path: str,
#         mode: str,
#         number_max_attempts: Union[Literal["∞"], int],
#     ) -> str:
#         """
#         @brief : Generate the button for IDE {id_ide} to perform the unit tests if a valid test_script.py is present.
#         @details : Hide the content in a div that is called in the Javascript
#         """
#         stripped_script_name = script_name.split("/")[-1]
#         relative_path = "/".join(script_name.split("/")[:-1])
#         script_name = f"{relative_path}/{stripped_script_name}_test"
#         content = read_external_file(script_name, path)

#         if content == "":
#             return ""

#         onclick_action = f"""\'check("{editor_name}","{mode}")\'"""
#         return f"""<span id="test_term_{editor_name}" class="py_mk_hide">{content}</span>\
#             {create_button("Check", onclick_action)}\
#             <span class="compteur">{number_max_attempts}/{number_max_attempts}</span>"""

#     def div(content: str, div_options: dict[str, str]) -> str:
#         formatted_div_options = " ".join(
#             [
#                 f"""{div_attribute}="{attribute_value}" """
#                 for div_attribute, attribute_value in div_options.items()
#             ]
#         )
#         return f"""<div {formatted_div_options}>{content}</div>"""

#     def grouped_buttons(buttons: str) -> str:
#         return buttons + blank_space(1)

#     def generate_empty_ide(editor_name: str, mode: str) -> str:
#         shortcut_comment_asserts = (
#             f'<span id="comment_{editor_name}" class="comment">###</span>'
#         )

#         div_editor_terminal = div(
#             "", {"id": f"term_{editor_name}", "class": f"term_editor{mode}"}
#         )

#         div_decorations = (
#             f'<div class="line_v"><div id="{editor_name}"></div></div>'
#             if mode == "_v"
#             else f'<div class="line" id="{editor_name}"></div>'
#         )

#         return div(
#             f"{shortcut_comment_asserts}{div_decorations}{div_editor_terminal}",
#             {"class": f"wrapper{mode}"},
#         )

#     def generate_row_of_buttons(
#         editor_name: str,
#         script_name: str,
#         mode: str,
#         filepath: str,
#         number_max_attempts: Union[int, Literal["∞"]],
#     ) -> str:
#         play_buttons_group = grouped_buttons(
#             create_button("Play", f"""'play("{editor_name}","{mode}")'""")
#             + create_unittest_button(
#                 editor_name, script_name, filepath, mode, number_max_attempts
#             )
#         )

#         transfer_buttons_group = grouped_buttons(
#             create_button(
#                 "Download", f"""\'download("{editor_name}","{script_name}")\'"""
#             )
#             + blank_space()
#             + create_upload_button(editor_name)
#         )

#         save_buttons_group = grouped_buttons(
#             create_button("Restart", f"""\'restart("{editor_name}")\'""")
#             + blank_space()
#             + create_button("Save", f"""\'save("{editor_name}")\'""")
#         )

#         return play_buttons_group + transfer_buttons_group + save_buttons_group

#     def get_last_dir_from(path: str) -> str:
#         return path.split("/")[-2]

#     def get_custom_dir() -> Union[str, None]:
#         for path in env.conf["theme"].dirs:
#             if os.path.exists(path + "/pyodide-mkdocs/start_REM.md"):
#                 return get_last_dir_from(path)
#         return None

#     @env.macro
#     def insert_remark_file(script_name: str, key_ide: str) -> str:
#         IDE_calls_from_md_file = [
#             elt for elt in env.page.markdown.split("\n") if test_style(script_name, elt)
#         ]
#         first_IDE_call = (
#             IDE_calls_from_md_file[0] if len(IDE_calls_from_md_file) >= 1 else ""
#         )

#         leading_spaces = " " * (len(first_IDE_call) - len(first_IDE_call.lstrip()))

#         block_remark = ""
#         if script_name != "":
#             block_remark = f"""
# {leading_spaces}--8<--- "{get_custom_dir()}/pyodide-mkdocs/start_REM.md"
# """
#             filepath = get_filepath()
#             block_remark += (
#                 f'''
# {leading_spaces}--8<--- "docs/{filepath if filepath != "" else "scripts"}/{script_name}_REM.md"'''
#                 if key_ide == ""
#                 else f""
#             )

#             block_remark += f"""
# {leading_spaces}--8<--- "{get_custom_dir()}/pyodide-mkdocs/end_REM.md"
# """
#         return block_remark

#     def insert_content(editor_name: str, ide_content: str) -> str:
#         return f"""<span id="content_{editor_name}" class="py_mk_hide">{ide_content}</span>"""

#     def insert_corr_content(
#         editor_name: str, ide_corr_content: str, key_ide: str
#     ) -> str:
#         return f"""<span id="corr_content_{editor_name}" class="py_mk_hide" data-strudel="{str(key_ide)}">{ide_corr_content}</span>"""

#     @env.macro
#     def IDE(
#         script_name: str = "",
#         mode: str = "",
#         MAX: Union[int, Literal["+"]] = 5,
#         SANS: str = "",
#     ) -> str:
#         """
#         @brief : Create an IDE (Editor+Terminal) within an Mkdocs document. {script_name}.py is loaded on the editor if present.
#         @details : Two modes are available : vertical or horizontal. Buttons are added through functional calls.
#         Last span hides the code content of the IDE when loaded.
#         """
#         filepath = get_filepath()

#         ide_content = read_external_file(script_name, filepath)
#         id_ide = generate_id_ide(ide_content)

#         if (max_from_file := get_max_from_file(ide_content)) != "":
#             ide_content = strip_empty_lines_after_max(ide_content)
#         allowed_number_of_attempts = get_allowed_number_of_attempts(max_from_file, MAX)

#         format_excluded_instructions = (
#             lambda instructions: "," + "".join(instructions.split(" "))
#             if len(instructions) > 0
#             else ""
#         )
#         editor_name = f"editor_{id_ide}"

#         div_exercise = div(
#             generate_empty_ide(editor_name, mode)
#             + generate_row_of_buttons(
#                 editor_name, script_name, mode, filepath, allowed_number_of_attempts
#             ),
#             {
#                 "class": "py_mk_ide",
#                 "data-max": f"{allowed_number_of_attempts}",
#                 "data-exclude": f'{"eval,exec" + format_excluded_instructions(SANS)}',
#             },
#         )

#         div_exercise += insert_content(editor_name, ide_content)

#         key_ide = generate_key(filepath)
#         ide_corr_content = read_external_file(
#             f"""{'/'.join(script_name.split('/')[:-1])}/{script_name.split('/')[-1]}_corr""",
#             filepath,
#         )
#         div_exercise += insert_corr_content(editor_name, ide_corr_content, key_ide)
#         div_exercise += insert_remark_file(script_name, key_ide)

#         return div_exercise

#     @env.macro
#     def mult_col(*text):
#         cmd = """<table style="border-color:transparent;background-color:transparent"><tr>"""
#         for column in text:
#             cmd += f"""<td><b style="font-size:1.2em">{column}</td>"""
#         cmd += f"""</tr></table>"""
#         return cmd

#     def generate_id():
#         alphabet = [chr(ord("a") + i) for i in range(26)]
#         return "".join(random.choices(alphabet, k=6))

#     def latexify(answer):
#         """$ might not be the first character :
#         blabla $1+1$
#         """
#         answer = str(answer)
#         if (
#             answer.count("$") - answer.count("\$") >= 2
#         ):  # regex begin ___$ and end $____ and $ not preceded by \
#             string = ""
#             start_dollar = True
#             for i in range(len(answer)):
#                 lettre = answer[i]
#                 if lettre == "$":
#                     if i == 0 or (
#                         i >= 1 and answer[i - 1] != "\\"
#                     ):  # case escaping dollar \$
#                         string += "\(" if start_dollar else "\)"
#                         start_dollar = not start_dollar
#                     else:
#                         string += "$"
#                 else:
#                     string += lettre
#             return string
#         return answer